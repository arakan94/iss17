<?php
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

abstract class Visibility {
	const ALL = 0;
	const ACADEMIC = 1;
	const ADMIN = 2;
	const ACADEMIC_ADMIN = 3;
	const STUDENT = 4;
	const ACADEMIC_STUDENT = 5;
	const USER = 6;
};

class MenuRow {
	public $text;
	public $page;
	public $level;
	public $visibility;
	public $path = "";
	public $args = "";

	public function __construct($text, $page, $level, $visibility, $path = "/pages/", $args = "")
	{
		$this->text = $text;
		$this->page = $page;
		$this->level = $level;
		$this->visibility = $visibility;
		$this->path = $path;
		$this->args = $args;
	}
};

class Menu {
	public $page;
	public $subpage;

	private $menu;
	private $student = false;
	private $user = false;
	private $admin = false;
	private $academic = false;	

	public function __construct($menu, $page, $subpage)
	{
		$this->menu = $menu;
		$this->page = $page;
		$this->subpage = $subpage;

		if ($_SESSION['login_status'])
			$this->user = true;

		if ($_SESSION['rocnik'] > 0)
			$this->student = true;

		if ($_SESSION['opravneni'] == 1)
			$this->admin = true;

		if (!empty($_SESSION['zkratka_ustav']))
			$this->academic = true;
	}

	private function visible($visibility)
	{
		return (
			($visibility == Visibility::ACADEMIC_STUDENT && ($this->academic || $this->student)) ||
			($visibility == Visibility::ACADEMIC_ADMIN && ($this->academic || $this->admin)) ||
			($visibility == Visibility::ADMIN          && ($this->admin))                    ||
			($visibility == Visibility::ACADEMIC       && ($this->academic))                 ||
			($visibility == Visibility::USER           && ($this->user))                     ||
			($visibility == Visibility::STUDENT        && ($this->student))                  ||
			($visibility == Visibility::ALL)
		);
	}

	public function toHTML()
	{
		$out  = '<nav>';
		$out .= '<ul>';

		for ($i = 0; $i < count($this->menu); $i++)
		{
			$row  = $this->menu[$i];

			// najdi dalsi viditelnou polozku
			for ($j = $i+1; $j < count($this->menu); $j++)
			{
				$next = $this->menu[$j];

				if ($this->visible($next->visibility))
					break;
			}

			if ($this->visible($row->visibility))
			{
				$out .= '<li';

				// aktivni item?
				if ($row->level == 0)
					$out .= ($this->page == $row->page ? ' class="aktivni"' : '');
				else
					$out .= ($this->subpage == $row->page ? ' class="aktivni_sub"' : '');

				$out .= '>';

				// adresa
				$out .= '<a href="';
				if ($row->path == "#")
					$out .= "#";
				else
					$out .= $_SERVER['ROOT_URL'] . $row->path . $row->page . '.php';

				if (!empty($row->args))
					$out .= '?' . $row->args;

				$out .= '">';
				$out .= $row->text;
				$out .= '</a>';

				// vyreseni druhe urovne menu
				if ($next->level > $row->level)
				{
					$out .= '<ul>';
				}
				else if ($next->level < $row->level)
				{
					$out .= '</ul>';
					$out .= '</li>';
				}
				else
					$out .= '</li>';
			}
		}

		$out .= '</ul>';
		$out .= '</ul></nav>';

		return $out;
	}
};

$menu_struct = [
	new MenuRow("Rozvrh",               "index", 0, Visibility::ALL, "/"),

	new MenuRow("Přidej",           "pridej_hl", 0, Visibility::ACADEMIC + Visibility::ADMIN, "#"),
	new MenuRow("Přidej předmět",   "pridej_pr", 1, Visibility::ADMIN),
	new MenuRow("Přidej obor",      "pridej_ob", 1, Visibility::ADMIN),
	new MenuRow("Přidej učebnu",    "pridej_uc", 1, Visibility::ADMIN),
	new MenuRow("Přidej ústav",     "pridej_us", 1, Visibility::ADMIN),
	new MenuRow("Přidej uživatele", "pridej_uz", 1, Visibility::ADMIN),
	new MenuRow("Přidej akci",      "pridej_ak", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Přidej vybavení",  "pridej_vy", 1, Visibility::ADMIN),
	new MenuRow("Přidej rezervaci", "pridej_re", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Přidej typ akce",  "pridej_ta", 1, Visibility::ADMIN),

	new MenuRow("Správa studentů",              "sprava_hl", 0, Visibility::ADMIN + Visibility::ACADEMIC, "#"),
	new MenuRow("Výpis registrovaných předmětů", "vypis_rp", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Registrace předmětů",          "pridej_rp", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Zápis oborů",             "pridej_ro", 1, Visibility::ADMIN + Visibility::ACADEMIC),

	new MenuRow("Osobní",               "osobni_roz", 0, Visibility::USER, "#"),
	new MenuRow("Můj účet",               "muj_ucet", 1, Visibility::USER),
	new MenuRow("Výpis předmětů",         "vypis_rp", 1, Visibility::STUDENT),
	new MenuRow("Rozvrh",                "rozvrh_os", 1, Visibility::STUDENT),
	new MenuRow("Garantované předměty", "uzivatel_d", 1, Visibility::ACADEMIC, "/pages/", "action=garant&uziv_cislo=" . $_SESSION['uziv_cislo']),
	//new MenuRow("Rozvrh - garant",               "rozvrh_os", 1, Visibility::ACADEMIC),

	new MenuRow("Výpis",            "vypis_hl", 0, Visibility::ALL, "#"),
	new MenuRow("Výpis předmětů",   "vypis_pr", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Výpis oborů",      "vypis_ob", 1, Visibility::ALL),
	new MenuRow("Výpis učeben",     "vypis_uc", 1, Visibility::ALL),
	new MenuRow("Výpis ústavů",     "vypis_us", 1, Visibility::ALL),
	new MenuRow("Výpis uživatelů",  "vypis_uz", 1, Visibility::ALL),
	new MenuRow("Výpis akcí",       "vypis_ak", 1, Visibility::ALL),
	new MenuRow("Výpis vybavení",   "vypis_vy", 1, Visibility::ADMIN + Visibility::ACADEMIC),
	new MenuRow("Výpis rezervací",  "vypis_re", 1, Visibility::ALL),
	new MenuRow("Výpis typů akcí",  "vypis_ta", 1, Visibility::ADMIN + Visibility::ACADEMIC)
];

$menu = new Menu($menu_struct, $page, $page1);
echo $menu->toHTML();

// login, logout
if ($_SESSION['login_status'] == true)
{   echo '<a href="' . $_SERVER['ROOT_URL'] . '/pages/prihlaseni.php?action=odhlaseni" class="button">Odhlásit se</a>';
	echo '  Uživatel: ' . $_SESSION['login'] .
	' Akademik: ' . (empty($_SESSION['zkratka_ustav']) ? "Ne" : "Ano") . 
	' Správce: ' . ($_SESSION['opravneni'] == 1 ? "Ano" : "Ne") . 
	' Student: ' . (empty($_SESSION['rocnik']) ? "Ne" : $_SESSION['rocnik'] . '. ročník');
	
}
else
	echo '<a href="' . $_SERVER['ROOT_URL'] . '/pages/prihlaseni.php" class="button">Přihlásit se</a>';
?>
