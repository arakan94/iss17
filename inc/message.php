<?php
if(!empty($_GET['message'])) {
    $message = $_GET['message'];
        if($message == "success"){
            echo "<div class='success' id='hide'> Položka úspěšně přidána.";
            echo "</div>";
        } else if($message == "edit"){
            echo "<div class='success' id='hide'> Položka úspěšně editována.";
            echo "</div>";
        } else if($message == "edit_fail"){
            echo "<div class='failure'> Editace položky se nezdařila.";
            echo "</div>";
        } else if($message == "edit_pk_fail"){
            echo "<div class='failure'> Editace primárního klíče se nezdařila.";
            echo "</div>";
        } else if($message == "obor_failure"){
            echo "<div class='failure'> Student již má tento obor zapsán.";
            echo "</div>";
        } else if($message == "predmet_failure"){
            echo "<div class='failure'> Student již má tento předmět registrován.";
            echo "</div>";
        } else if($message == "delete"){
            echo "<div class='success' id='hide'> Položka úspěšně odstraněna.";
            echo "</div>";
        } else if($message == "delete_fail"){
            echo "<div class='failure'> Položka nebyla odstraněna.";
            echo "</div>";
        } else if($message == "password"){
            echo "<div class='success' id='hide'> Heslo úspěšně změněno.";
            echo "</div>";
        } else if($message == "password_fail"){
            echo "<div class='failure'> Heslo se nepodařilo změnit.";
            echo "</div>";
        } else if($message == "exist"){
            echo "<div class='failure'> Položka už existuje.";
            echo "</div>";
        } else if($message == "failure"){
            echo "<div class='failure'> Něco se nepovedlo.";
            echo "</div>";
        } else if($message == "prihlaseni"){
            echo "<div class='success' id='hide'> Uživatel byl přihlášen.";
            echo "</div>";
        } else if($message == "odhlaseni"){
            echo "<div class='success' id='hide'> Uživatel byl odhlášen.";
            echo "</div>";
        } else if($message == "odhlaseni_neak"){
            echo "<div class='failure'> Uživatel byl odhlášen z důvodu neaktivity.";
            echo "</div>";
        } else if($message == "prihlaseni_neexistuje"){
            echo "<div class='failure'> Uživatel, kterého se snažíte přihlásit, neexistuje.";
            echo "</div>";
        } else if($message == "registrace"){
            echo "<div class='success' id='hide'> Uživatel byl úspěšně vytvořen.";
            echo "</div>";
        }else if($message == "registrace_exist"){
            echo "<div class='failure'> Uživatel s loginem ".$_GET["login"]." již existuje.";
            echo "</div>";
        }
        
    }
?>
