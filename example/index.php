<?php // Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz> ?>
<?php //ini_set("default_charset", "UTF-8"); // v pripade problemu s kodovanim.. nutne u PHP 5.xx ?>

<?php
	require "../db/db.php";
	require "../db/obor.php";
?>

<?php
	$conn = db_connect();
?>

<?php // zpracovani formulare na stejne strance - metoda 2

	if (isset($_POST['zkratka_obor']) && isset($_POST['nazev']))
	{
		$obor = new Obor($_POST['zkratka_obor'], $_POST['nazev']);

		if ($_POST['action'] == "upravit")
		{
			$obor->update();
		}
		else if ($_POST['action'] == "odstranit")
		{
			$obor->delete();
		}
		else
		{
			$obor->add();
		}
	}

?>

<!doctype html>

<html lang="cs">

<head>
	<meta charset="utf-8">

	<title>Test</title>
	<meta name="description" content="Testovací PHP+MySQL stránka na serveru eva.">
	<meta name="author" content="David Novák">

	<link rel="stylesheet" href="css/style.css">

	<script>
	function validateForm()
	{
		var zkratka_obor = document.forms["pridat_obor"]["zkratka_obor"].value;
		var nazev = document.forms["pridat_obor"]["nazev"].value;

		if (zkratka_obor == "" || nazev == "")
		{
			alert("Povinná pole nevyplněna!");
			return false;
		}

		return true;
	}
	
	</script>

</head>

<body>
	<p>
	TEST +ěščřžýáíé=
	</p>
	
	<h2>Obory</h2>
	<?php
		$result = get_obory();
		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo $row["zkratka_obor"] . " - " . $row["nazev"] . '<a href="index.php?action=upravit&zkratka_obor=' .$row["zkratka_obor"]. '&nazev='.$row["nazev"].'"> Upravit </a> <a href="index.php?action=odstranit&zkratka_obor=' .$row["zkratka_obor"]. '&nazev='.$row["nazev"].'"> Odstranit </a>'. "<br>";
			}
		}
		else 
			echo "0 results";
	?>
	
	<!-- Osetreni formulare provedeno JS -->
	<h2>Přidat obor</h2>
	<form name="pridat_obor" onsubmit="return validateForm()" action="scripts/add_obor.php?from=<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		Zkratka*: <input type="text" name="zkratka_obor"> <br>
		Název*:   <input type="text" name="nazev"> <br>
		<input type="submit">
	</form>
	
	<p>* hodnota je povinná</p>
	
	<!-- Osetreni formulare provedeno primo HTML - moderni browsery.. Asi pouzit oboje? -->
	<h2>Přidat obor - metoda 2</h2>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		Zkratka*: <input type="text" name="zkratka_obor" value="<?php echo $_GET['zkratka_obor']; ?>" required> <br>
		Název*:   <input type="text" name="nazev" value="<?php echo $_GET['nazev']; ?>" required> <br>
		<?php
			if ($_GET['action'] == "upravit")
				echo '<input type="hidden" name="action" value="upravit">
				      <input type="submit" value="Upravit">';
			else if ($_GET['action'] == "odstranit")  // odstraneni by to z pohledu GUI urcite chtelo resit jinak - zde jen pro overeni funkcnosti
				echo '<input type="hidden" name="action" value="odstranit">
				      <input type="submit" value="Odstranit">';
			else
				echo '<input type="submit" value="Vložit">';
		?>
	</form>

	<p>* hodnota je povinná</p>

	<?php
		echo phpinfo();
	?>

</body>

</html>
