<?php // Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz> ?>
<?
	require "../../db/db.php";
	require "../../db/obor.php";

	$conn = db_connect();

	$obor = new Obor($_POST['zkratka_obor'], $_POST['nazev']);

	$obor->add();

	header("Location: " . $_GET['from']);
?>
