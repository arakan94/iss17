<?php // Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz> ?>
<?php //ini_set("default_charset", "UTF-8"); // v pripade problemu s kodovanim.. nutne u PHP 5.xx ?>

<?php
	require "../db/db.php";
	require "../db/predmet.php";
?>

<?php
	$conn = db_connect();
?>

<?php

	if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok']))
	{
		$predmet = new Predmet($_POST['zkratka_predmet'], $_POST['ak_rok']);

		if ($_POST['action'] == "upravit")
		{
			$predmet->nazev = $_POST['nazev'];

			if (isset($_POST['rocnik']))
				$predmet->rocnik = $_POST['rocnik'];

			if (!$predmet->exists())
				echo "predmet nexistuje!";
			else
				$predmet->update();
		}
		else if ($_POST['action'] == "odstranit")
		{
			$predmet->delete();
		}
		else
		{
			if ($predmet->exists())
			{
				echo "predmet uz existuje!";
				echo $predmet->nazev;
			}
			else
			{
				$predmet->nazev = $_POST['nazev'];
				$predmet->rocnik = $_POST['rocnik'];
				if ($predmet->add())
					echo "predmet " . $predmet->nazev . " přidán..";
				else
					echo "neco se nepovedlo!";
			}
		}
	}

?>

<!doctype html>

<html lang="cs">

<head>
	<meta charset="utf-8">

	<title>Test</title>
	<meta name="description" content="Testovací PHP+MySQL stránka na serveru eva.">
	<meta name="author" content="David Novák">

	<link rel="stylesheet" href="css/style.css">

</head>

<body>
	<p>
	TEST +ěščřžýáíé=
	</p>
	
	<h2>Obory</h2>
	<?php
		$result = get_predmety();
		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo $row["zkratka_predmet"] . " - " . $row["nazev"] . " ročník: " . $row["rocnik"] .
			  '<a href="predmet.php?action=upravit&zkratka_predmet=' .$row["zkratka_predmet"].'&ak_rok='.$row["ak_rok"].'"> Upravit </a> 
			  <a href="predmet.php?action=odstranit&zkratka_predmet=' .$row["zkratka_predmet"]. '&ak_rok='.$row["ak_rok"].'"> Odstranit </a>'. "<br>";
			}
		}
		else 
			echo "0 results";
	?>


	<h2>Přidat obor - metoda 2</h2>
<?php // druhy zpusob reseni upravy predmetu

	if ($_GET['action'] == "upravit")
	{
		$u_predmet = new Predmet($_GET['zkratka_predmet'], $_GET['ak_rok']);
	
		if ($u_predmet->exists())
			echo "hura - upravujeme<br>";
		else
			echo "upravovany predmet neexistuje<br>";
	}
	else if ($_GET['action'] == "odstranit")
	{
		$u_predmet = new Predmet($_GET['zkratka_predmet'], $_GET['ak_rok']);
	
		if ($u_predmet->exists())
			echo "chces opravdu smazat?<br>";
		else
			echo "mazany predmet neexistuje<br>";
	}
	else
		$u_predmet = new Predmet(NULL, NULL);

?>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		Zkratka*: <input type="text" name="zkratka_predmet" value="<?php echo $u_predmet->get_zkratka(); ?>" required> <br>
		Název*:   <input type="text" name="nazev" value="<?php echo $u_predmet->nazev; ?>" required> <br>
		Ak. rok*: <input type="text" name="ak_rok" value="<?php echo $u_predmet->get_ak_rok(); ?>" required> <br>
		Ročník:  <input type="text" name="rocnik" value="<?php echo $u_predmet->rocnik; ?>"> <br>
		<?php
			if ($_GET['action'] == "upravit")
				echo '<input type="hidden" name="action" value="upravit">
				      <input type="submit" value="Upravit">';
			else if ($_GET['action'] == "odstranit")  // odstraneni by to z pohledu GUI urcite chtelo resit jinak - zde jen pro overeni funkcnosti
				echo '<input type="hidden" name="action" value="odstranit">
				      <input type="submit" value="Odstranit">';
			else
				echo '<input type="submit" value="Vložit">';
		?>
	</form>

	<p>* hodnota je povinná</p>

	<?php
		echo phpinfo();
	?>

</body>

</html>
