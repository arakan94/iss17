<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require $_SERVER['ROOT_DIR'] . "/db/db.php";
require $_SERVER['ROOT_DIR'] . "/db/obor.php";
require $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require $_SERVER['ROOT_DIR'] . "/db/akce.php";
require $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

$conn = db_connect();
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <script> 
            $( document ).ready(function() {
                if($("#hide")){
                    $("#hide").fadeTo(3000, 400).slideUp(400, function(){
                       $("#hide").slideUp(400);
                        });   
                }
            });
        </script>
    </head>
    <body>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php
            $page = 'index';
            include($_SERVER['ROOT_DIR'] . '/inc/menu.php');
            ?>

        </header>
        <div class="center">
            <article>
                        <script>
                            $(document).ready(function () {

                                // Datepicker
                                var fullDate = "";
                                var navrat = [];
                                
                                $('#datepicker').datepicker({
                                    dateFormat: 'yy-mm-dd',
                                });
                            });
                            function setDate(date, da) {
                                var dat;
                                var od, ddo;
                                var items = da;
                                if (date != "") {
                                    var len = items.length;
                                    var new_arr = [];
                                    new_arr = items; // předávání odkazem mění se i původní pole items
                                    while (len > 0) {
                                        dat = parseDate(items[len - 1][1]);
                                        if (date != dat) {
                                            new_arr = deleteItem(new_arr, len - 1);
                                        } else {
                                            od = parseHour(items[len - 1][1]);
                                            ddo = parseHour(items[len - 1][2]);
                                            new_arr[len - 1].unshift(ddo);
                                            new_arr[len - 1].unshift(od);
                                        }
                                        len--;
                                    }
                                }
                                return new_arr;
                            }
                            
                            function findDate(today, arr){
                                var len = arr.length;
                                var dat;
                                while(len > 0){
                                    dat = parseDate(arr[len - 1][1]);
                                    if(today == dat){
                                        return true;
                                    }
                                    len--;
                                }
                                return false;
                            }

                            function setDefault(today) { //nastavení tabulky do defaultní hodnoty
                                
                                var sec = document.getElementById("pripni");
                                if(document.getElementById("t_udalosti")){
                                    var elem = document.getElementById("t_udalosti");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                if(document.getElementById("not_exist")){
                                    var elem = document.getElementById("not_exist");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                var table = document.createElement('table');
                                table.setAttribute("id", "t_udalosti");
                                var tr = document.createElement('tr');
                                var th = document.createElement('th');
                                th.setAttribute("id", "t_head");
                                var text = document.createTextNode(today);
                                th.appendChild(text);
                                tr.appendChild(th);
                                table.appendChild(tr);
                                for (var i = 6; i <= 22; i++) {
                                    tr = document.createElement('tr');
                                    tr.setAttribute("id", i);
                                    var td1 = document.createElement('td');
                                    var td2 = document.createElement('td');
                                    var str1 = String(i);
                                    var str2 = ":00";
                                    var res = str1.concat(str2);
                                    var text1 = document.createTextNode(res);
                                    str1 = String(i);
                                    str2 = "1";
                                    res = str1.concat(str2);
                                    td2.setAttribute("id", res);
                                    td1.appendChild(text1);
                                    tr.appendChild(td1);
                                    tr.appendChild(td2);
                                    table.appendChild(tr);
                                }
                                sec.appendChild(table);
                            }
                            
                            function setNewT() { //nastavení tabulky do defaultní hodnoty
                                //var elem = document.getElementById("t_udalosti");
                                var sec = document.getElementById("pripni");
                                //elem.outerHTML = "";
                                //delete elem;
                                var table = document.createElement('table');
                                table.setAttribute("id", "t_udalosti");
                                var tr = document.createElement('tr');
                                var th = document.createElement('th');
                                th.setAttribute("id", "t_head");
                                var text = document.createTextNode("");
                                th.appendChild(text);
                                tr.appendChild(th);
                                table.appendChild(tr);
                                for (var i = 6; i <= 22; i++) {
                                    tr = document.createElement('tr');
                                    tr.setAttribute("id", i);
                                    var td1 = document.createElement('td');
                                    var td2 = document.createElement('td');
                                    var str1 = String(i);
                                    var str2 = ":00";
                                    var res = str1.concat(str2);
                                    var text1 = document.createTextNode(res);
                                    str1 = String(i);
                                    str2 = "1";
                                    res = str1.concat(str2);
                                    td2.setAttribute("id", res);
                                    td1.appendChild(text1);
                                    tr.appendChild(td1);
                                    tr.appendChild(td2);
                                    table.appendChild(tr);
                                }
                                sec.appendChild(table);
                            }

                            function deleteItem(arr, index) {
                                arr.splice(index, 1);
                                return arr;
                            }

                            function parseDate(datum) {
                                //alert(datum);
                                var i = 0;
                                var result = [""];
                                while (datum[i] != ' ') {
                                    //  alert(datum);
                                    result += datum[i];
                                    i++;
                                }
                                return result; //vrátí datum upravený pro porovnání
                                //alert(result);
                            }

                            function parseHour(dt) {
                                var i = 0;
                                var result = "";
                                while (dt[i]) {
                                    if (dt[i] == ' ') {
                                        if (dt[i + 1] == "0") {
                                            result = dt[i + 2];
                                        } else {
                                            result = dt[i + 1];
                                            result += dt[i + 2];
                                        }
                                        break;
                                    }
                                    i++;
                                }
                                return result; //vrací hodiny
                            }

                            function elementIsEmpty(el) {
                                return (/^(\s|&nbsp;)*$/.test(el.innerHTML));
                            }

                            function setEvent1(p_sl, items) {
                                var delka = items.length;
                                var i = 0;
                                var navrt = 0;
                                var od = items[0][0];
                                var ddo = items[0][1];
                                var name = items[0][2];
                                var id_detail = items[0][5];
                                var p_slouceni = (parseInt(ddo) - parseInt(od));
                                var navrat = jeVolno(od, p_slouceni, p_sl);

                                if (navrat !== false) {
                                    var str1 = String(od);
                                    var str2 = navrat;
                                    var res = str1.concat(str2);
                                    var pom = document.getElementById(res);
                                    //spoj = rozdil + 1;
                                    var id = document.getElementById(od).id;
                                    var para = document.createElement("div");
                                    var odkaz = document.createElement("a");
                                    var zalomeni = document.createElement("br");
                                    var text12 = document.createTextNode("Detail");
                                    odkaz.setAttribute('href', '<?php echo $_SERVER['ROOT_URL']?>/pages/rezervace_d.php?action=upravit&rezerv_id=' + id_detail + '');
                                    //para.setAttribute("id", "event");
                                    pom.innerHTML = name;
                                    pom.rowSpan = p_slouceni;
                                    pom.appendChild(zalomeni);
                                    <?php
                                    if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
                                      echo 'odkaz.appendChild(text12);
                                            pom.appendChild(odkaz);';
                                    ?>
                                    items.shift(); //posunutí pole, odstranení prvku, který jsem vložil
                                    navrt = items.length;
                                    smaz(od, p_slouceni, navrat);
                                } else {
                                    var i = 6;
                                    p_sl++;
                                    while (i <= 22) {
                                        var str1 = String(i);
                                        var str2 = p_sl;
                                        var res = str1.concat(str2);
                                        var parrent = document.getElementById(i);
                                        var para = document.createElement("td");
                                        parrent.appendChild(para);
                                        para.setAttribute("id", res);
                                        i++;
                                    }
                                }

                                var delka = parseInt(items.length);
                                if (delka != 0) {
                                    navrt = setEvent1(p_sl, items);// rekurze
                                }
                                return navrt;
                            }

                            function nastavHlavicku() {
                                var radky = document.getElementById("t_udalosti").rows.length;

                                var vysledek = 0;
                                var x;
                                for (var j = 0; j < radky; j++) {

                                    x = document.getElementById("t_udalosti").rows[j].cells.length;
                                    if (x > vysledek) {
                                        vysledek = x;
                                    }
                                }
                                document.getElementById("t_head").colSpan = vysledek;
                            }



                            function smaz(pozice, p_spojeni, c_sloupce) {
                                var zarazka = parseInt(pozice) + parseInt(p_spojeni);
                                var poz = parseInt(pozice) + 1; //posunutí pozice na prvek, který budu mazat
                                while (poz < zarazka) { //odstranění spojených buňek
                                    var str1 = String(poz);
                                    var str2 = String(c_sloupce);
                                    var res = str1.concat(str2);
                                    var element = document.getElementById(res);
                                    element.outerHTML = "";
                                    delete element;
                                    poz++;
                                }
                            }

                            function jeVolno(pozice, p_spojeni, p_sloupcu) {
                                var i = 1;
                                var j = 1;
                                var pom = 0;
                                var pod_pozice = 1;
                                var counter = 0;
                                var puvodni_pozice = pozice;
                                while (j <= p_sloupcu) {
                                    pozice = puvodni_pozice;
                                    i = 1;
                                    //pod_pozice++;
                                    while (i <= p_spojeni) {
                                        var str1 = String(pozice);
                                        var str2 = String(pod_pozice);
                                        var res = str1.concat(str2);
                                        var parrent = document.getElementById(pozice);
                                        var prvek = document.getElementById(res);
                                        //alert(res);
                                        if (prvek) {
                                            if (elementIsEmpty(prvek)) {
                                                pom = 1;
                                                counter++;
                                            } else {
                                                pom = 0;
                                                break;
                                            }
                                        } else {
                                            pozice++;
                                            i++;
                                            continue; //pokračuj když prvek neexistuje v prohledávání v dalším sloupčku
                                        }
                                        pozice++;
                                        i++;
                                        if (counter == p_spojeni) { //jakmile mám 3 volné místa okamžitě vracím true nepokračuji dál
                                            return pod_pozice;
                                        }
                                    }
                                    if (pom == 0 && j == p_sloupcu) { //aby po prvním cyklu nedošlo k ukončení, musíme pokračovat abychom prošli všechny sloupce
                                        return false;
                                    }
                                    pod_pozice++;
                                    j++;
                                }
                                return false;
                            }

                            function fromDB() {
                                $.ajax({url: '<?php echo $_SERVER['ROOT_URL']?>/pages/json.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        ucebna: document.getElementById("ucebny").options[document.getElementById("ucebny").selectedIndex].value,
                                        ucitel: document.getElementById("ucitele").options[document.getElementById("ucitele").selectedIndex].value,
                                        predmet: document.getElementById("predmety").options[document.getElementById("predmety").selectedIndex].value,
                                        ak_rok: document.getElementById("rok").options[document.getElementById("rok").selectedIndex].value,
                                        typ_id: document.getElementById("typ_id").options[document.getElementById("typ_id").selectedIndex].value
                                    },
                                    success: function (out) {
                                        if(out == "false"){
                                            if(document.getElementById("not_exist")){
                                        
                                            }
                                            else {
                                                if(document.getElementById("t_udalosti")){
                                                var elem = document.getElementById("t_udalosti");
                                                elem.outerHTML = "";
                                                delete elem;
                                            }
                                                var sec = document.getElementById("pripni");
                                                var div = document.createElement('div');
                                                var text = document.createTextNode("Pro požadovaný filtr nejsou v DB žádné výsledky");
                                                div.setAttribute("id", "not_exist");
                                                div.appendChild(text);
                                                sec.appendChild(div);
                                            }
                                        } else{
                                            show(out);
                                        }                  
                                    }
                                });
                            }
                            
                            function show(data){
                                var navrat;
                                //navrat = findDate(today, data);
                                var ucitel =  document.getElementById("ucebny").options[document.getElementById("ucebny").selectedIndex].value;
                                var ucebna =  document.getElementById("ucitele").options[document.getElementById("ucitele").selectedIndex].value;
                                var predmet =  document.getElementById("predmety").options[document.getElementById("predmety").selectedIndex].value;
                                     if(ucitel != "default" || ucitel != "default" || ucitel != "default"){
                                }
                                var den = $("#datepicker").datepicker('getDate').getDate();
                                if(den < 10){
                                    den = '0'+ den;
                                }
                                var mesic = $("#datepicker").datepicker('getDate').getMonth() + 1;
                                var rok = $("#datepicker").datepicker('getDate').getFullYear();
                                
                                var vrat;
                                var p_sl = 1;
                                var today = rok + "-" + mesic + "-" + den;
                                navrat = findDate(today, data);
                                if(navrat == true){
                                    setDefault(today);
                                    vrat = setDate(today, data);
                                    setEvent1(p_sl, vrat);
                                    nastavHlavicku();
                                } else {
                                    if(document.getElementById("not_exist")){
                                        
                                    }
                                    else {
                                        if(document.getElementById("t_udalosti")){
                                    var elem = document.getElementById("t_udalosti");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                    var sec = document.getElementById("pripni");
                                    var div = document.createElement('div');
                                    var text = document.createTextNode("Pro požadovaný filtr nejsou v DB žádné výsledky");
                                    div.setAttribute("id", "not_exist");
                                    div.appendChild(text);
                                    sec.appendChild(div);
                                }
                                }
                            
                            }
            
            function show_filter(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                } else if (i == 2){
                    document.getElementById("dis1").style.display = "block";
                    document.getElementById("butt2").innerHTML = "Skrýt";
                    document.getElementById("butt2").setAttribute("onClick", "discard(2)");
                } else if (i == 3){
                    document.getElementById("dis2").style.display = "block";
                    document.getElementById("butt3").innerHTML = "Skrýt";
                    document.getElementById("butt3").setAttribute("onClick", "discard(3)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Učebna filtr";
                    document.getElementById("butt1").setAttribute("onClick", "show_filter(1)");
                } else if (i == 2){
                    document.getElementById("dis1").style.display = "none";
                    document.getElementById("butt2").innerHTML = "Předmět filtr";
                    document.getElementById("butt2").setAttribute("onClick", "show_filter(2)");
                } else if (i == 3){
                    document.getElementById("dis2").style.display = "none";
                    document.getElementById("butt3").innerHTML = "Učitel filtr";
                    document.getElementById("butt3").setAttribute("onClick", "show_filter(3)");
                }
            }
                        </script>
                <section>
                    <button class="button1" id="butt1" onclick="show_filter(1)">Učebna filtr</button>
                    <button class="button1" id="butt2" onclick="show_filter(2)">Předmět filtr</button>
                    <button class="button1" id="butt3" onclick="show_filter(3)">Učitel filtr</button>
                    <div id="dis">
                    <select class="sl_style" id="ucebny">
                        <option value="default" >Vyberte učebnu</option>
                        <?php
                        $result = get_ucebny();
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                echo '<option value="' . $row["ucebna_id"] .'">' . $row["budova"] . $row["patro"] . $row["cislo_mistnosti"] . '</option>';
                            }
                        } else
                            echo "0 results";
                        ?>
                    </select>
                    </div>
                    <div id="dis2">
                    <select class="sl_style" id="ucitele">
                        <option value="default">Vyberte učitele</option>
                        <?php
                        $result = get_akademiky(); 
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                echo '<option value="' . $row["uziv_cislo"] . '">' . $row["jmeno"] . '</option>';
                            }
                        } else
                            echo "0 results";
                        ?>
                    </select>
                        </div>
                     <div id="dis1">
                    <select class="sl_style" id="predmety">
                        <option value="default">Vyberte předmět</option>
                        <?php
                        $result = get_predmety();
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                echo '<option value="' . $row["zkratka_predmet"] . '">' . $row["zkratka_predmet"] . '</option>';
                            }
                        } else
                            echo "0 results";
                        ?>
                    </select>
                    <select class="sl_style" id="rok">
                        <option value="default">Vyberte rok</option>
                        <?php
                        $result = get_predmety();
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                echo '<option value="' . $row["ak_rok"] . '">' . $row["ak_rok"] . '</option>';
                            }
                        } else
                            echo "0 results";
                        ?>
                    </select>
                    <select class="sl_style" id="typ_id">
                        <option value="default">Vyberte typ</option>
                        <?php
                        $result = get_all_typ_akce();
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while ($row = $result->fetch_assoc()) {
                                echo '<option value="' . $row["typ_id"] . '">' . $row["nazev"] . '</option>';
                            }
                        } else
                            echo "0 results";
                        ?>
                    </select>
                         </div>
                    <br>
                         <div id="datepicker"> </div>
                         <input class = "button1" type="submit" onclick="fromDB()" value="Zobrazit rozvrh">
                    <div id="pripni">
                </div>
                </section>
                

                <div class="cleaner"></div>
            </article>
        </div>

   		<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>

    </body>
</html>
