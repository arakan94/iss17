<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require $_SERVER['ROOT_DIR'] . "/db/db.php";
require $_SERVER['ROOT_DIR'] . "/db/obor.php";
require $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
require $_SERVER['ROOT_DIR'] . "/db/ustav.php";

$conn = db_connect();

if (isset($_POST['login']))
{
	$uzivatel = new Uzivatel($_POST['login']);

	if ($_GET['action'] == "prihlaseni")
	{
		if (!$uzivatel->exists())
		{
			echo '<script>window.location.href = "prihlaseni.php?message=prihlaseni_neexistuje";</script>';
		}
		else if ($uzivatel->check_heslo($_POST["heslo"]))
		{
			session_start();
			$_SESSION['uziv_cislo'] = $uzivatel->get_id();
			$_SESSION['login'] = $uzivatel->login;
			$_SESSION['jmeno'] = $uzivatel->jmeno;
			$_SESSION['opravneni'] = $uzivatel->opravneni;
			$_SESSION['rocnik'] = $uzivatel->rocnik;
			$_SESSION['zkratka_ustav'] = $uzivatel->zkratka_ustav;
			$_SESSION['last_activity'] = time();
			$_SESSION['login_status'] = true;

			echo '<script>window.location.href = "' . $_SERVER['ROOT_URL'] . '/index.php?message=prihlaseni";</script>';
			exit();
		}
		else
		{
			echo "Nesprávné heslo";
		}
	}
}

if ($_GET['action'] == "odhlaseni")
{
	$_SESSION = array();
	session_destroy();
	echo '<script>window.location.href = "prihlaseni.php?message=odhlaseni";</script>';
}

if ($_GET['action'] == "auto_odhlaseni")
{
	$_SESSION = array();
	session_destroy();
	echo '<script>window.location.href = "prihlaseni.php?message=odhlaseni_neak";</script>';
}
?>
<!DOCTYPE html>


<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <script> 
            $( document ).ready(function() {
                if($("#hide")){
                    $("#hide").fadeTo(3000, 400).slideUp(400, function(){
                       $("#hide").slideUp(400);
                        });   
                }
            });
        </script>
    </head>
    <body>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php
		          include($_SERVER['ROOT_DIR'] . '/inc/menu.php');
            ?>
        </header>
        <div class="center">
            <article>
                <section >
                    <h2>Přihlášení uživatele</h2>
                    <div id = "zarovne">
                    <?php
                      if (isset($_SESSION['login_status']) && $_SESSION['login_status'] == true)
                  		{
                  			echo'<button class="button1" id="butt1" onclick="window.location.href=\'' .$_SERVER['PHP_SELF']. '?action=odhlaseni\'">Odhlásit se</button>';
                      }
                      else
                      {
		                    echo'<form action="'.$_SERVER['PHP_SELF'].'?action=prihlaseni" method="post">
			                      Login: <input type="text" name="login" value="" required>
			                      Heslo: <input type="password" name="heslo" value="" required>
			                      <input class="button1" type="submit" value="Přihlásit se">
		                    </form>';
                      }
                     ?>
                    </div>

                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
