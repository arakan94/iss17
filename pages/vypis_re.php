<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/rezervace.php";

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_re'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	<section >
		<h2>Výpis rezervací</h2>
		<table>
		<tr>
			<th>Název akce</th>
			<th>Typ akce</th>
			<th>Zkratka předmětu</th>
			<th>Ak. rok</th>
			<th>Učebna</th>
			<th>Od</th>
			<th>Do</th>
			<?php if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav'])) echo'<th colspan = "3">Akce</th>'; ?>
		</tr>
	<?php
		$result = get_all_rezervace();
		if ($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
        echo '<tr><td>' . $row["nazev"] . '</td>';
        echo '<td>' . $row["typ_akce_nazev"] . '</td>';
			  echo '<td>'.$row["zkratka_predmet"] . '</td><td>'.$row["ak_rok"] . '</td><td>'. $row['budova' ]. $row['patro'] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</td><td>'.$row["zacatek"] . '</td><td>'.$row["konec"] . '</td>';

			  if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
			  {
			  	echo '<td><a href="rezervace_d.php?action=upravit&rezerv_id=' .$row["rezerv_id"]. '&ak_rok='.$row["ak_rok"].'&typ_id='.$row["typ_id"].'"> Upravit </a></td> <td><a href="rezervace_d.php?action=odstranit&rezerv_id=' .$row["rezerv_id"]. '&nazev='.$row["nazev"].'"> Odstranit </a></td>';
			  	echo ' <td><a href="rezervace_d.php?action=upravit&rezerv_id=' .$row["rezerv_id"]. '&ak_rok='.$row["ak_rok"].'&typ_id='.$row["typ_id"].'"> Detail </a></td>';
			  }
			  echo '</tr>';
			}
		}
		else 
			echo "0 results";
	?>
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
