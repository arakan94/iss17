<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_uz'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
  <section >
	<button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>?action=akademik'">Akademici</button>
	<button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>?action=student'">Studenti</button>
	<button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>?action=admin'">Administrátoři</button>
	<button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>'">Všichni</button>

<?php
		if (!isset($_GET['action']))
		{
			echo '<h2>Výpis uživatelů';
			if (isset($_GET["zkratka_obor"])) echo ' - ' .$_GET["zkratka_ustav"];
			echo '</h2>';
			$result = get_uzivatele();
		}
		else if ($_GET['action'] == "akademik")
		{
			echo '<h2>Výpis akademiků';
			if (isset($_GET["zkratka_ustav"])) echo ' - ' .$_GET["zkratka_ustav"];
			echo '</h2>';
			$result = get_akademiky();
		}
		else if ($_GET['action'] == "student")
		{
			if (isset($_GET["zkratka_obor"]))
			{
				echo '<h2>Výpis studentů oboru '. $_GET["zkratka_obor"].'</h2>';
				$result = get_studenty_obor($_GET["zkratka_obor"]);
			}
			else
			{
				echo '<h2>Výpis studentů</h2>';
				$result = get_studenty();
			}
		}
		else if ($_GET['action'] == "admin")
		{
			echo '<h2>Výpis administrátorů</h2>';
			$result = get_spravce();
		}
?>

     <table>
      <tr>
   		<th>Login</th>
   		<th>Jméno</th>
      <th>Ústav</th>
      <th>Student</th>
   		<th colspan = "3">Akce</th>
   				
   	    </tr>
	<?php
		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
				if ($_GET["zkratka_ustav"] == $row["zkratka_ustav"] || !isset($_GET["zkratka_ustav"]) || $_GET['action'] != "akademik")
				{
					echo '<tr><td>'.$row["login"] . '</td><td>' . $row["jmeno"] . '</td><td>' . $row["zkratka_ustav"] . '</td><td>' . ($row["rocnik"] > 0 ? 'Ano' : 'Ne') . '</td>';

					if ($_SESSION['opravneni'] == 1) echo '<td><a href="uzivatel_d.php?action=upravit&uziv_cislo=' .$row["uziv_cislo"].'"> Upravit </a></td> <td><a href="uzivatel_d.php?action=odstranit&uziv_cislo=' .$row["uziv_cislo"]. '"> Odstranit </a></td>';

					echo '<td><a href="uzivatel_d.php?action=upravit&uziv_cislo=' .$row["uziv_cislo"]. '"> Detail </a></td></tr>';
			  }
			}
			echo '</table>';
		}
		else 
			echo "</table>0 results";
        
	?>
        
        
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
