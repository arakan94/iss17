<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_ta'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section >
           <h2>Výpis typů akcí</h2>
           <table>
            <tr>
   		<th>Název</th>
   		<th colspan = "3">Akce</th>
   				
   	    </tr>
	<?php
		$result = get_all_typ_akce();
		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row["nazev"] . '</td></td>';
			  if ($_SESSION['opravneni'] == 1) echo '<td><a href="typAkce_d.php?action=upravit&typ_id=' .$row["typ_id"]. '"> Upravit </a></td> <td><a href="typAkce_d.php?action=odstranit&typ_id=' .$row["typ_id"]. '"> Odstranit </a></td>';
			  echo '<td><a href="typAkce_d.php?action=upravit&typ_id=' .$row["typ_id"]. '"> Detail </a></td></tr>';
			}
		}
		else 
			echo "0 results";
	?>
        
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
