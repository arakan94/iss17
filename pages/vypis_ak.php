<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";

$conn = db_connect();
?>

<!DOCTYPE html>


<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_ak'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section>
        <button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>?action=all'">Všechny akademické roky</button>
        <button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>'">Pouze tento rok</button>

        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            Předmět: <input type="text" name="filtr_zkratka_predmet" value="<?php echo $_POST['filtr_zkratka_predmet']?>" style="
    width: 230px;
" required>
            <input type="submit" class="button1" value="Filtrovat">
          <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
        </form>

	<?php
		if (empty($_POST['filtr_zkratka_predmet']))
			if ($_GET['action'] == "all")
			{
				$all = true;
				$result = get_all_akce();
			}
			else
				$result = get_akce_rok(date("Y"));
		else
			if ($_POST['action'] == "all")
			{
				$all = true;
				$result = get_akce_predmet($_POST['filtr_zkratka_predmet']);
			}
			else
				$result = get_akce_predmet_rok($_POST['filtr_zkratka_predmet'], date("Y"));

		if ($all)
			echo '<h2>Výpis akcí</h2>';
		else
			echo '<h2>Výpis akcí - '.date("Y").'</h2>';

		echo'<table>
		<tr>
		<th>Zkratka předmětu</th>';

		if ($all) echo '<th>Ak. rok</th>';

		echo '<th>Název</th>
		<th>Popis</th>
		<th colspan = "3">Akce</th>
		</tr>';

		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row["zkratka_predmet"] . '</td>';
				if ($all) echo '<td>'.$row['ak_rok'].'</td>';
			  echo '<td>'.$row["nazev"] . '</td><td>' . $row["popis"] . '</td>';

			  if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
			  	echo '<td><a href="akce_d.php?action=upravit&zkratka_predmet=' .$row["zkratka_predmet"]. '&ak_rok='.$row["ak_rok"].'&typ_id='.$row["typ_id"].'"> Upravit </a></td> <td><a href="akce_d.php?action=odstranit&zkratka_predmet=' .$row["zkratka_predmet"]. '&ak_rok='.$row["ak_rok"].'&typ_id='.$row["typ_id"].'"> Odstranit </a></td>';

			  echo ' <td><a href="akce_d.php?action=upravit&zkratka_predmet=' .$row["zkratka_predmet"]. '&ak_rok='.$row["ak_rok"].'&typ_id='.$row["typ_id"].'"> Detail </a></td></tr>';
			}
		}
		else 
			echo "V databázi nejsou žádné akce.";
        
	?>
        
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
