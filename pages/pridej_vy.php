<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

if ($_SESSION['login_status'] != true)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_GET['ucebna_id']))
	$ucebna_id = $_GET['ucebna_id'];

if (isset($_POST['vyber_uc']))
{
      $pom = $_POST['vyber_uc'];
      $len = strlen($pom);
      $pole = array();
      $ii = 0;
      
      for($i = 0; $i < $len; $i++){ //parsování budovy, patra, č. učebny
          $pole[$ii] = $pole[$ii] . $pom[$i];
          if($pom[$i] == " "){
              $ii++;
          }
      }
          
     $result = get_ucebna_by_location($pole[0], $pole[1], $pole[2]);
		  if ($result->num_rows > 0) {
		      $row = $result->fetch_assoc();
		      $id_ucebny = $row["ucebna_id"];
		  } else
		      die("Učebna" . $_POST['vyber_uc'] . " nenalezena");

	$vybaveni = new Vybaveni(null, null);
	if ($vybaveni->exists())
	{
		echo '<script>window.location.href = "pridej_vy.php?message=exist";</script>';
		exit();
	}
	else
	{
		$vybaveni->nazev = $_POST['nazev'];
		$vybaveni->popis = $_POST["popis"];

		if ($_POST['typ'] == "new" && isset($_POST['novy_typ']))
			$vybaveni->typ = $_POST['novy_typ'];
		else
			$vybaveni->typ = $_POST['typ'];

		if ($vybaveni->add($id_ucebny))
			echo '<script>window.location.href = "pridej_vy.php?message=success";</script>';
		else
			echo '<script>window.location.href = "pridej_vy.php?message=failure";</script>';
		exit();
	}
}

?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });

	function typ_select()
	{
		var select = document.getElementById("typy");
		var field = document.getElementById("novy_typ_field");

		if (select.value == "new")
			field.style.display = "block";
		else
			field.style.display = "none";
	}

	function checkForm()
	{
		if (document.getElementById("ucebny").value == "default" ||
			document.getElementById("typy").value == "default")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else if (document.getElementById("typy").value == "new" && document.getElementById("novy_typ_field").value == "")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}		
		else
			return true;
	}

</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'pridej_hl'; $page1 = 'pridej_vy'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
           
        <section >
             <h2>Přidej vybavení</h2>
             <div id = "zarovne">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return checkForm()">
            Učebna*: <select name="vyber_uc" class="sl_style_add" id="ucebny">
            <option value="default">Vyberte učebnu</option>
                    <?php
                    $result = get_ucebny();
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            echo '<option value="'.$row["budova"].' '.$row["patro"].' '.$row["cislo_mistnosti"].'" '.($ucebna_id == $row['ucebna_id'] ? "selected" : "").'>' . $row["budova"].$row["patro"].str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT). '</option>';
                        }
                    } else
                        echo "V databázi nejsou žádné učebny.";
                    // u typ id zatím chybí db modul a u uziv_cislo přihlášení
                    ?> 
                </select>
		Název*:   <input type="text" name="nazev" value="" maxlength="50" required> <br>
        Typ*: <select name="typ" class="sl_style_add" id="typy" onchange="typ_select()">
                    <option value="default">Vyberte typ</option>
                    <option value="new">Nový typ</option>
				<?php   
					$res = get_typy_vybaveni();
					if ($res->num_rows > 0)
						while($row = $res->fetch_assoc())
						{
							echo '<option value="'.$row['typ'].'">'.$row['typ'].'</option>';
						}
				?>
                </select>
             <input type="text" name="novy_typ" value="" id="novy_typ_field"  maxlength="50" style="display:none"> <br>   
		Popis:   <input type="text" name="popis" value="" maxlength="250"> <br>
                <input class = "button1" type="submit" value="Vložit">
	</form>
                 </div>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
