<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_ob'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section >
           <h2>Výpis oborů</h2>
           <table>
            <tr>
   		<th>Zkratka</th>
   		<th>Název</th>
   		<th colspan = "5">Akce</th>
   				
   	    </tr>
	<?php
		$result = get_obory();
		if ($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row["zkratka_obor"] . "</td><td> " . $row["nazev"] . '</td>';

				echo '<td><a href=vypis_uz.php?action=student&zkratka_obor='.$row["zkratka_obor"].'>Zapsaní studenti</a></td>';

				if ($_SESSION['opravneni'] == 1)
					echo '<td><a href=vypis_pr.php?zkratka_obor='.$row["zkratka_obor"].'>Předměty</a></td>';

			  if ($_SESSION['opravneni'] == 1)
					echo '<td><a href="obor_d.php?action=upravit&zkratka_obor=' .$row["zkratka_obor"]. '&nazev='.$row["nazev"].'"> Upravit </a></td> <td><a href="obor_d.php?action=odstranit&zkratka_obor=' .$row["zkratka_obor"]. '&nazev='.$row["nazev"].'"> Odstranit </a></td><td><a href="obor_d.php?action=upravit&zkratka_obor=' .$row["zkratka_obor"]. '&nazev='.$row["nazev"].'"> Detail </a></td></tr>';
			}
		}
		else 
			echo "0 results";
	?>
        
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
