<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['budova']) && isset($_POST['patro']) && isset($_POST['cislo_m']))
{
	$ucebna = new Ucebna($_POST['budova'], $_POST['patro'], $_POST["cislo_m"]);

	if ($ucebna->exists())
	{
		echo '<script>window.location.href = "pridej_uc.php?message=exist";</script>';
		exit();
	}
	else
	{
		$ucebna->budova = $_POST['budova'];
		$ucebna->patro = $_POST['patro'];
		$ucebna->cislo_mistnosti = $_POST["cislo_m"];
		$ucebna->kapacita = $_POST["kapacita"];
		$ucebna->popis = $_POST["popis"];

		if ($ucebna->add())
			 echo '<script>window.location.href = "pridej_uc.php?message=success";</script>';
		else
			echo '<script>window.location.href = "pridej_uc.php?message=failure";</script>';
		exit();
	}
}

?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'pridej_hl'; $page1 = 'pridej_uc'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
           
        <section >
             <h2>Přidej učebnu</h2>
             <div id = "zarovne">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		Budova*: <input type="text" name="budova" value="" required maxlength="1"> <br>
		Patro*:   <input type="number" name="patro" value="" required max="99" min="-99"> <br>
    Číslo místnosti*:   <input type="number" name="cislo_m" required value="" min="0" max="99"> <br>
    Kapacita:   <input type="number" name="kapacita" value="" min="0" max="9999"> <br>
		Popis:   <input type="text" name="popis" value="" maxlength="250"> <br>
                <input class = "button1" type="submit" value="Vložit">
	</form>
                 </div>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
