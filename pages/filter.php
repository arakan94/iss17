<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/rezervace.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";

$conn = db_connect();
if(isset($_POST["ucitel"])){
    $uziv_id = $_POST["ucitel"];
}
if(isset($_POST["datum_od"]) && isset($_POST["cas_od"]) && isset($_POST["datum_do"]) && isset($_POST["cas_do"])){
    $datum_od = $_POST["datum_od"] . ' ' . $_POST["cas_od"];
    $datum_do = $_POST["datum_do"] . ' ' . $_POST["cas_do"];
    $id_ucebny = $_POST["ucebna_id"];
}
if(isset($_POST["typ"])){
    $typ = $_POST["typ"];
}

if(isset($_POST["predmet"])){
    $predmet = $_POST["predmet"];
}

$uziv_id1 = $_POST["zak"];
/*
$result = get_predmet($zkratka_predmet, $ak_rok);
$row = $result->fetch_assoc();
echo '<h2>Karta předmětu - ' . $row["nazev"] . '</h2>';
echo '<div class="text_l">';
echo '<b>Zkratka - </b>' . $row["zkratka_predmet"] . '</br>';
echo '<b>Akademický rok - </b>' . $row["ak_rok"] . '</br>';
*/
$i = 0;
$pole = [];
$pole1 = [];

if(!empty($uziv_id)) {
    $result = get_rezervace_garant($uziv_id);
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = 'Název: ' . $row["nazev"];
              $pole[$i][1] = 'Od: ' . $row["zacatek"];
              $pole[$i][2] = 'Do: ' . $row["konec"];
              $pole[$i][3] = 'Učebna: ' . $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
} else if(!empty($datum_od) && !empty($datum_do)){
    $ret = is_ucebna_volna($id_ucebny, $datum_od, $datum_do);
    $pole[0][0] = $ret;
} else if(!empty($typ)){
    $result = get_ucebny_by_vybaveni($typ);
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = $row["budova"];
              $pole[$i][1] = $row["patro"];
              $pole[$i][2] = $row["cislo_mistnosti"];
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
} else if(!empty($uziv_id1)) {
    $result = get_registrovane_predmety($uziv_id1); //dostanu registrované předměty a ty pak pošlu do registrací, aby mně to vrátilo registrace pro předměty 
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
              $pole1[$i][0] = $row["zkratka_predmet"];
              $pole1[$i][1] = $row["ak_rok"];
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
        $k = 0;
        for($j = 0; $j < count($pole1); $j++){
            $result1 = get_rezervace_predmet($pole1[$j][0], $pole1[$j][1]);
            if ($result1->num_rows > 0)
            {
                // output data of each row
                  while($row = $result1->fetch_assoc())
                {
                  $pole[$k][0] = 'Název: ' . $row["nazev"] .'<br> Typ: '. $row["typ_akce_nazev"] .'<br> Zkratka před. : '. $row["zkratka_predmet"] .'<br> Ak. rok: '. $row["ak_rok"] . '<br> Učebna: '. $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
                  $pole[$k][1] = $row["zacatek"];
                  $pole[$k][2] = $row["konec"];
                  $pole[$k][3] = $row["rezerv_id"];
                  $k++;
                }
            }
        }
} else if(!empty($predmet)){
    $result = get_predmety_roky($predmet);
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = $row["ak_rok"];
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
}
//print_r($pole);
$ajax = array($row["zkratka_predmet"], $row["zacatek"]);
echo json_encode($pole);

?>
