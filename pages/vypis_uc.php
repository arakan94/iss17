<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="../datetime/jquery.timepicker.css">
        <link rel="stylesheet" type="text/css" href="../datetime/lib/bootstrap-datepicker.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="../datetime/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="../datetime/lib/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../datepair/dist/datepair.js"></script>
        <script type="text/javascript" src="../datepair/dist/jquery.datepair.js"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
    
      function getTyp() {
                                $.ajax({url: 'filter.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        typ: document.getElementById("typy").options[document.getElementById("typy").selectedIndex].value
                                    },
                                    success: function (out) {
                                        if(out == "false"){
                                           
                                        } else{
                                            setNewT(out);
                                        }                  
                                    }
                                });
                            }
    
    function setDefault() { //nastavení tabulky do defaultní hodnoty
                                var sec = document.getElementById("pripni");
                                if(document.getElementById("t_udalosti")){
                                    var elem = document.getElementById("t_udalosti");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                if(document.getElementById("not_exist")){
                                    var elem = document.getElementById("not_exist");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                var table = document.createElement('table');
                                table.setAttribute("id", "t_udalosti");
                                var tr = document.createElement('tr');
                                var th = document.createElement('th');
                                th.setAttribute("id", "t_head");
                                var text = document.createTextNode("4.10.2017");
                                th.appendChild(text);
                                tr.appendChild(th);
                                table.appendChild(tr);
                                for (var i = 6; i <= 22; i++) {
                                    tr = document.createElement('tr');
                                    tr.setAttribute("id", i);
                                    var td1 = document.createElement('td');
                                    var td2 = document.createElement('td');
                                    var str1 = String(i);
                                    var str2 = ":00";
                                    var res = str1.concat(str2);
                                    var text1 = document.createTextNode(res);
                                    str1 = String(i);
                                    str2 = "1";
                                    res = str1.concat(str2);
                                    td2.setAttribute("id", res);
                                    td1.appendChild(text1);
                                    tr.appendChild(td1);
                                    tr.appendChild(td2);
                                    table.appendChild(tr);
                                }
                                sec.appendChild(table);
                            }
                            
                            function setNewT(data) { //nastavení tabulky do defaultní hodnoty
                                if(document.getElementById("table_h")){
                                    var elem = document.getElementById("table_h");
                                    elem.outerHTML = "";
                                    delete elem;
                                }
                                var sec = document.getElementById("pripni");
                                var table = document.createElement('table');
                                table.setAttribute("id", "table_h");
                                
                                var tr = document.createElement('tr');
                                var th = document.createElement('th');
                                var th1 = document.createElement('th');
                                var th2 = document.createElement('th');
                                var text = document.createTextNode("Budova");
                                var text1 = document.createTextNode("Patro");
                                var text2 = document.createTextNode("Č. místnosti");
                                th.appendChild(text);
                                th1.appendChild(text1);
                                th2.appendChild(text2);
                                tr.appendChild(th);
                                tr.appendChild(th1);
                                tr.appendChild(th2);
                                table.appendChild(tr);
                                var len = data.length;
                                for (var i = 0; i < len; i++) {
                                    tr = document.createElement('tr');
                                    var td1 = document.createElement('td');
                                    var td2 = document.createElement('td');
                                    var td3 = document.createElement('td');
                                    var text1 = document.createTextNode(data[0][0]);
                                    var text2 = document.createTextNode(data[0][1]);
                                    var text3 = document.createTextNode(data[0][2]);
                                    td1.appendChild(text1);
                                    td2.appendChild(text2);
                                    td3.appendChild(text3);
                                    tr.appendChild(td1);
                                    tr.appendChild(td2);
                                    tr.appendChild(td3);
                                    table.appendChild(tr);
                                }
                                sec.appendChild(table);
                            }

  function checkDateTime()
  {
  	if (document.getElementById("datum_od").value == "" ||
  	    document.getElementById("cas_od").value == "" ||
  	    document.getElementById("cas_do").value == "" ||
  	    document.getElementById("datum_do").value == "")
  	{
  		alert("Není vyplněno časové rozpětí");
  		return false;
  	}
  }
</script>
</head>

<body>
	<header id="hlavicka">
		<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_uc'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>

	<div class="center">
	<article>
		<section>

          <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" onsubmit="return checkDateTime();">
            <div id = "zarovne">
            <?php
            echo'<div id="datepairExample">
              Od:
              <input type="text" id="datum_od" name="filtr_dat_od" class="date start" value="'.$_GET['filtr_dat_od'].'"/>
              <input type="text" id="cas_od" name="filtr_cas_od" class="time start" value="'.$_GET['filtr_cas_od'].'" />
              <br/>
              Do:
              <input type="text" id="datum_do" name="filtr_dat_do" class="date end" value="'.$_GET['filtr_dat_od'].'" />
              <input type="text" id="cas_do" name="filtr_cas_do" class="time end" value="'.$_GET['filtr_cas_do'].'" />';
            ?>
						</div>
            <script>
                $('#datepairExample .time').timepicker({
                    'showDuration': true,
                    'timeFormat': 'H:i:i',
                    'step': '60',
                    'minTime': '7:00',
                    'maxTime': '22:00'
                });

                $('#datepairExample .date').datepicker({
                    'format': 'yyyy-m-d',
                    'autoclose': true
                });

                $('#datepairExample').datepair();
            </script>
            <input type="submit" class="button1" value="Zobrazit volné učebny">
            <input type="hidden" name="action" value="volne_ucebny">
            </div>
          </form>

			<select name="zkratka_predmet" class="sl_style_add" id="typy">
				<option value="default">Vyberte typ</option>
				<?php   
					$res = get_typy_vybaveni();
					if ($res->num_rows > 0)
						while($row = $res->fetch_assoc())
						{
							echo '<option value="'.$row['typ'].'">'.$row['typ'].'</option>';
						}
				?>
			</select>
			<button class="button1" id="butt1" onclick="getTyp()">Typ filtr</button>

			<div id="pripni">

<?php

	$result = get_ucebny();

	if (empty($_GET['filtr_cas_od']) || empty($_GET['filtr_cas_do']) || empty($_GET['filtr_dat_od']) || empty($_GET['filtr_dat_do'])){
		$volne_filtr = false;
	} else {
		$do = $_GET['filtr_dat_od'] .' ' . $_GET['filtr_cas_do'];
		$od = $_GET['filtr_dat_od'] .' ' . $_GET['filtr_cas_od'];
		$volne_filtr = true;
	}

	if ($result->num_rows > 0)
	{
		if ($volne_filtr)
		{
			echo '<h2>Výpis volných učeben</h2>';
			echo '<p>Interval: '.$od.' - '.$do.'</p>';
		}
		echo '<h2>Výpis učeben</h2>';
		echo '<table id="table_h">
				<tr>
					<th>Učebna</th>
					<th>Kapacita</th>
					<th colspan = "3">Akce</th>
				</tr>';

		while($row = $result->fetch_assoc())
		{
			if ($volne_filtr == false || is_ucebna_volna($row["ucebna_id"], $od, $do))
			{
				echo '<tr><td>'.$row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) . '</td>';	

				echo '<td>'. $row['kapacita'] .'</td>';

				if ($_SESSION['opravneni'] == 1)
					echo '<td><a href="ucebna_d.php?action=upravit&budova=' .$row["budova"]. '&patro='.$row["patro"].'&cislo_mistnosti='.$row["cislo_mistnosti"].'&ucebna_id='.$row["ucebna_id"].'"> Upravit </a></td> <td><a href="ucebna_d.php?action=odstranit&budova=' .$row["budova"]. '&patro='.$row["patro"].'&cislo_mistnosti='.$row["cislo_mistnosti"].'&ucebna_id='.$row["ucebna_id"].'"> Odstranit </a></td>';

				echo '<td><a href="ucebna_d.php?action=upravit&budova=' .$row["budova"]. '&patro='.$row["patro"].'&cislo_mistnosti='.$row["cislo_mistnosti"].'&ucebna_id='.$row["ucebna_id"].'"> Detail </a></td></tr>';
			}
		}

		echo '</table>';
	}
	else 
		echo "Nebyly vloženy žádné učebny.";

?>
        
                 </div>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
