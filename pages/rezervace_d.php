<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

/* Datepair, datetime library. Copyright (c) 2014 Jon Thornton.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/rezervace.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['rezerv_id']))
{
	$split2 = explode("|", $_POST['vyber_akci']);
	$zacatek = $_POST["zacatek_datum"] .' ' . $_POST["zacatek_cas"];
	$konec = $_POST["zacatek_datum"] .' ' . $_POST["konec_cas"];

	$rezervace = new Rezervace($_POST['rezerv_id']);
	if ($rezervace->exists() == false)
		die("Rezervace s ID ".$_POST['rezerv_id']." neexistuje.");
	
	if ($_POST['action'] == "upravit")
	{
		if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
			die("Nemáte oprávnění k úpravám.");

		$rezervace->zkratka_predmet = $split2[0];
		$rezervace->ak_rok = $split2[1];
		$rezervace->typ_id = $split2[2];
		$rezervace->ucebna_id = $_POST["ucebna_id"];
		$rezervace->zacatek = $zacatek;
		$rezervace->konec = $konec;
		$rezervace->poznamka = $_POST["poznamka"];
		$rezervace->uziv_cislo = $_POST["uziv_cislo"];

		$retu = $rezervace->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_re.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_re.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
			die("Nemáte oprávnění k úpravám.");

		$retu = $rezervace->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_re.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_re.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="../datetime/jquery.timepicker.css">
        <link rel="stylesheet" type="text/css" href="../datetime/lib/bootstrap-datepicker.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="../datetime/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="../datetime/lib/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../datepair/dist/datepair.js"></script>
        <script type="text/javascript" src="../datepair/dist/jquery.datepair.js"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "block";
                    document.getElementById("butt2").innerHTML = "Skrýt";
                    document.getElementById("butt2").setAttribute("onClick", "discard(2)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "none";
                    document.getElementById("butt2").innerHTML = "Přidej vybavení";
                    document.getElementById("butt2").setAttribute("onClick", "show(2)");
                }
            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_re'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
if (isset($_POST['rezerv_id']))
	$rezerv_id = $_POST['rezerv_id'];
else
	$rezerv_id = $_GET['rezerv_id'];

$result = get_rezervace($rezerv_id);

if ($result->num_rows < 1)
	die("Rezervace s ID ".$rezerv_id." neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta rezervace - ' . $row["nazev"] .'</h2>';
echo '<b>Zodpovědná osoba - </b>' . $row["login"] . ' ('.$row["jmeno"].', '.$row['zkratka_ustav'].')<br/>';
echo '<b>Zkratka předmět - </b>' . $row["zkratka_predmet"] . '<br/>';
echo '<b>Typ - </b>' . $row["typ_akce_nazev"] . '<br/>';
echo '<b>Učebna - </b>' . $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) . '<br/>';
echo '<b>Začátek - </b>' . $row["zacatek"] . '<br/>';
echo '<b>Konec - </b>' . $row["konec"] . '<br/>';
echo '<b>Poznámka</b><br/>' .$row["poznamka"] .'<br/>';
echo '<div class="text_l">';
$split = explode(" ", $row["zacatek"]);
$split1 = explode(" ", $row["konec"]);

$split[1] = substr($split[1], 0, strrpos($split[1], ':'));
$split1[1] = substr($split1[1], 0, strrpos($split1[1], ':'));
?>

<script>
	function checkForm()
	{
		if (document.getElementById("akce_select").value == "default" ||
			document.getElementById("ucebna_select").value == "default" ||
			document.getElementById("datum_od").value == "" ||
			document.getElementById("cas_od").value == "" ||
			document.getElementById("cas_do").value == ""
			//|| document.getElementById("datum_do").value == ""
			)
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else
			return true;
	}


	function fromDB()
	{
		if (checkForm() == false)
			return false;

		var d_od = "<?php echo $split[0]; ?>";
		var c_od = "<?php echo $split[1] ?>";
		var c_do = "<?php echo $split1[1]; ?>";
		var d_do = "<?php echo $split1[0]; ?>";

		if (document.getElementById("datum_od").value == d_od &&
			document.getElementById("cas_od").value == c_od &&
			document.getElementById("cas_do").value == c_do &&
			document.getElementById("datum_do").value == d_do)
		{
			document.getElementById("upravit_form").submit();
			return;
		}

		$.ajax({url: 'filter.php',
		    type: 'POST',
		    dataType: "json",
		    data: {
		        datum_od: document.getElementById("datum_od").value,
		        cas_od: document.getElementById("cas_od").value,
		        cas_do: document.getElementById("cas_do").value,
		        datum_do: document.getElementById("datum_do").value,
		        ucebna_id: document.getElementById("ucebna_select").value
		    },
		    success: function (out) {
		        if(out[0][0] == true){
		            document.getElementById("upravit_form").submit();
		        } else {
		            alert("Učebna není volná v zadaném termínu.");
		        }                  
		    }
		});
	}
</script>
			</div>
			<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return confirm('Opravdu chcete zrušit rezervaci?');">
				<input type="hidden" name="rezerv_id" value="<?php echo $row["rezerv_id"]; ?>">
				<input type="hidden" name="action" value="odstranit">
				<input class="button1" type="submit" value="Odstranit">
			</form>
		<div id = "zarovne">
		<div id="dis">
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="upravit_form" method="post">

	Zodpovědná osoba*: <select name="uziv_cislo" class="sl_style_add" id="garant">
		<?php
			$result_ak = get_akademiky();
			if ($result_ak->num_rows > 0) {
				while ($row_ak = $result_ak->fetch_assoc())
				{
              echo '<option';
              if ($row["uziv_cislo"] == $row_ak["uziv_cislo"]) echo ' selected';
              echo ' value="'.$row_ak["uziv_cislo"].'">'.$row_ak["login"].' ('.$row_ak["jmeno"].', '.$row_ak['zkratka_ustav'].')</option>';
				}
			}
			else
				echo "Databáze neobsahuje žádné akademiky.";
		?> 
			</select>

			Vyberte akci*: <select name="vyber_akci" class="sl_style_add" id="akce_select">
			<option value="default">Vyberte akci</option>
			<?php
			$result1 = get_all_akce();
			if ($result1->num_rows > 0)
			{
				while ($row1 = $result1->fetch_assoc())
				{
					echo '<option value="'.$row1["zkratka_predmet"].'|'.$row1["ak_rok"].'|'.$row1["typ_id"].'" ';

					if ($row1["zkratka_predmet"] == $row["zkratka_predmet"] && $row1["ak_rok"] == $row["ak_rok"] && $row1["typ_id"] == $row["typ_id"])
						echo 'selected="selected"';

					echo '>' . $row1["nazev"] . '</option>';
				}
			}
			else
				echo "Databáze neobsahuje žádné akce.";
			?>
			</select>
			Vyberte učebnu*: <select name="ucebna_id" class="sl_style_add" id="ucebna_select">
			<option value="default">Vyberte učebnu</option>
			<?php
			$result1 = get_ucebny();
			if ($result1->num_rows > 0)
				while ($row1 = $result1->fetch_assoc())
				{
					echo '<option value="'.$row1["ucebna_id"].'" ';

					if ($row1["ucebna_id"] == $row["ucebna_id"])
						echo 'selected="selected"';

					echo '>' . $row1["budova"] . $row1["patro"] . str_pad($row1['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</option>';
				}
			else
				echo "Databáze neobsahuje žádné učebny";
			?> 
			</select>
			<p id="datepairExample">
					Od:
					<input type="text" id="datum_od" value="<?php echo $split[0]; ?>" name="zacatek_datum" class="date start" />
					<input type="text" id="cas_od" value="<?php echo $split[1]; ?>" name="zacatek_cas" class="time start" />
					<br/>
					Do:
					<input type="text" id="datum_do" disabled="disabled" value="<?php echo $split1[0]; ?>" name="konec_datum" class="date end" />
					<input type="text" id="cas_do" value="<?php echo $split1[1]; ?>" name="konec_cas" class="time end" />
			</p>
			<script>
					// initialize input widgets first
					$('#datepairExample .time').timepicker({
						  'showDuration': true,
						  'timeFormat': 'H:i',
						  'step': '60',
						  'minTime': '7:00',
						  'maxTime': '22:00'
					});

					$('#datepairExample .date').datepicker({
						  'format': 'yyyy-m-d',
						  'autoclose': true
					});

					// initialize datepair
					$('#datepairExample').datepair();
			</script>
			Poznámka:   <input type="text" name="poznamka" value="<?php echo $row["poznamka"]; ?>"> <br>
			<input type="hidden" name="rezerv_id" value="<?php echo $row["rezerv_id"]; ?>">
<?php
    echo '<input type="hidden" name="action" value="upravit">';
?>
</form>
<button class = "button1" id="upravit_submit" onclick="fromDB();">Upravit</button>
</div>
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
