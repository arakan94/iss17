<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['vybav_id']) && isset($_POST['ucebna_id'])) {

	$vybav = new Vybaveni($_POST['ucebna_id'], $_POST['vybav_id']);
	if ($vybav->exists() == false)
		die("Vybavní s ID ".$_POST['vybav_id']." v učebně ID ".$_POST['ucebna_id']." neexistuje.");

	if ($_POST['action'] == "upravit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$vybav->nazev = $_POST['nazev'];
		$vybav->typ = $_POST['typ'];
		$vybav->popis = $_POST['popis'];

		$retu = $vybav->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_vy.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_vy.php?message=edit";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$retu = $vybav->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_vy.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_vy.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }
	function typ_select()
	{
		var select = document.getElementById("typy");
		var field = document.getElementById("novy_typ_field");

		if (select.value == "new")
			field.style.display = "block";
		else
			field.style.display = "none";
	}

	function checkForm()
	{
		if (document.getElementById("typy").value == "new" && document.getElementById("novy_typ_field").value == "")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}		
		else
			return true;
	}
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_vy'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php

if (isset($_POST['vybav_id']) && isset($_POST['ucebna_id'])) {
	$vybav_id = $_POST['vybav_id'];
	$ucebna_id = $_POST['ucebna_id'];
} else {
	$vybav_id = $_GET['vybav_id'];
	$ucebna_id = $_GET['ucebna_id'];
}

$result = get_vybaveni($ucebna_id, $vybav_id);
if ($result->num_rows < 1)
	die("Vybavení ID ".$vybav_id." v učebně s ID ".$ucebna_id." neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta vybavení</h2>';
echo '<div class="text_l">';
echo '<b>Název - </b>' . $row["nazev"] . '<br/>';
echo '<b>Typ - </b>' . $row["typ"] . '<br/>';
echo '<b>Popis - </b>' . $row["popis"] . '<br/>';
?>
                    </div>
                    <?php
                    if ($_SESSION['opravneni'] == 1)
                    {
                    echo '<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
                    <form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return confirm(\'Do you really want to submit the form?\');">
                        <input type="hidden" name="vybav_id" value="'.$row["vybav_id"].'">
                        <input type="hidden" name="ucebna_id" value="'.$row["ucebna_id"].'">
                        <input type="hidden" name="action" value="odstranit">
                        <input class="button1" type="submit" value="Odstranit">
                    </form>
                    <div id = "zarovne">
                    <form action="'.$_SERVER['PHP_SELF'].'" method="post" id = "dis" onsubmit="return checkForm()">
                        Název*:   <input type="text" name="nazev" value="'.$row["nazev"].'" maxlength="50" required> <br>';
                        
                        echo 'Typ*: <select name="typ" class="sl_style_add" id="typy" onchange="typ_select()">
                    <option value="new">Nový typ</option>';

								$res = get_typy_vybaveni();
								if ($res->num_rows > 0)
									while($row_vy = $res->fetch_assoc())
									{
										echo '<option value="'.$row_vy['typ'].'"';
										if ($row_vy['typ'] == $row["typ"]) echo ' selected';
										echo '>'.$row_vy['typ'].'</option>';
									}

						          echo'</select>
						       <input type="text" name="novy_typ" value="" id="novy_typ_field"  maxlength="50" style="display:none"> <br>';
                        
                        //Typ*: <input type="text" name="typ" value="'.$row["typ"].'" maxlength="50" required> <br>
                        echo'Popis: <input type="text" name="popis" value="'.$row["popis"].'" maxlength="250"> <br>
                        <input type="hidden" name="vybav_id" value="'.$row["vybav_id"].'">
                        <input type="hidden" name="ucebna_id" value="'.$row["ucebna_id"].'">';

                    echo '<input type="hidden" name="action" value="upravit">
                    <input class="button1" type="submit" value="Upravit">';
                    }
?>
                    </form>
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
