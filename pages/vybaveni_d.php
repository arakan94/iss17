<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "block";
                    document.getElementById("butt2").innerHTML = "Skrýt";
                    document.getElementById("butt2").setAttribute("onClick", "discard(2)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "none";
                    document.getElementById("butt2").innerHTML = "Přidej vybavení";
                    document.getElementById("butt2").setAttribute("onClick", "show(2)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_uc'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
if (isset($_POST['ucebna_id'])) {
	$ucebna_id = $_POST['ucebna_id'];
} else {
	$ucebna_id = $_GET['ucebna_id'];
}

$res_ucebna = get_ucebna($ucebna_id);
if ($res_ucebna->num_rows < 1)
	die("Učebna s ID " .$ucebna_id. " neexistuje");

if (isset($_POST['typ'])) {
	$typ = $_POST['typ'];
} else if (isset($_GET['typ'])) {
	$typ = $_GET['typ'];
}

$row_uc = $res_ucebna->fetch_assoc();
echo '<h2>Karta učebny - ' . $row_uc["budova"] . $row_uc["patro"] . str_pad($row_uc['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</h2>';
echo '<div class="text_l">';

if (isset($typ))
	echo '<h4>Vybavení učebny - '.$typ.'</h4>';
else
	echo '<h4>Vybavení učebny</h4>';
?>
                    
                    <table>
                        <tr>
                            <th>Název</th>
                            <th>Typ</th>
                            <th>Popis</th>
                            <th colspan = "3">Akce</th>

                        </tr>
<?php

if (isset($typ))
	$result = get_vybaveni_typ($ucebna_id, $typ);
else
	$result = get_vybaveni_ucebna($ucebna_id);

if ($result->num_rows == 0)
{
	echo "</br>";
	if (isset($typ))
		echo "Tato učebna nemá přidané žádné vybavení typu." . $typ;
	else
		echo "Tato učebna nemá přidané žádné vybavení.";
}
else
{
	while($row = $result->fetch_assoc())
	{
		echo '<tr><td>'.$row["nazev"] . '</td><td>' . $row["typ"] . '</td><td>' . $row["popis"] . '</td>';

		if ($_SESSION['opravneni'] == 1) echo '<td><a href="vybav_d.php?action=upravit&ucebna_id='.$row["ucebna_id"].'&vybav_id='.$row["vybav_id"].'"> Upravit </a></td> 
		<td><a href="vybav_d.php?action=odstranit&ucebna_id='.$row["ucebna_id"].'&vybav_id='.$row["vybav_id"].'"> Odstranit </a></td>';

		echo '<td><a href="vybav_d.php?action=detail&ucebna_id='.$row["ucebna_id"].'&vybav_id='.$row["vybav_id"].'"> Detail </a></td></tr>';
	}
}
?>
                        </table>
                    </div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
