<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok']) && isset($_POST['typ_id']))
{
	$akce = new Akce($_POST['zkratka_predmet'], $_POST['ak_rok'], $_POST['typ_id']);

	if ($akce->exists())
	{
		echo '<script>window.location.href = "pridej_ak.php?message=exist";</script>';
		exit();
	}
	else
	{
		$akce->nazev = $_POST['nazev'];
		$akce->popis = $_POST['popis'];
		$akce->uziv_cislo = $_POST["uziv_cislo"];

		if ($akce->add())
			echo '<script>window.location.href = "pridej_ak.php?message=success";</script>';
		else
			echo '<script>window.location.href = "pridej_ak.php?message=failure";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });

     function getRok() {
                                $.ajax({url: 'filter.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        predmet: document.getElementById("predmety").options[document.getElementById("predmety").selectedIndex].value
                                    },
                                    success: function (out) {
                                        if(out == "false"){
                                           
                                        } else{
                                            setNewSel(out);
                                        }                  
                                    }
                                });
                            }
    
    function setNewSel(data) {
			var len = data.length;
			var select= document.getElementById('ak_rok');

			for (var i = select.length-1; i >=0; i--)
			{
				select.remove(i);
			}

			for (var j = 0; j < len; j++){
				var option1 = document.createElement('option');
				option1.value = data[j];
				option1.text = data[j];
				select.appendChild(option1);
			}
   }

	function checkForm()
	{
		if (document.getElementById("predmety").value == "default" ||
			document.getElementById("ak_rok").value == "default" ||
			document.getElementById("typ_akce").value == "default")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else
			return true;
	}
</script>
    </head>
    <body>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
<?php
	$page = 'pridej_hl';
	$page1 = 'pridej_ak';
	include( $_SERVER['ROOT_DIR'] . '/inc/menu.php');
?>
        </header>
        <div class="center">
            <article>
                <section>
                    <h2>Přidej akci</h2>
                    <div id = "zarovne">
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return checkForm();">
                        Zkratka*: <select name="zkratka_predmet" class="sl_style_add" id="predmety" onchange="getRok()">
                        <option value="default">Vyberte předmět</option>
                                <?php
                                $result = get_predmety_zkratka();
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["zkratka_predmet"].'">' . $row["zkratka_predmet"] . '</option>';
                                    }
                                } else
                                    echo "Databáze neobsahuje žádné předměty,";
                                ?>
                            </select>
                            Akademický rok*: <select name="ak_rok" class="sl_style_add" id="ak_rok">
                            <option value="default">Vyberte nejprve předmět</option>
                                </select>
                            Typ*: <select name="typ_id" class="sl_style_add" id="typ_akce">
                            <option value="default">Vyberte typ akce</option>
                                    <?php
                                    $result = get_all_typ_akce();
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_assoc()) {
                                            echo '<option value="'.$row["typ_id"].'">' . $row["nazev"] . '</option>';
                                        }
                                    } else
                                        echo "Databáze neobsahuje žádné typy akcí.";
                                    ?> 
                                </select>
                            Garant*: <select name="uziv_cislo" class="sl_style_add" id="garant">
                            <?php
				                      if (!empty($_SESSION['zkratka_ustav']))
				                      	echo '<option value="'.$_SESSION['uziv_cislo'].'">'.$_SESSION['jmeno'].' (já)</option>';
				                      else
				                      	echo '<option value="default">Vyberte garanta</option>';

                                $result = get_akademiky();
                                if ($result->num_rows > 0) {
		                              while ($row = $result->fetch_assoc())
		                              {
		                              	if ($row["uziv_cislo"] != $_SESSION['uziv_cislo'])
		                                  echo '<option value="'.$row["uziv_cislo"].'">'.$row["login"].' ('.$row["jmeno"].', '.$row['zkratka_ustav'].')</option>';
		                              }
                                }
                                else
                                	echo "Databáze neobsahuje žádné akademiky.";
                            ?> 
                                </select>
                            Název*: <input type="text" name="nazev" value="" maxlength="50" required> <br>
                            Popis:  <input type="text" name="popis" value="" maxlength="250"> <br>
                            <input class = "button1" type="submit" value="Vložit">
                        </form>
                    </div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
