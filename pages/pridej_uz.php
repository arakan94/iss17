<?php
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require $_SERVER['ROOT_DIR'] . "/db/db.php";
require $_SERVER['ROOT_DIR'] . "/db/obor.php";
require $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
require $_SERVER['ROOT_DIR'] . "/db/ustav.php";

if ($_SESSION['login_status'] != true)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");


$conn = db_connect();

if (isset($_POST['login']))
{
	$uzivatel = new Uzivatel($_POST['login']);

	if ($_GET['action'] == "registrace")
	{
		if ($uzivatel->exists())
		{
			echo '<script>window.location.href = "pridej_uz.php?message=registrace_exist&login='.$_POST['login'].'";</script>';
		}
		else
		{
			$uzivatel->set_heslo($_POST["heslo"]);
			$uzivatel->jmeno = $_POST['jmeno'];
			$uzivatel->zkratka_ustav =  empty($_POST['zkratka_ustav']) ? null : $_POST['zkratka_ustav'];
			$uzivatel->rocnik =  empty($_POST['rocnik']) ? null : $_POST['rocnik'];
			$uzivatel->opravneni = $_POST['opravneni'];
			$retu = $uzivatel->add();

			if ($retu)
				echo '<script>window.location.href = "pridej_uz.php?message=registrace";</script>';
			else
				echo '<script>window.location.href = "pridej_uz.php?message=failure";</script>';
		}
	}
}


?>
<!DOCTYPE html>


<html lang="cs-cz">
<head>
	<title>Učebny</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
	<link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
	<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
	<script> 
		  $( document ).ready(function() {
		      if($("#hide")){
		          $("#hide").fadeTo(3000, 400).slideUp(400, function(){
		             $("#hide").slideUp(400);
		              });   
		      }
		  });

		function updateForm()
		{
			var select = document.getElementById("student");
			var field = document.getElementById("rocnik_in");

			if (select.value == "1")
				field.style.display = "block";
			else
				field.style.display = "none";
		}

		function checkForm()
		{
			var select = document.getElementById("student");

			if (select.value == 1 && document.getElementById("rocnik").value < 1)
			{
				alert("Nevyplněn ročník studenta!");
				return false;
			}
			else
				return true;
		}

	</script>
</head>
<body>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
	<header id="hlavicka">
		  <h1>Učebny - FIT</h1>
		  <?php
		    include($_SERVER['ROOT_DIR'] . '/inc/menu.php');
		  ?>
	</header>

	<div class="center">
	<article>
		<section>
			<h2>Vytvoření uživatele</h2>
			<div id = "zarovne">
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>?action=registrace" method="post" onsubmit="return checkForm()">
			Jméno*: <input type="text" name="jmeno" value="" maxlength="50" required>
			Login*: <input type="text" name="login" value="" maxlength="8" required>
			Heslo*: <input type="text" name="heslo" value="" maxlength="255" required>
			Administrátor: <select name="opravneni" class="sl_style_add">
				<option value="0">Ne</option>
				<option value="1">Ano</option>
			</select><br/>

			<?php
			echo'Ústav:
			<select class="sl_style_add" name="zkratka_ustav">
				<option value="">Není akademik</option>';

				$result = get_ustavy();
				if ($result->num_rows > 0) {
			    while ($row2 = $result->fetch_assoc()) {
		        echo '<option value="' . $row2["zkratka_ustav"] . '"';
		        if ($row2["zkratka_ustav"] == $row["zkratka_ustav"]) echo ' selected';
		        echo '>' . $row2["zkratka_ustav"] . '</option>';
			    }
				} else
				    echo "Databáze neobsahuje žádné ústavy.";

			echo'</select> <br>';
			?>

			Student: <select name="student" id="student" class="sl_style_add" onchange="updateForm()">
				<option value="1">Ano</option>
				<option value="0">Ne</option>
			</select><br/>

			<div id="rocnik_in">Ročník: <input type="number" name="rocnik" id="rocnik" value="0" max="9"></div>
			<input class="button1" type="submit" value="Vložit do databáze">
			</form>
			</div>
		</section>
	<div class="cleaner"></div>
	</article>
	</div>
	<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
