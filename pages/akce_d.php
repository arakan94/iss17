<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

$conn = db_connect();

if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok']) && isset($_POST['typ_id']))
{
	$akce = new Akce($_POST['zkratka_predmet'], $_POST['ak_rok'], $_POST['typ_id']);

	if ($akce->exists() == false)
		die("Akce ".$_POST['zkratka_predmet']." ".$_POST['ak_rok']." ".$_POST['typ_id']." neexistuje.");

	if ($_POST['action'] == "upravit")
	{
		if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
			die("Nemáte oprávnění k úpravám.");

		$akce->nazev = $_POST['nazev'];
		$akce->popis = $_POST['popis'];
		$akce->uziv_cislo = $_POST['uziv_cislo'];
		$retu = $akce->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_ak.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_ak.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
			die("Nemáte oprávnění k úpravám.");

		$retu = $akce->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_ak.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_ak.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        return true;
                    } else {
                        return false;
                    }
            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_ak'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
	echo ($_POST['vybav_id']);
	echo ($_POST['ucebna_id']);

	if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok']) && isset($_POST['typ_id']))
	{
		$zkratka_predmet = $_POST['zkratka_predmet'];
		$ak_rok = $_POST['ak_rok'];
		$typ_id = $_POST['typ_id'];
	}
	else
	{
		$zkratka_predmet = $_GET['zkratka_predmet'];
		$ak_rok = $_GET['ak_rok'];
		$typ_id = $_GET['typ_id'];
	}

	$result = get_akce_join($zkratka_predmet, $ak_rok, $typ_id);

	if ($result->num_rows < 1)
		die("Akce předmětu ".$zkratka_predmet.$ak_rok." s Typ ID ".$typ_id." neexistuje.");

	$row = $result->fetch_assoc();
	echo '<h2>Karta akce - ' . $row["nazev"] . '</h2>';
	echo '<div class="text_l">';
	echo '<b>Garant - </b>' . $row["login"] . ' ('.$row["jmeno"].', '.$row['zkratka_ustav'].')<br/>';
	echo '<b>Typ - </b>' . $row["typ_akce_nazev"] . '<br/>'; 
	echo '<b>Popis - </b>' . $row["popis"] . '<br/>';
	echo '</div>';

	if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
	{
		echo '<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
		<form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return del();">
			<input type="hidden" name="zkratka_predmet" value="'.$row["zkratka_predmet"].'">
			<input type="hidden" name="ak_rok" value="'.$row["ak_rok"].'">
			<input type="hidden" name="typ_id" value="'.$row["typ_id"].'">
			<input type="hidden" name="action" value="odstranit">
			<input class="button1" type="submit" value="Odstranit">
		</form>';

		echo '<div id = "zarovne">
			<form action="'.$_SERVER['PHP_SELF'].'" method="post" id = "dis">
				Název*:   <input type="text" name="nazev" value="'.$row["nazev"].'" maxlength="50" required> <br>';
				
				echo'Garant*: <select name="uziv_cislo" class="sl_style_add" id="garant_select">';
                            
        $result_ak = get_akademiky();
        if ($result_ak->num_rows > 0) {
          while ($row_ak = $result_ak->fetch_assoc())
          {
              echo '<option';
              if ($row["uziv_cislo"] == $row_ak["uziv_cislo"]) echo ' selected';
              echo ' value="'.$row_ak["uziv_cislo"].'">'.$row_ak["login"].' ('.$row_ak["jmeno"].', '.$row_ak['zkratka_ustav'].')</option>';
          }
        }
        else
        	echo "Databáze neobsahuje žádné akademiky.";
  
        echo'</select>';
				
				echo 'Popis: <input type="text" name="popis" value="'.$row["popis"].'" maxlength="250"> <br>
				<input type="hidden" name="zkratka_predmet" value="'.$row["zkratka_predmet"].'">
				<input type="hidden" name="ak_rok" value="'.$row["ak_rok"].'">
				<input type="hidden" name="typ_id" value="'.$row["typ_id"].'">
				<input type="hidden" name="action" value="upravit">
				<input class="button1" type="submit" value="Upravit">
			</form>
			</div>';
	}
?>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
