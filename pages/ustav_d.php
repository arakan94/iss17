<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ustav.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['zkratka_ustav']) && isset($_POST['nazev']))
{
	$ustav = new Ustav($_POST['zkratka_id']); 

	if ($_POST['action'] == "upravit")
	{
		if (!$ustav->change_primary_key($_POST['zkratka_ustav']))
		{
			echo '<script>window.location.href = "vypis_us.php?message=edit_pk_fail";</script>';
			exit();
		}

		$ustav->nazev = $_POST['nazev'];

		$retu = $ustav->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_us.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_us.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		$retu = $ustav->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_us.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_us.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_us'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
if (isset($_POST['zkratka_ustav'])) {
	$zkratka_ustav = $_POST['zkratka_ustav'];
} else {
	$zkratka_ustav = $_GET['zkratka_ustav'];
}

$result = get_ustav($zkratka_ustav);
if ($result->num_rows < 1)
	die("Ústav ".$zkratka_ustav." neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta ústavu - ' . $row["zkratka_ustav"] . '</h2>';
echo '<div class="text_l">';
echo '<b>Název - </b>' . $row["nazev"] . '</br>';
?>
                    </div>
                    <button class="button1" id="butt1" onclick="show(1)">Upravit</button>
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return confirm('Opravdu chcete odstranit tento ústav?');">
                        <input type="hidden" name="zkratka_ustav" value="<?php echo $row["zkratka_ustav"]; ?>">
                        <input type="hidden" name="nazev" value="<?php echo $row["nazev"]; ?>">
                        <input type="hidden" name="zkratka_id" value="<?php echo $row["zkratka_ustav"]; ?>">
                        <input type="hidden" name="action" value="odstranit">
                        <input class="button1" type="submit" value="Odstranit">
                    </form>
                    <div id = "zarovne">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id = "dis">
                        Zkratka*: <input type="text" name="zkratka_ustav" value="<?php echo $row["zkratka_ustav"]; ?>" maxlength="4" required> <br>
                        Název*:   <input type="text" name="nazev" value="<?php echo $row["nazev"]; ?>" maxlength="50" required> <br>
                        <input type="hidden" name="zkratka_id" value="<?php echo $row["zkratka_ustav"]; ?>">
<?php
    echo '<input type="hidden" name="action" value="upravit">
    <input class="button1" type="submit" value="Upravit">';
?>
                    </form>
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
