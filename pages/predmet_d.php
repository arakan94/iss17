<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require $_SERVER['ROOT_DIR'] . "/db/db.php";
require $_SERVER['ROOT_DIR'] . "/db/obor.php";
require $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require $_SERVER['ROOT_DIR'] . "/db/ustav.php";
require $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION["zkratka_ustav"]))
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['old_zkratka_predmet']) && isset($_POST['old_ak_rok'])) {

    $predmet = new Predmet($_POST['old_zkratka_predmet'], $_POST['old_ak_rok']);

    if ($_POST['action'] == "upravit") {
    		if ($_SESSION['opravneni'] != 1)
					die("Nemáte oprávnění provádět úpravy.");

        $predmet->nazev = $_POST['nazev'];
        $predmet->rocnik = $_POST['rocnik'];
        $predmet->zkratka_obor = $_POST["zkratka_obor"];
        $predmet->zkratka_ustav = $_POST["zkratka_ustav"];
        $predmet->uziv_cislo = $_POST["uziv_cislo"];
        $predmet->popis = $_POST["popis"];

        if (!$predmet->change_primary_key($_POST['zkratka_predmet'], $_POST['ak_rok']))
        {
        	echo '<script>window.location.href = "vypis_pr.php?message=edit_pk_fail";</script>';
        	exit();
        }

        $retu = $predmet->update();

				if ($retu)
	        echo '<script>window.location.href = "vypis_pr.php?message=edit";</script>';
	      else
	      	echo '<script>window.location.href = "vypis_pr.php?message=edit_fail";</script>';
        exit();
    } else if ($_POST['action'] == "odstranit") {
    		if ($_SESSION['opravneni'] != 1)
					die("Nemáte oprávnění provádět úpravy.");

        $retu = $predmet->delete();
        
        if ($retu)
	        echo '<script>window.location.href = "vypis_pr.php?message=delete";</script>';
	      else
	        echo '<script>window.location.href = "vypis_pr.php?message=delete_fail";</script>';
        exit();
    }
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }

        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_pr'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section>

<?php
if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok'])) {
	$zkratka_predmet = $_POST['zkratka_predmet'];
	$ak_rok = $_POST['ak_rok'];
} else {
	$zkratka_predmet = $_GET['zkratka_predmet'];
	$ak_rok = $_GET['ak_rok'];
}

$result = get_predmet($zkratka_predmet, $ak_rok);

if ($result->num_rows < 1)
	die("Předmět " . $zkratka_predmet . " " . $ak_rok . " neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta předmětu - ' . $row["nazev"] . '</h2>';
echo '<div class="text_l">';
echo '<b>Zkratka - </b>' . $row["zkratka_predmet"] . '</br>';
echo '<b>Akademický rok - </b>' . $row["ak_rok"] . '</br>';
echo '<b>Zkratka ústavu - </b>' . $row["zkratka_ustav"] . '</br>';
echo '<b>Zkratka oboru - </b>' . $row["zkratka_obor"] . '</br>';
echo '<b>Garant - </b>' . $row["login"] . ' ('.$row["jmeno"].')</br>';
//echo '<b>Ročník - </b>' . $row["rocnik"] . '</br></br>';
echo '</br>';
echo '<b>Popis: </b><p>' . $row["popis"] . '</p>';
?>
                    </div>
                    <?php
                    if ($_SESSION['opravneni'] == 1)
                    echo'<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
                    <form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return confirm(\'Opravdu chcete odstranit předmět?\');">
                        <input type="hidden" name="old_zkratka_predmet" value="'.$row["zkratka_predmet"].'">
                        <input type="hidden" name="old_ak_rok" value="'.$row["ak_rok"].'">
                        <input type="hidden" name="action" value="odstranit">
                        <input class="button1" type="submit" value="Odstranit">
                    </form>';
                    ?>
                    <div id = "zarovne">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id = "dis">
                        Zkratka*: <input type="text" name="zkratka_predmet" value="<?php echo $row["zkratka_predmet"]; ?>" maxlength="3" required> <br>
                        Název*:   <input type="text" name="nazev" value="<?php echo $row["nazev"]; ?>" maxlength="50" required> <br>
                        Akademický rok*:   <input type="number" name="ak_rok" value="<?php echo $row["ak_rok"]; ?>" min="2002" max="9999" required> <br>

                        Garant*: <select name="uziv_cislo" class="sl_style_add" id="garant_select">
                            <?php
                                $result_ak = get_akademiky();
                                if ($result_ak->num_rows > 0) {
		                              while ($row_ak = $result_ak->fetch_assoc())
		                              {
		                                  echo '<option';
		                                  if ($row["uziv_cislo"] == $row_ak["uziv_cislo"]) echo ' selected';
		                                  echo ' value="'.$row_ak["uziv_cislo"].'">'.$row_ak["login"].' ('.$row_ak["jmeno"].', '.$row_ak['zkratka_ustav'].')</option>';
		                              }
                                }
                                else
                                	echo "Databáze neobsahuje žádné akademiky.";
                            ?> 
                                </select>

                         Zkratka obor*: <select name="zkratka_obor" id="obor_select" class="sl_style_add">
                                <option value="default">Vyberte obor</option>
                                    <?php
                                    $result_ob = get_obory();
                                    if ($result_ob->num_rows > 0) {
                                        while ($row_ob = $result_ob->fetch_assoc()) {
                                            echo '<option';
                                            if ($row["zkratka_obor"] == $row_ob["zkratka_obor"]) echo ' selected';
                                            echo ' value="'.$row_ob["zkratka_obor"].'">' .$row_ob["zkratka_obor"]. '</option>';
                                        }
                                    } else
                                        echo "Databáze neobsahuje žádné obory.";
                                    ?> 
                                            </select>
                         Zkratka ústav*: <select name="zkratka_ustav" id="ustav_select" class="sl_style_add">
                                <option value="default">Vyberte ústav</option>
                                    <?php
                                    $result_us = get_ustavy();
                                    if ($result_us->num_rows > 0) {
                                        while ($row_us = $result_us->fetch_assoc()) {
                                            echo '<option';
                                            if ($row["zkratka_ustav"] == $row_us["zkratka_ustav"]) echo ' selected';
                                            echo ' value="'.$row_us["zkratka_ustav"].'">' .$row_us["zkratka_ustav"]. '</option>';
                                        }
                                    } else
                                        echo "0 results";
                                    ?> 
                                            </select>

                        <input type="hidden" name="rocnik" value="<?php echo $row["rocnik"]; ?>"> <br>
                        Popis:   <input type="text" name="popis" value="<?php echo $row["popis"]; ?>" maxlength="250"> <br>
                        <input type="hidden" name="old_zkratka_predmet" value="<?php echo $row["zkratka_predmet"]; ?>">
                        <input type="hidden" name="old_ak_rok" value="<?php echo $row["ak_rok"]; ?>">
<?php
    echo '<input type="hidden" name="action" value="upravit">
    <input class="button1" type="submit" value="Upravit">';
?>
                    </form>
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
