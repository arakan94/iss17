<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

/* Datepair, datetime library. Copyright (c) 2014 Jon Thornton.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/rezervace.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['uziv_cislo']))
{
	$zacatek = $_POST["zacatek_datum"] .' ' . $_POST["zacatek_cas"];
	$konec = $_POST["zacatek_datum"] .' ' . $_POST["konec_cas"];

	$akce = $_POST['akce'];
	$akce_arr = explode('|', $akce);

	$rezervace = new Rezervace();

	$rezervace->zkratka_predmet = $akce_arr[0];
	$rezervace->ak_rok = $akce_arr[1];
	$rezervace->typ_id = $akce_arr[2];
	$rezervace->uziv_cislo = $_POST['uziv_cislo'];
	$rezervace->ucebna_id = $_POST["ucebna_id"];
	$rezervace->zacatek = $zacatek;
	$rezervace->konec = $konec;
	$rezervace->poznamka = $_POST["poznamka"];

	if ($rezervace->add())
		echo '<script>window.location.href = "pridej_re.php?message=success";</script>';
	else
		echo '<script>window.location.href = "pridej_re.php?message=failure";</script>';
	exit();
}
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="../datetime/jquery.timepicker.css">
        <link rel="stylesheet" type="text/css" href="../datetime/lib/bootstrap-datepicker.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="../datetime/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="../datetime/lib/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../datepair/dist/datepair.js"></script>
        <script type="text/javascript" src="../datepair/dist/jquery.datepair.js"></script>
        <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
    
	function checkForm()
	{
		if (document.getElementById("akce_select").value == "default" ||
			document.getElementById("ucebna_select").value == "default" ||
			<?php if (empty($_SESSION['zkratka_ustav'])) echo 'document.getElementById("uziv_cislo_select").value == "default" ||' ?>
			document.getElementById("datum_od").value == "" ||
			document.getElementById("cas_od").value == "" ||
			document.getElementById("cas_do").value == "" 
			//|| document.getElementById("datum_do").value == ""
		)
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else
			return true;
	}


	function fromDB()
	{
		if (checkForm() == false)
			return false;

		$.ajax({url: 'filter.php',
		    type: 'POST',
		    dataType: "json",
		    data: {
		        datum_od: document.getElementById("datum_od").value,
		        cas_od: document.getElementById("cas_od").value,
		        cas_do: document.getElementById("cas_do").value,
		        datum_do: document.getElementById("datum_do").value,
		        ucebna_id: document.getElementById("ucebna_select").value
		    },
		    success: function (out) {
		        if(out[0][0] == true){
		            document.getElementById("vlozit_form").submit();
		        } else {
		            alert("Učebna není volná v zadaném termínu.");
		        }                  
		    }
		});
	}
</script>
    </head>
    <body>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
<?php
	$page = 'pridej_hl';
	$page1 = 'pridej_re';
	include( $_SERVER['ROOT_DIR'] . '/inc/menu.php');
?>
        </header>
        <div class="center">
            <article>
                <section>
                    <h2>Přidej rezervaci</h2>
                    <div id = "zarovne">
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="vlozit_form" onsubmit="return checkForm();">
                        <?php
                        	if (!empty($_SESSION['zkratka_ustav']))
                        		echo '<input type="hidden" name="uziv_cislo" value="' . $_SESSION["uziv_cislo"] .'">'; 
                        	else
                        	{
                        		echo 'Zodpovědná osoba*: <select name="uziv_cislo" id="uziv_cislo_select" class="sl_style_add">';
                            
		                      	echo '<option value="default">Vyberte zodpovědnou osobu</option>';

                            $res = get_akademiky();
                            if ($res->num_rows > 0) {
                              while ($row_ak = $res->fetch_assoc())
                              {
                              	if ($row_ak["uziv_cislo"] != $_SESSION['uziv_cislo'])
                                  echo '<option value="'.$row_ak["uziv_cislo"].'">'.$row_ak["jmeno"].' ('.$row_ak['zkratka_ustav'].')</option>';
                              }
                            }
                            else
                            	echo "Databáze neobsahuje žádné akademiky.";
                        
                            echo '</select>';
                        	}
                        
                        ?>
                        Vyberte akci*: <select name="akce" class="sl_style_add" id="akce_select">
                        <option value="default">Vyberte akci</option>
                                <?php
                                $result = get_all_akce();
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["zkratka_predmet"].'|'.$row["ak_rok"].'|'.$row["typ_id"].'">' . $row["nazev"] . '</option>';
                                    }
                                } else
                                    echo "V databázi nejsou žádné akce.";
                                ?>
                            </select>
                            Vyberte učebnu*: <select name="ucebna_id" class="sl_style_add" id="ucebna_select">
                            <option value="default">Vyberte učebnu</option>
                                   <?php
                                   $result = get_ucebny();
                                   if ($result->num_rows > 0) {
                                       while ($row = $result->fetch_assoc()) {
                                           echo '<option value="'.$row["ucebna_id"].'" '.($row["ucebna_id"] == $_GET['ucebna_id'] ? "selected" : "").'>' . $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</option>';
                                       }
                                   } else
                                       echo "V databázi nejsou žádné učebny.";
                                   ?>
                               </select>
                            <div id="datepairExample">
                                Od:
                                <input type="text" id="datum_od" name="zacatek_datum" class="date start" value="<?php echo $_GET['datum_od'] ?>" />
                                <input type="text" id="cas_od" name="zacatek_cas" class="time start" value="<?php echo $_GET['cas_od'] ?>" />
                                <br/>
                                Do:
                                <input type="text" id="datum_do" disabled="disabled" name="konec_datum" class="date end" value="<?php echo $_GET['datum_do'] ?>" />
                                <input type="text" id="cas_do" name="konec_cas" class="time end" value="<?php echo $_GET['cas_do'] ?>" />
                            </div>
                            <script>
                                // initialize input widgets first
                                $('#datepairExample .time').timepicker({
                                    'showDuration': true,
                                    'timeFormat': 'H:i',
                                    'step': '60',
                                    'minTime': '7:00',
                                    'maxTime': '22:00'
                                });

                                $('#datepairExample .date').datepicker({
                                    'format': 'yyyy-m-d',
                                    'autoclose': true
                                });

                                // initialize datepair
                                $('#datepairExample').datepair();
                            </script>
                            Poznámka: <input type="text" name="poznamka" value="" maxlength="250"> <br>

                        </form>
                            <button class = "button1" id="vlozit_submit" onclick="fromDB();">Vložit</button>
                    </div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
