<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['uziv_cislo']))
{
	$uzivatel = new Uzivatel($_POST['uziv_cislo']);

	if ($uzivatel->exists())
	{
		if ($uzivatel->add_registrovany_predmet($_POST['zkratka_predmet'], $_POST['ak_rok']))
		{
			echo '<script>window.location.href = "pridej_rp.php?message=success&uziv_cislo='.$_POST['uziv_cislo'].'";</script>';
			exit();
		}
		else
		{
			echo '<script>window.location.href = "pridej_rp.php?message=predmet_failure&uziv_cislo='.$_POST['uziv_cislo'].'";</script>';
			exit();
		} 
	}
	else
	{
		echo '<script>window.location.href = "pridej_rp.php?message=failure&uziv_cislo='.$_POST['uziv_cislo'].'";</script>';
		exit();
	}
}

?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
    
     function getRok() {
                                $.ajax({url: 'filter.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        predmet: document.getElementById("predmety").options[document.getElementById("predmety").selectedIndex].value
                                    },
                                    success: function (out) {
                                        if(out == "false"){
                                           
                                        } else{
                                            setNewSel(out);
                                        }                  
                                    }
                                });
                            }
    
    function setNewSel(data) {
			var len = data.length;
			var select= document.getElementById('ak_rok');

			for (var i = select.length-1; i >=0; i--)
			{
				select.remove(i);
			}

			for (var j = 0; j < len; j++){
				var option1 = document.createElement('option');
				option1.value = data[j];
				option1.text = data[j];
				select.appendChild(option1);
			}
   }

	function checkForm()
	{
		if (document.getElementById("uziv_cislo").value == "default" ||
			document.getElementById("ak_rok").value == "default" ||
			document.getElementById("predmety").value == "default")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else
			return true;
	}
</script>

    </head>
    <body>
<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'sprava_hl'; $page1 = 'pridej_rp'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>

        <section >
             <h2>Registrace předmětu</h2>
             <div id = "zarovne">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return checkForm();">
        Student*: <select name="uziv_cislo" class="sl_style_add" id="uziv_cislo">
                            <?php
				                      	echo '<option value="default">Vyberte studenta</option>';

                                $result = get_studenty();
                                if ($result->num_rows > 0) {
		                              while ($row = $result->fetch_assoc())
		                              {
	                                  echo '<option value="'.$row["uziv_cislo"].'"';
	                                  if ($row["uziv_cislo"] == $_GET["uziv_cislo"]) echo ' selected';
	                                  echo '>'.$row["login"].' ('.$row["jmeno"].', '.$row['rocnik'].'. ročník)</option>';
		                              }
                                }
                                else
                                	echo "Databáze neobsahuje žádné akademiky.";
                            ?> 
                                </select>
        
        
             Předmět*: <select name="zkratka_predmet" onchange="getRok()" class="sl_style_add" id="predmety">
                        <option value="default">Vyberte předmět</option>
                                <?php
                                $result = get_predmety_zkratka();
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["zkratka_predmet"].'">' . $row["zkratka_predmet"] . '</option>';
                                    }
                                } else
                                    echo "Databáze neobsahuje žádné předměty.";
                                ?>
                            </select>
             
                            Akademický rok*: <div id="pripni"> </div><select name="ak_rok" class="sl_style_add" id="ak_rok">
                            <option value="default">Vyberte nejprve předmět</option>
                                </select>
                <input class = "button1" type="submit" value="Registrovat">
	</form>
                 </div>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
