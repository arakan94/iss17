<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

/* Datepair, datetime library. Copyright (c) 2014 Jon Thornton.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

$conn = db_connect();

if (isset($_POST['ucebna_id']))
{
	$ucebna = new Ucebna($_POST['ucebna_id']); 
	if ($_POST['action'] == "upravit") {
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$ucebna->budova = $_POST["budova"];
		$ucebna->patro = $_POST["patro"];
		$ucebna->cislo_mistnosti = $_POST["cislo_mistnosti"];
		$ucebna->kapacita = $_POST["kapacita"];
		$ucebna->popis = $_POST["popis"];

		$retu = $ucebna->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_uc.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_uc.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$retu = $ucebna->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_uc.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_uc.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="../datetime/jquery.timepicker.css">
        <link rel="stylesheet" type="text/css" href="../datetime/lib/bootstrap-datepicker.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="../datetime/jquery.timepicker.min.js"></script>
        <script type="text/javascript" src="../datetime/lib/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="../datepair/dist/datepair.js"></script>
        <script type="text/javascript" src="../datepair/dist/jquery.datepair.js"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "block";
                    document.getElementById("butt2").innerHTML = "Skrýt";
                    document.getElementById("butt2").setAttribute("onClick", "discard(2)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "none";
                    document.getElementById("butt2").innerHTML = "Přidej vybavení";
                    document.getElementById("butt2").setAttribute("onClick", "show(2)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }
            
            function checkDateTime()
            {
            	if (document.getElementById("datum_od").value == "" ||
            	    document.getElementById("cas_od").value == "" ||
            	    document.getElementById("cas_do").value == "" ||
            	    document.getElementById("datum_do").value == "")
            	{
            		alert("Není vyplněno časové rozpětí");
            		return false;
            	}
            }
            
            function fromDB(silent, rezervace) {
            	if (checkDateTime() == false)
            		return false;
                                $.ajax({url: 'filter.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        datum_od: document.getElementById("datum_od").value,
                                        cas_od: document.getElementById("cas_od").value,
                                        cas_do: document.getElementById("cas_do").value,
                                        datum_do: document.getElementById("datum_do").value,
                                        ucebna_id: document.getElementById("ucebna_id").value
                                    },
                                    success: function (out) {
                                        if(out[0][0] == true){
                                            if (!silent) alert("Učebna je volná v zadaném termínu.");
                                            if (rezervace) rezervovat();
                                        } else {
                                            if (!silent) alert("Učebna není volná v zadaném termínu.");
                                        }                  
                                    }
                                });
                            }
            
            function rezervovat() {
            	var datum_od = document.getElementById("datum_od").value;
            	var datum_do = document.getElementById("datum_do").value;
            	var cas_do = document.getElementById("cas_do").value;
            	var cas_od = document.getElementById("cas_od").value;
            	var ucebna_id = document.getElementById("ucebna_id").value;
            	
            	window.location.href='<?php echo $_SERVER['ROOT_URL']?>/pages/pridej_re.php?datum_od='+datum_od+'&cas_od='+cas_od+'&cas_do='+cas_do+'&datum_do='+datum_od+'&ucebna_id='+ucebna_id;
            	
            }
                            
                            function getUcitele() {
                                $.ajax({url: 'filter.php',
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        ucitel: document.getElementById("ucitele").options[document.getElementById("ucitele").selectedIndex].value
                                    },
                                    success: function (out) {
                                        if(out == "false"){
                                           
                                        } else{
                                            vypis(out);
                                        }                  
                                    }
                                });
                            }
                            
                            function vypis(pole) {
                                var len = pole.length;
                                var doc = document.getElementById("pripnu");
                                for(var i = 0; i < len; i++){
                                    var div = document.createElement('p');
                                    var ent = document.createElement('br');
                                    var ent1 = document.createElement('br');
                                    var ent2 = document.createElement('br');
                                    var ent3 = document.createElement('br');
                                    var text1 = document.createTextNode(pole[i][0]);
                                    var text2 = document.createTextNode(pole[i][1]);
                                    var text3 = document.createTextNode(pole[i][2]);
                                    var text4 = document.createTextNode(pole[i][3]);
                                    div.appendChild(text1);
                                    div.appendChild(ent);
                                    div.appendChild(text2);
                                    div.appendChild(ent1);
                                    div.appendChild(text3);
                                    div.appendChild(ent2);
                                    div.appendChild(text4);
                                    div.appendChild(ent3);
                                    doc.appendChild(div);
                                }
                                
                            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_uc'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
if (isset($_POST['ucebna_id'])) {
	$ucebna_id = $_POST['ucebna_id'];
} else {
	$ucebna_id = $_GET['ucebna_id'];
}

$result = get_ucebna($ucebna_id);
if ($result->num_rows < 1)
	die("Učebna s ID ".$ucebna_id." neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta učebny - ' . $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</h2>';
echo '<div class="text_l">';
echo '<b>Budova - </b>' . $row["budova"] . '<br/>';
echo '<b>Patro - </b>' . $row["patro"] . '<br/>';
echo '<b>Číslo místnosti - </b>' . $row["cislo_mistnosti"] . '<br/>';
echo '<b>Kapacita - </b>' . $row["kapacita"] . '<br/>';
echo '<b>Popis - </b>' . $row["popis"] . '<br/>';

echo '<h4>Vybavení učebny <a href="vybaveni_d.php?action=upravit&ucebna_id=' . $ucebna_id .'"> Detail </a></h4>'; 

$res_typy = get_typy_vybaveni_ucebna($ucebna_id);
if ($res_typy->num_rows < 1)
{
	echo "<br/>";   
	echo "Tato učebna nemá žádné vybavení.<br/>";
}
else
{
	while ($row_typy = $res_typy->fetch_assoc())
	{
		$res_vybav = get_vybaveni_typ($ucebna_id, $row_typy["typ"]);
		if ($res_vybav->num_rows > 0)
			echo '<b>'.$row_typy["typ"].'</b> - ' . $res_vybav->num_rows . 'ks <a href="vybaveni_d.php?action=upravit&ucebna_id=' . $ucebna_id .'&typ='.$row_typy["typ"].'"> Detail </a><br/>';
	}
}

if ($_SESSION['opravneni'] == 1)
	echo '<br/><a href="'.$_SERVER['ROOT_URL'].'/pages/pridej_vy.php?ucebna_id='.$ucebna_id.'">Přidat vybavení</a>';

echo '</div>';

echo' <div id = "zarovne">';

if ($_SESSION['opravneni'] == 1)
{
echo '
<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
<form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return confirm(\'Opravdu chcete odstranit učebnu?\');">
    <input type="hidden" name="ucebna_id" value="'.$row["ucebna_id"] .'">
    <input type="hidden" name="action" value="odstranit">
    <input class="button1" type="submit" value="Odstranit">
</form>';

echo'<form action="'.$_SERVER['PHP_SELF'].'" method="post" id = "dis">
    Budova*: <input type="text" name="budova" value="'.$row["budova"].'" maxlength="1" required> <br>
    Patro*:   <input type="number" name="patro" value="'.$row["patro"].'" max="99" min="-99" required> <br>
    Číslo místnosti*:   <input type="number" name="cislo_mistnosti" value="'.$row["cislo_mistnosti"].'" min="0" max="99" required> <br>
    Kapacita:   <input type="number" name="kapacita" value="'.$row["kapacita"].'" min="0" max="9999"> <br>
    Popis:   <input type="text" name="popis" value="'.$row["popis"].'" maxlength="250"> <br>
     <input type="hidden" name="ucebna_id" value="'.$row["ucebna_id"].'">';

echo '<input type="hidden" name="action" value="upravit">
<input class="button1" type="submit" value="Upravit"></form>';
}
?>
                         <div id="datepairExample">
                                Od:
                                <input id="datum_od" type="text" name="zacatek_datum" class="date start" />
                                <input id="cas_od" type="text" name="zacatek_cas" class="time start" />
                                <br>
                                Do:
                                <input id="datum_do" type="text" name="konec_datum" class="date end" />
                                <input id="cas_do" type="text" name="konec_cas" class="time end" />
                                <input type="hidden" id="ucebna_id" name="ucebna_id" value="<?php echo $ucebna_id; ?>">
                         </div>
                            <script>
                                // initialize input widgets first
                                $('#datepairExample .time').timepicker({
                                    'showDuration': true,
                                    'timeFormat': 'H:i:i',
                                    'step': '60',
                                    'minTime': '7:00',
                                    'maxTime': '22:00'
                                    
                                });

                                $('#datepairExample .date').datepicker({
                                    'format': 'yyyy-m-d',
                                    'autoclose': true
                                });

                                // initialize datepair
                                $('#datepairExample').datepair();
                            </script>
                            <button class="button1" id="butt2" onclick="fromDB(false, false)">Prověřit dostupnost učebny</button>
                            <?php if (!empty($_SESSION['zkratka_ustav']) || $_SESSION['opravneni'] == 1) echo'<button class="button1" id="butt2" onclick="fromDB(true, true)">Rezervovat učebnu</button>'; ?>
                            <div id="tf"></div>
                             Vyberte učitele: <select class="sl_style_add" id="ucitele">
                        <option value="">Vyberte učitele</option>
                                <?php
                                $result = get_akademiky();
                                if ($result->num_rows > 0) {
                                    while ($row = $result->fetch_assoc()) {
                                        echo '<option value="'.$row["uziv_cislo"].'">' . $row["jmeno"] . ' ('.$row["zkratka_ustav"].')</option>';
                                    }
                                } else
                                    echo "0 results";
                                ?>
                            </select>
                            <button class="button1" id="butt1" onclick="getUcitele()">Ukázat registrované termíny</button>
                            <div id="pripnu">
                            </div>
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
