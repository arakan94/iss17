<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require $_SERVER['ROOT_DIR'] . "/db/db.php";
require $_SERVER['ROOT_DIR'] . "/db/obor.php";
require $_SERVER['ROOT_DIR'] . "/db/akce.php";

$conn = db_connect();

if (isset($_POST['nazev'])) {

	$typ_akce = new TypAkce($_POST['nazev']);
	if ($typ_akce->exists() == false)
		die("Typ akce s názvem ".$_POST['nazev']." neexistuje.");

	if ($_POST['action'] == "upravit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$typ_akce->nazev = $_POST['new_nazev'];

		$retu = $typ_akce->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_ta.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_ta.php?message=edit_fail";</script>';
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$retu = $typ_akce->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_ta.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_ta.php?message=delete_fail";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                }
            }
            
            function del(){
                if (confirm('Opravdu chcete předmět odstranit?')) {
                        
                    } else {
                        
                    }
            }
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php $page = 'vypis_hl'; $page1 = 'vypis_ta'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
        </header>
        <div class="center">
            <article>
                <section>

<?php
if (isset($_POST['typ_id']))
	$typ_id = $_POST['typ_id'];
else
	$typ_id = $_GET['typ_id'];

$result = get_typ_akce_id($typ_id);

if ($result->num_rows < 1)
	die("TypAkce s ID ".$typ_id." neexistuje.");

$row = $result->fetch_assoc();
echo '<h2>Karta oboru</h2>';
echo '<div class="text_l">';
echo '<b>Název - </b>' . $row["nazev"] . '</br>';
echo '</div>';

if ($_SESSION['opravneni'] == 1)
{
	echo '<button class="button1" id="butt1" onclick="show(1)">Upravit</button>
	<form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return confirm(\'Opravdu chcete odstranit typ akce?\');">
		  <input type="hidden" name="nazev" value="'.$row["nazev"].'">
		  <input type="hidden" name="action" value="odstranit">
		  <input class="button1" type="submit" value="Odstranit">
	</form>
	<div id = "zarovne">
	<form action="'.$_SERVER['PHP_SELF'].'" method="post" id = "dis">
		  Název*:   <input type="text" name="new_nazev" value="'.$row["nazev"].'" maxlength="50" required> <br>
		  <input type="hidden" name="nazev" value="'.$row["nazev"].'">';

	echo '<input type="hidden" name="action" value="upravit">
	<input class="button1" type="submit" value="Upravit">
	</form>
	</div>';
}
?>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
