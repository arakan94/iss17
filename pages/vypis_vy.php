<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav'])) // neni ani admin ani akademik
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_vy'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section >
           <h2>Výpis vybavení</h2>
           <table>
            <tr>
      <th>Učebna</th>
   		<th>Název</th>
   		<th>Typ</th>
   		<th>Popis</th>
   		<th colspan = "3">Akce</th>
   				
   	    </tr>
	<?php
		$result = get_all_vybaveni();
		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row['budova'].$row["patro"].str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT) .'</td><td>' . $row["nazev"] . '</td><td>' . $row["typ"] . '</td><td>' . $row["popis"] . '</td>';

				if ($_SESSION["opravneni"] == 1)
					echo '<td><a href="vybav_d.php?action=upravit&vybav_id=' .$row["vybav_id"]. '&ucebna_id='.$row["ucebna_id"].'"> Upravit </a></td><td><a href="vybav_d.php?action=odstranit&vybav_id=' .$row["vybav_id"]. '&ucebna_id='.$row["ucebna_id"].'"> Odstranit </a></td>';

			  echo '<td><a href="vybav_d.php?action=detail&vybav_id=' .$row["vybav_id"]. '&ucebna_id='.$row["ucebna_id"].'"> Detail </a></td></tr>';
			}
		}
		else 
			echo "0 results";
	?>
        
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>

</html>
