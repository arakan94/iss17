<?php
require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/rezervace.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
?>

<?php
$conn = db_connect();


$ucebna_id = $_POST["ucebna"]; 
//echo ($ucebna_id);
$ucitel = $_POST["ucitel"];  
$zkratka_predmet = $_POST["predmet"];
$ak_rok_predmet = $_POST["ak_rok"];
$typ_id = $_POST["typ_id"];
$uziv_id = $_POST["ucitel"];
/*
$result = get_predmet($zkratka_predmet, $ak_rok);
$row = $result->fetch_assoc();
echo '<h2>Karta předmětu - ' . $row["nazev"] . '</h2>';
echo '<div class="text_l">';
echo '<b>Zkratka - </b>' . $row["zkratka_predmet"] . '</br>';
echo '<b>Akademický rok - </b>' . $row["ak_rok"] . '</br>';
*/
$i = 0;
$pole = [];
if($zkratka_predmet != "default" && $ak_rok_predmet != "default" && $typ_id != "default"){ //filtrování pomocí predmetu
$result = get_rezervace_akce($zkratka_predmet, $ak_rok_predmet, $typ_id);
	if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = 'Název: ' . $row["nazev"] .'<br> Typ: '. $row["typ_akce_nazev"] .'<br> Zkratka před. : '. $row["zkratka_predmet"] .'<br> Ak. rok: '. $row["ak_rok"] . '<br> Učebna: '. $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
              $pole[$i][1] = $row["zacatek"];
              $pole[$i][2] = $row["konec"];
              $pole[$i][3] = $row["rezerv_id"];
              $i++;
            }
		}
		else 
			$pole[0][0] = false;
} else if($ucebna_id != "default") {
    $result = get_rezervace_ucebna($ucebna_id);
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = 'Název: ' . $row["nazev"] .'<br> Typ: '. $row["typ_akce_nazev"] .'<br> Zkratka před. : '. $row["zkratka_predmet"] .'<br> Ak. rok: '. $row["ak_rok"] . '<br> Učebna: '. $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
              $pole[$i][1] = $row["zacatek"];
              $pole[$i][2] = $row["konec"];
              $pole[$i][3] = $row["rezerv_id"];
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
} else if($uziv_id != "default") {
    $result = get_rezervace_garant($uziv_id);
    if ($result->num_rows > 0)
		{
            // output data of each row
              while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = 'Název: ' . $row["nazev"] .'<br> Typ: '. $row["typ_akce_nazev"] .'<br> Zkratka před. : '. $row["zkratka_predmet"] .'<br> Ak. rok: '. $row["ak_rok"] . '<br> Učebna: '. $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
              $pole[$i][1] = $row["zacatek"];
              $pole[$i][2] = $row["konec"];
              $pole[$i][3] = $row["rezerv_id"];
              $i++;
            }
            
		}
		else 
			$pole[0][0] = false;
}
else {
    $result = get_all_rezervace();
	if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  $pole[$i][0] = 'Název: ' . $row["nazev"] .'<br> Typ: '. $row["typ_akce_nazev"] .'<br> Zkratka před. : '. $row["zkratka_predmet"] .'<br> Ak. rok: '. $row["ak_rok"].'<br> Učebna: '. $row["budova"] . $row["patro"] . str_pad($row['cislo_mistnosti'], 2, "0", STR_PAD_LEFT);
              $pole[$i][1] = $row["zacatek"];
              $pole[$i][2] = $row["konec"];
              $pole[$i][3] = $row["rezerv_id"];
              $i++;
            }
		}
		else 
			$pole[0][0] = false;
}
//print_r($pole);
echo json_encode($pole);

?>