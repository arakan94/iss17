<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1 && empty($_SESSION["zkratka_ustav"]))
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();
?>

<!DOCTYPE html>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'vypis_hl'; $page1 = 'vypis_pr'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section >
        <button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?>?action=all<?php if (!empty($_GET['zkratka_obor'])) echo '&zkratka_obor='.$_GET['zkratka_obor']; ?>'">Všechny akademické roky</button>
        <button class="button1" onclick="window.location.href='<?php echo $_SERVER['PHP_SELF']?><?php if (!empty($_GET['zkratka_obor'])) echo '?zkratka_obor='.$_GET['zkratka_obor']; ?>'">Pouze tento rok</button>

	<?php
		if (empty($_GET['zkratka_obor']))
			if ($_GET['action'] == "all")
			{
				$all = true;
				$result = get_predmety();
			}
			else
				$result = get_predmety_by_rok(date("Y"));
		else
			if ($_GET['action'] == "all")
			{
				$all = true;
				$result = get_predmety_by_obor($_GET['zkratka_obor']);
			}
			else
				$result = get_predmety_by_obor_rok($_GET['zkratka_obor'], date("Y"));

		if ($all)
			echo '<h2>Výpis předmětů</h2>';
		else
			echo '<h2>Výpis předmětů - '.date("Y").'</h2>';

			echo'<table>
				<tr>
					<th>Zkratka</th>';
		if ($all) echo '<th>Ak. rok</th>';
		echo '<th>Název</th>
			<th colspan = "3">Akce</th></tr>';

		if ($result->num_rows > 0)
		{
			// output data of each row
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row["zkratka_predmet"] . '</td>';
			  
			  if ($all) echo '<td>'.$row["ak_rok"].'</td>';
			  
			  echo '<td> ' . $row["nazev"] . '</td>';

				if ($_SESSION['opravneni'] == 1)
			  	echo '<td><a href="predmet_d.php?action=upravit&zkratka_predmet=' .$row["zkratka_predmet"].'&ak_rok='.$row["ak_rok"].'"> Upravit </a></td> <td><a href="predmet_d.php?action=odstranit&zkratka_predmet=' .$row["zkratka_predmet"]. '&ak_rok='.$row["ak_rok"].'"> Odstranit </a></td>';
			  echo' <td><a href="predmet_d.php?action=upravit&zkratka_predmet=' .$row["zkratka_predmet"].'&ak_rok='.$row["ak_rok"].'"> Detail </a></td></tr>';
			}
		}
		else 
			echo "V databázi nejsou žádné předměty.";
	?>
        
        </table>
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
