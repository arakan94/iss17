<?php
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/ustav.php";

if ($_SESSION['login_status'] != true)
	die("Uživatel není přihlášen.");

$conn = db_connect();

if (isset($_POST['uziv_cislo'])) {
	$user = new Uzivatel($_POST['uziv_cislo']);

	if (!$user->exists())
		die("Uživatel s ID ".$_POST['uziv_cislo']." neexistuje.");

	if ($_POST['action'] == "zmena_jmeno")
	{
		$user->jmeno = $_POST["jmeno"];

		$retu = $user->update();

		if ($retu)
			echo '<script>window.location.href = "prihlaseni.php?action=odhlaseni&message=edit";</script>';
		else
			echo '<script>window.location.href = "muj_ucet.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "zmena_hesla")
	{
		if ($_POST["heslo"] != "")
			$retu = $user->set_heslo($_POST["heslo"]);
		else
			$retu = false;

		if ($retu)
			echo '<script>window.location.href = "prihlaseni.php?action=odhlaseni&message=password";</script>';
		else
			echo '<script>window.location.href = "muj_ucet.php?message=password_fail";</script>';
		exit();
	}

}

?>

<!DOCTYPE html>

<html lang="cs-cz">
<head>
	<title>Učebny</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
	<link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
	<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
</head>

<body>
	<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

	<script>
		$( document ).ready(function() {
		if($("#hide")){
				$("#hide").fadeTo(3000, 400).slideUp(400, function(){
				   $("#hide").slideUp(400);
				    });   
		}
		});
	</script>

	<header id="hlavicka">
		<h1>Učebny - FIT</h1>
		<?php
			$page = 'osobni_roz';
			$page1 = 'muj_ucet';
			include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); 
		?>
	</header>
	<div class="center">
	<article>
		<section>

		<?php
		$uziv_cislo = $_SESSION["uziv_cislo"];

		$result = get_uzivatel($uziv_cislo);
		if ($result->num_rows < 1)
			die("Uživatel s ID ".$uziv_cislo." neexistuje.");

		$row = $result->fetch_assoc();
		$uziv_cislo = $row["uziv_cislo"];

		echo '<h2>Správa účtu</h2>';
		echo '<div class="text_l">';
		echo '<b>Jméno - </b>' . $row["jmeno"] . '<br/>';

		echo '<br/>';

		if ($row["rocnik"] > 0)
		{
			echo '<b>Ročník - </b>' . $row["rocnik"] . '<br/>';
		}

		if (!empty($row["zkratka_ustav"]))
			echo '<b>Ústav - </b>' . $row["zkratka_ustav"] . '<br/>';

		echo '<br/>';

		if ($row["rocnik"] > 0)
		{
			$res_obory = get_zapsane_obory_uzivatel($uziv_cislo);
			if ($res_obory->num_rows > 0)
			{
				echo '<b>Zapsané obory</b><br/>';
				while ($row_obory = $res_obory->fetch_assoc())
				{
					echo $row_obory["zkratka_obor"] . ' - ' . $row_obory["nazev"];
					echo '<br/>';
				}
			}
			else
				echo 'Pozor! Nemáte zapsaný žádný obor.<br/>';

			echo '<br/>';
		}

		if (!empty($row["zkratka_ustav"]))
		{
			$res_gar = get_garantovane_predmety($uziv_cislo);
			if ($res_gar->num_rows > 0)
			{
				echo '<b>Garantované předměty</b><br/>';
				while ($row_gar = $res_gar->fetch_assoc())
				{
					echo '<a href="predmet_d.php?zkratka_predmet='.$row_gar["zkratka_predmet"].'&ak_rok='.$row_gar["ak_rok"].'">';
					echo $row_gar["zkratka_predmet"].' ('.$row_gar["ak_rok"].')</a> - ' . $row_gar["nazev"] . '<br/>';
				}
			}
		}

		echo'</div>';
		echo '<div id = "zarovne">';

		echo '<h3>Změna jména</h3>';    
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
		echo 'Jméno*: <input type="text" name="jmeno" value="'.$row["jmeno"].'" maxlength="50" required> <br>';
		echo '<input type="hidden" name="uziv_cislo" value="'.$uziv_cislo.'">';
		echo '<input type="hidden" name="action" value="zmena_jmeno">';
		echo '<input class="button1" type="submit" value="Změnit jméno"></form>';


		echo '<h3>Změna hesla</h3>';    
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
		echo 'Heslo*: <input type="text" name="heslo" value="" required> <br>';
		echo '<input type="hidden" name="uziv_cislo" value="'.$uziv_cislo.'">';
		echo '<input type="hidden" name="action" value="zmena_hesla">';
		echo '<input class="button1" type="submit" value="Změnit heslo"></form>';
		echo '</div>';
		?>

		</section>
		<div class="cleaner"></div>
	</article>
	</div>
  <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
