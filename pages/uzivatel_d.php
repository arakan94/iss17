<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";
require  $_SERVER['ROOT_DIR'] . "/db/ucebna.php";
require  $_SERVER['ROOT_DIR'] . "/db/ustav.php";

$conn = db_connect();

if (isset($_POST['uziv_cislo'])) {
	$user = new Uzivatel($_POST['uziv_cislo']);

	if (!$user->exists())
		die("Uživatel s ID ".$_POST['uziv_cislo']." neexistuje.");

	if ($_POST['action'] == "upravit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$user->jmeno = $_POST["jmeno"];
		$user->login = $_POST["login"];
		$user->rocnik = $_POST["rocnik"];
		$user->opravneni = $_POST["opravneni"];

		if (empty($_POST["zkratka_ustav"]))
			$user->zkratka_ustav = null;
		else
			$user->zkratka_ustav = $_POST["zkratka_ustav"];

		$retu = $user->update();

		if ($retu)
			echo '<script>window.location.href = "vypis_uz.php?message=edit";</script>';
		else
			echo '<script>window.location.href = "vypis_uz.php?message=edit_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "zmena_hesla")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		if ($_POST["heslo"] != "")
			$retu = $user->set_heslo($_POST["heslo"]);
		else
			$retu = false;

		if ($retu)
			echo '<script>window.location.href = "vypis_uz.php?message=password";</script>';
		else
			echo '<script>window.location.href = "vypis_uz.php?message=password_fail";</script>';
		exit();
	}
	else if ($_POST['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1)
			die("Nemáte oprávnění k úpravám.");

		$retu = $user->delete();

		if ($retu)
			echo '<script>window.location.href = "vypis_uz.php?message=delete";</script>';
		else
			echo '<script>window.location.href = "vypis_uz.php?message=delete_fail";</script>';
		exit();
	}
}

if ($_GET['action'] == "odstranit_obor" && isset($_GET['uziv_cislo']) && isset($_GET['zkratka_obor']))
{
	$retu = delete_zapis_obor_uzivatel($_GET['uziv_cislo'], $_GET['zkratka_obor']);

	if ($retu)
		echo '<script>window.location.href = "uzivatel_d.php?message=delete&uziv_cislo='.$_GET['uziv_cislo'].'";</script>';
	else
		echo '<script>window.location.href = "uzivatel_d.php?message=delete_fail&uziv_cislo='.$_GET['uziv_cislo'].'";</script>';
}
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
    </head>
    <body>
        <script>
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
            
            $('#remove').submit(function() {
                var c = confirm("Click OK to continue?");
                return c;
            });
            
            function show(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "block";
                    document.getElementById("butt1").innerHTML = "Skrýt";
                    document.getElementById("butt1").setAttribute("onClick", "discard(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "block";
                    document.getElementById("butt2").innerHTML = "Skrýt";
                    document.getElementById("butt2").setAttribute("onClick", "discard(2)");
                }
            }

            function discard(i) {
                if (i == 1) {
                    document.getElementById("dis").style.display = "none";
                    document.getElementById("butt1").innerHTML = "Upravit";
                    document.getElementById("butt1").setAttribute("onClick", "show(1)");
                } else if(i == 2){
                    document.getElementById("dis1").style.display = "none";
                    document.getElementById("butt2").innerHTML = "Přidej vybavení";
                    document.getElementById("butt2").setAttribute("onClick", "show(2)");
                }
            }
            
        </script>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
            <?php
            	if ($_GET["action"] == "garant")
            	{
            		$page = 'osobni_roz';
            		$page1 = 'uzivatel_d';
            	}
            	else
            	{
            		$page = 'vypis_hl';
            		$page1 = 'vypis_uz';
            	}
            	include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); 
            ?>
        </header>
        <div class="center">
            <article>
                <section >

<?php
	if (isset($_POST['uziv_cislo'])) {
		  $pom = $_POST['uziv_cislo'];
		  $result = get_uzivatel($_POST['uziv_cislo']);
	} else {
		  $pom = $_GET['uziv_cislo'];
		  $result = get_uzivatel($_GET['uziv_cislo']);
	}

	if ($result->num_rows < 1)
		die("Uživatel s ID ".$pom." neexistuje.");

	$row = $result->fetch_assoc();
	$uziv_cislo = $row["uziv_cislo"];

	echo '<h2>Karta uživatele</h2>';
	echo '<div class="text_l">';
	echo '<b>Login - </b>' . $row["login"] . '</br>';
	echo '<b>Jméno - </b>' . $row["jmeno"] . '</br>';

	echo '</br>';

	if ($row["rocnik"] > 0)
	{
		echo '<b>Student - </b>Ano</br>';
		echo '<b>Ročník - </b>' . $row["rocnik"] . '</br>';
	}
	else
	{
		echo '<b>Student - </b>Ne</br>';
	}

	if (empty($row["zkratka_ustav"]))
		echo '<b>Akademik - </b>Ne</br>';
	else
		echo '<b>Akademik - </b>Ano (' . $row["zkratka_ustav"] . ')</br>';

	echo '</br>';

	if ($row["rocnik"] > 0)
	{
		$res_obory = get_zapsane_obory_uzivatel($uziv_cislo);
		if ($res_obory->num_rows > 0)
		{
			echo '<b>Zapsané obory</b><br/>';
			while ($row_obory = $res_obory->fetch_assoc())
			{
				echo $row_obory["zkratka_obor"] . ' - ' . $row_obory["nazev"];

				if ($_SESSION["opravneni"] == 1 || !empty($_SESSION["zkratka_ustav"]))
					echo ' <a href="'.$_SERVER['PHP_SELF'].'?action=odstranit_obor&uziv_cislo='.$row["uziv_cislo"].'&zkratka_obor='.$row_obory["zkratka_obor"].'" onClick="return confirm(\'Opravdu chcete zrušit registraci oboru '.$row_obory["zkratka_obor"].' studentovi '.$row["login"].'?\')">Odstranit</a>';

				echo '<br/>';
			}
		}
		else
			echo 'Pozor! Student nemá zapsaný žádný obor.<br/>';

		echo '<br/>';
		
		echo '<a href="pridej_ro.php?uziv_cislo='.$row["uziv_cislo"].'">Zapsat obory</a><br/>';
		echo '<a href="pridej_rp.php?uziv_cislo='.$row["uziv_cislo"].'">Zapsat předměty</a><br/>';
		echo '<a href="vypis_rp.php?uziv_cislo='.$row["uziv_cislo"].'">Zobrazit předměty</a><br/>';
		
		echo '<br/><br/>';
	}

	if (!empty($row["zkratka_ustav"]))
	{
		$res_gar = get_garantovane_predmety($uziv_cislo);
		if ($res_gar->num_rows > 0)
		{
			echo '<b>Garantované předměty</b><br/>';
			while ($row_gar = $res_gar->fetch_assoc())
			{
				echo '<a href="predmet_d.php?zkratka_predmet='.$row_gar["zkratka_predmet"].'&ak_rok='.$row_gar["ak_rok"].'">';
				echo $row_gar["zkratka_predmet"].' ('.$row_gar["ak_rok"].')</a> - ' . $row_gar["nazev"] . '<br/>';
			}
		}
	}

	echo'</div>';

	echo '<div id = "zarovne">';

  if ($_SESSION['opravneni'] == 1)
  {
		echo '<button class="button1" id="butt1" onclick="show(1)">Upravit</button>';
		
		echo'<form action="'.$_SERVER['PHP_SELF'].'" method="post" id = "dis">
		    Jméno*: <input type="text" name="jmeno" value="'.$row["jmeno"].'" maxlength="50" required> <br>
		    Login*:   <input type="text" name="login" value="'.$row["login"].'" maxlength="8" required> <br>
		    Ročník:   <input type="text" name="rocnik" value="'.$row["rocnik"].'"> <br>
		    Administrátor: <select name="opravneni" class="sl_style_add">
		    	<option value="0" '.($row["opravneni"] == 0 ? "selected" : "").'>Ne</option>
		    	<option value="1" '.($row["opravneni"] == 1 ? "selected" : "").'>Ano</option>
		    </select>
		    <input type="hidden" name="uziv_cislo" value="'.$row["uziv_cislo"].'"><br/>
		    
		    Ústav:
		    <select class="sl_style_add" name="zkratka_ustav">
		    <option value="">Není akademik</option>';

    $result = get_ustavy();
    if ($result->num_rows > 0) {
        while ($row2 = $result->fetch_assoc()) {
            echo '<option value="' . $row2["zkratka_ustav"] . '"';
            if ($row2["zkratka_ustav"] == $row["zkratka_ustav"]) echo ' selected';
            echo '>' . $row2["zkratka_ustav"] . '</option>';
        }
    } else
        echo "Databáze neobsahuje žádné ústavy.";

		echo'</select> <br>';

		echo '<input type="hidden" name="action" value="upravit">
		<input class="button1" type="submit" value="Upravit"></form>';
  }

  if ($_SESSION['opravneni'] == 1)
  {
		echo '<h3>Změna hesla</h3>';    
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
		echo 'Heslo*: <input type="text" name="heslo" value="" required> <br>';
		echo '<input type="hidden" name="uziv_cislo" value="'.$uziv_cislo.'">';
		echo '<input type="hidden" name="action" value="zmena_hesla">';
		echo '<input class="button1" type="submit" value="Změnit heslo"></form>';
	}

  if ($_SESSION['opravneni'] == 1)
  {
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post" onsubmit="return confirm(\'Opravdu chcete odstranit uživatele?\');">
		    <input type="hidden" name="uziv_cislo" value="'.$uziv_cislo.'">
		    <input type="hidden" name="action" value="odstranit">
		    <input class="button1" type="submit" value="Odstranit">
		</form>';
	}
?>
                         
</div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
