<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

$conn = db_connect();

if (isset($_GET['uziv_cislo']))
{
	$predmet = new Uzivatel($_GET['uziv_cislo']);

	if ($_GET['action'] == "odstranit")
	{
		if ($_SESSION['opravneni'] != 1 && empty($_SESSION['zkratka_ustav']))
			die("Nemáte oprávnění provádět změny.");

		if ($predmet->delete_registrovany_predmet($_GET['zkratka_predmet'], $_GET['ak_rok']))
			echo '<script>window.location.href = "vypis_rp.php?message=delete&uziv_cislo='.$_GET['uziv_cislo'].'";</script>';
		else
			echo '<script>window.location.href = "vypis_rp.php?message=delete_fail&uziv_cislo='.$_GET['uziv_cislo'].'";</script>';
		exit();
	}
}
    
?>

<!DOCTYPE html>


<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
<script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>
	<header id="hlavicka">
	<h1>Učebny - FIT</h1>
		<?php $page = 'sprava_hl'; $page1 = 'vypis_rp'; include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); ?>
	</header>
	<div class="center">
	<article>
	
        <section >

<?php

if (!empty($_SESSION['zkratka_ustav']) || $_SESSION['opravneni'] == 1)
	if (isset($_GET['uziv_cislo']))
		$uziv_cislo = $_GET['uziv_cislo'];
	else
		$uziv_cislo = null;
else
	$uziv_cislo = $_SESSION["uziv_cislo"];

if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
{
	echo '<form action="'.$_SERVER['PHP_SELF'].'" method="get">';

	echo 'Student:
	<select name="uziv_cislo" class="sl_style_add" >';

	if (empty($uziv_cislo))
		echo '<option value="">Zvolte studenta</option>';

	$res_uz = get_studenty();
	while ($row_uz = $res_uz->fetch_assoc())
	{
		echo '<option';
		if ($row_uz["uziv_cislo"] == $_GET["uziv_cislo"]) echo ' selected';
		echo ' value="'.$row_uz["uziv_cislo"].'">'.$row_uz["login"].' ('.$row_uz["jmeno"].', '.$row_uz["rocnik"].'. ročník)</option>';
	}
	
	echo '</select>';

	echo '<input class="button1" type="submit" value="Vybrat">';

	echo '</form>';
}

?>


           <h2>Výpis registrovaných předmětů</h2>
           <table>
            <tr>
   		<th>Zkratka</th>
   		<th>Ak. rok</th>
   		<th>Název</th>
   		<?php if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav'])) echo '<th colspan = "3">Akce</th>'; ?>
   				
   	    </tr>
	<?php
	if (!empty($uziv_cislo))
	{
		$result = get_registrovane_predmety($uziv_cislo);
		if ($result->num_rows > 0)
		{
			while($row = $result->fetch_assoc())
			{
			  echo '<tr><td>'.$row["zkratka_predmet"] . "</td><td> " . $row["ak_rok"] . '</td><td>' . $row["nazev"] . '</td>';
			  
			  if ($_SESSION['opravneni'] == 1 || !empty($_SESSION['zkratka_ustav']))
			  echo '<td><a onclick="return confirm(\'Opravdu chcete zrušit registraci předmětu?\');" href="vypis_rp.php?action=odstranit&zkratka_predmet=' .$row["zkratka_predmet"].'&ak_rok='.$row["ak_rok"].'&uziv_cislo='.$uziv_cislo.'"> Odstranit </a></td>';
			  
			  echo '</tr>';
			}
			echo "</table>";
		}
		else 
			echo "</table>Student nemá registrované žádné předměty.";
	}
	else
		echo "</table>Není zvolen student.";
	?>
        
        
        </section>
        <div class="cleaner"></div>
</article>
</div>
<?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
</body>
</html>
