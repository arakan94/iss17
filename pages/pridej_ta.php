<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/akce.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['nazev'])) {
	$typ_akce = new TypAkce($_POST['nazev']);
    
	if ($typ_akce->exists()) {
		echo '<script>window.location.href = "pridej_ta.php?message=exist";</script>';
		exit();
	} else {
		if ($typ_akce->add())
			echo '<script>window.location.href = "pridej_ta.php?message=success";</script>';
		else
			echo '<script>window.location.href = "pridej_ta.php?message=failure";</script>';
		exit();
	}
}
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });
</script>
    </head>
    <body>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
<?php 
	$page = 'pridej_hl';
	$page1 = 'pridej_ta';
	include( $_SERVER['ROOT_DIR'] . '/inc/menu.php'); 
?>
        </header>
        <div class="center">
            <article>
                <section>
                    <h2>Přidej typ akce</h2>
                    <div id = "zarovne">
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                            Název*:   <input type="text" name="nazev" value="" maxlength="50" required> <br>
                            <input class = "button1" type="submit" value="Vložit">
                        </form>
                    </div>
                </section>
                <div class="cleaner"></div>
            </article>
        </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>
</html>
