<?php
// Author: Přemysl Mlýnek <xmlyne04@stud.fit.vutbr.cz>
// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

require  $_SERVER['ROOT_DIR'] . "/db/db.php";
require  $_SERVER['ROOT_DIR'] . "/db/obor.php";
require  $_SERVER['ROOT_DIR'] . "/db/predmet.php";
require  $_SERVER['ROOT_DIR'] . "/db/ustav.php";
require  $_SERVER['ROOT_DIR'] . "/db/uzivatel.php";

if ($_SESSION['login_status'] == false)
	die("Uživatel není přihlášen.");

if ($_SESSION['opravneni'] != 1)
	die("Nemáte oprávnění přistupovat k této stránce.");

$conn = db_connect();

if (isset($_POST['zkratka_predmet']) && isset($_POST['ak_rok'])) {
    $predmet = new Predmet($_POST['zkratka_predmet'], $_POST['ak_rok']);

    if ($predmet->exists()) {
        echo '<script>window.location.href = "pridej_pr.php?message=exist";</script>';
        exit();
    } else {
        $predmet->nazev = $_POST['nazev'];
        $predmet->rocnik = $_POST['rocnik'];
        $predmet->zkratka_obor = $_POST["zkratka_obor"];
        $predmet->zkratka_ustav = $_POST["zkratka_ustav"];
        $predmet->uziv_cislo = $_POST["uziv_cislo"];
        $predmet->popis = $_POST["popis"];

        if ($predmet->add())
            echo '<script>window.location.href = "pridej_pr.php?message=success";</script>';
        else
            echo '<script>window.location.href = "pridej_pr.php?message=failure";</script>';
        exit();
    }
}
?>

<!DOCTYPE html>

<?php include($_SERVER['ROOT_DIR'] . '/inc/message.php'); ?>

<html lang="cs-cz">
    <head>
        <title>Učebny</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['ROOT_URL'] ?>/css/main.css" >
        <link rel="shortcut icon" href="<?php echo $_SERVER['ROOT_URL'] ?>/images/icon.ico" type="image/x-icon">
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/themes/base/jquery-ui.css" type="text/css" media="all" /> 
        <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" /> 

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.8/jquery-ui.min.js" type="text/javascript"></script>
        <script> 
    $( document ).ready(function() {
        if($("#hide")){
            $("#hide").fadeTo(3000, 400).slideUp(400, function(){
               $("#hide").slideUp(400);
                });   
        }
    });

	function checkForm()
	{
		if (document.getElementById("obor_select").value == "default" ||
			document.getElementById("ustav_select").value == "default")
		{
			alert("Nevyplněny povinné údaje");
			return false;
		}
		else
			return true;
	}
</script>
    </head>
    <body>
        <header id="hlavicka">
            <h1>Učebny - FIT</h1>
<?php
	$page = 'pridej_hl';
	$page1 = 'pridej_pr';
	include( $_SERVER['ROOT_DIR'] . '/inc/menu.php');
?>
        </header>
        <div class="center">
             
            <article>
                
                <section>
                    <h2>Přidej předmět</h2>
                    <div id = "zarovne">
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return checkForm();">
                            Zkratka*: <input type="text" name="zkratka_predmet" value="" maxlength="3" required> <br>
                            Název*:   <input type="text" name="nazev" value="" maxlength="50" required> <br>
                            Akademický rok*:   <input type="number" name="ak_rok" id="ak_rok" value="" min="2002" max="9999" required> <br>

                            Garant*: <select name="uziv_cislo" class="sl_style_add" id="garant_select">
                            <?php
				                      if (!empty($_SESSION['zkratka_ustav']))
				                      	echo '<option value="'.$_SESSION['uziv_cislo'].'">'.$_SESSION['jmeno'].' (já)</option>';
				                      else
				                      	echo '<option value="default">Vyberte garanta</option>';

                                $result = get_akademiky();
                                if ($result->num_rows > 0) {
		                              while ($row = $result->fetch_assoc())
		                              {
		                              	if ($row["uziv_cislo"] != $_SESSION['uziv_cislo'])
		                                  echo '<option value="'.$row["uziv_cislo"].'">'.$row["login"].' ('.$row["jmeno"].', '.$row['zkratka_ustav'].')</option>';
		                              }
                                }
                                else
                                	echo "Databáze neobsahuje žádné akademiky.";
                            ?> 
                                </select>

                            Zkratka obor*: <select name="zkratka_obor" id="obor_select" class="sl_style_add">
                                <option value="default">Vyberte obor</option>
                                    <?php
                                    $result = get_obory();
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_assoc()) {
                                            echo '<option value="'.$row["zkratka_obor"].'">' .$row["zkratka_obor"]. '</option>';
                                        }
                                    } else
                                        echo "0 results";
                                    ?> 
                                            </select>
                             Zkratka ústav*: <select name="zkratka_ustav" id="ustav_select" class="sl_style_add">
                                <option value="default">Vyberte ústav</option>
                                    <?php
                                    $result = get_ustavy();
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_assoc()) {
                                            echo '<option value="'.$row["zkratka_ustav"].'">' .$row["zkratka_ustav"]. '</option>';
                                        }
                                    } else
                                        echo "0 results";
                                    ?> 
                                            </select>
                                            <input type="hidden" name="rocnik" value="0"> <br>
                                            Popis:   <input type="text" name="popis" value="" maxlength="250"> <br>
                                <input class = "button1" type="submit" value="Vložit">
                        </form>
                    </div>
                </section>
                   
                <div class="cleaner"></div>
                
            </article>
            </div>
        <?php include($_SERVER['ROOT_DIR'] . '/inc/footer.php'); ?>
    </body>

</html>
