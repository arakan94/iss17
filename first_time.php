<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

if (!isset($_SERVER['ROOT_URL']))
	die("Proměnná ROOT_URL není nastavená! Proveďte nastavení v .htaccess");

if (!isset($_SERVER['ROOT_DIR']))
	die("Proměnná ROOT_DIR není nastavená! Proveďte nastavení v .htaccess");

	session_start();

	$_SESSION['uziv_cislo'] = 0;
	$_SESSION['login'] = "first";
	$_SESSION['jmeno'] = "First time setup";
	$_SESSION['opravneni'] = 1;
	$_SESSION['rocnik'] = 0;
	$_SESSION['zkratka_ustav'] = "";
	$_SESSION['last_activity'] = time();
	$_SESSION['login_status'] = true;

	header("Location: " . $_SERVER['ROOT_URL'] . "/pages/pridej_uz.php?action=prihlaseni");
?>
