-- Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

CREATE TABLE uzivatel(
  uziv_cislo int AUTO_INCREMENT PRIMARY KEY,
  jmeno varchar(50) NOT NULL,
  login char(8) NOT NULL,
  heslo char(255) NOT NULL,
  rocnik tinyint,
  opravneni tinyint NOT NULL, -- 0 neni admin, 1 je admin
  zkratka_ustav char(4),
  
  CHECK (rocnik > 0 OR rocnik = NULL),
  CHECK (opravneni = 0 OR opravneni = 1)
);

CREATE TABLE ustav (
  zkratka_ustav char(4) PRIMARY KEY,
  nazev varchar(50) NOT NULL
);

CREATE TABLE obor (
  zkratka_obor char(3) PRIMARY KEY,
  -- zkratka_ustav char(4) NOT NULL,
  nazev varchar(50) NOT NULL
);

CREATE TABLE zapsany_obor (
  uziv_cislo int NOT NULL,
  zkratka_obor char(3) NOT NULL,
  PRIMARY KEY (uziv_cislo, zkratka_obor)
);

CREATE TABLE predmet(
  zkratka_predmet char(3) NOT NULL,
  ak_rok smallint NOT NULL,
  uziv_cislo int,  -- garant
  zkratka_obor char(3),
  zkratka_ustav char(4),
  rocnik tinyint, -- 0 videt mohou vsichni, jinak od daneho rocniku
  nazev varchar(50) NOT NULL,
  popis varchar(250),
  
  CHECK (ak_rok >= 2002),
  CHECK (rocnik >= 0),
  
  PRIMARY KEY (zkratka_predmet, ak_rok)
);

CREATE TABLE registrovany_predmet(
  zkratka_predmet char(3) NOT NULL,
  ak_rok smallint NOT NULL,
  uziv_cislo int NOT NULL,
  
  CHECK (ak_rok >= 2002),
  PRIMARY KEY (zkratka_predmet, ak_rok, uziv_cislo)
);

CREATE TABLE akce(
  zkratka_predmet char(3) NOT NULL,
  ak_rok smallint NOT NULL,
  typ_id int NOT NULL,
  uziv_cislo int,
  nazev varchar(50) NOT NULL,
  popis varchar(250),
  
  CHECK (ak_rok >= 2002),
  PRIMARY KEY (zkratka_predmet, ak_rok, typ_id)
);

CREATE TABLE typ_akce(
  typ_id int AUTO_INCREMENT PRIMARY KEY,
  nazev varchar(50) NOT NULL,

  CONSTRAINT typ_akce_uq UNIQUE (nazev)
);

CREATE TABLE ucebna(
  ucebna_id int AUTO_INCREMENT PRIMARY KEY,
  budova char(1) NOT NULL,
  patro tinyint NOT NULL,
  cislo_mistnosti tinyint NOT NULL,
  kapacita int NOT NULL,
  popis varchar(250),
  
  CHECK (cislo_mistnosti > 0),
  CHECK (kapacita >= 0),

  CONSTRAINT ucebna_uq UNIQUE (budova, patro, cislo_mistnosti)
);

CREATE TABLE vybaveni(  -- prace se slozenym primarnim klicem s vybav_id pocitanymi pri pridavani nefungovala v MySQL (narozdil od Oracle SQL)
  vybav_id int AUTO_INCREMENT PRIMARY KEY,
  ucebna_id int NOT NULL,
  nazev varchar(50) NOT NULL,
  typ varchar(50) NOT NULL,
  popis varchar(250)
);

CREATE TABLE rezervace(
  rezerv_id int AUTO_INCREMENT PRIMARY KEY,
  zkratka_predmet char(3) NOT NULL,
  ak_rok smallint NOT NULL,
  typ_id int NOT NULL,
  uziv_cislo int,
  ucebna_id int NOT NULL,
  zacatek datetime(6) NOT NULL,
  konec datetime(6) NOT NULL,
  poznamka varchar(250),
  
  -- zajisteni smysluplnych hodnot casu
  CHECK (ak_rok >= 2002 AND YEAR(zacatek) >= 2002 AND YEAR(konec) >= 2002)
);

-- vytvoreni cizich klicu
ALTER TABLE uzivatel ADD FOREIGN KEY (zkratka_ustav) REFERENCES ustav(zkratka_ustav) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE zapsany_obor ADD FOREIGN KEY (uziv_cislo) REFERENCES uzivatel(uziv_cislo) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE zapsany_obor ADD FOREIGN KEY (zkratka_obor) REFERENCES obor(zkratka_obor) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE predmet ADD FOREIGN KEY (uziv_cislo) REFERENCES uzivatel(uziv_cislo) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE predmet ADD FOREIGN KEY (zkratka_obor) REFERENCES obor(zkratka_obor) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE predmet ADD FOREIGN KEY (zkratka_ustav) REFERENCES ustav(zkratka_ustav) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE registrovany_predmet ADD FOREIGN KEY (zkratka_predmet, ak_rok) REFERENCES predmet(zkratka_predmet, ak_rok) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE registrovany_predmet ADD FOREIGN KEY (uziv_cislo) REFERENCES uzivatel(uziv_cislo) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE akce ADD FOREIGN KEY (zkratka_predmet, ak_rok) REFERENCES predmet(zkratka_predmet, ak_rok) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE akce ADD FOREIGN KEY (typ_id) REFERENCES typ_akce(typ_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE akce ADD FOREIGN KEY (uziv_cislo) REFERENCES uzivatel(uziv_cislo) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE vybaveni ADD FOREIGN KEY (ucebna_id) REFERENCES ucebna(ucebna_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rezervace ADD FOREIGN KEY (zkratka_predmet, ak_rok, typ_id) REFERENCES akce(zkratka_predmet, ak_rok, typ_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rezervace ADD FOREIGN KEY (ucebna_id) REFERENCES ucebna(ucebna_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE rezervace ADD FOREIGN KEY (uziv_cislo) REFERENCES uzivatel(uziv_cislo) ON DELETE SET NULL ON UPDATE CASCADE;

-- ALTER TABLE obor ADD FOREIGN KEY (zkratka_ustav) REFERENCES ustav(zkratka_ustav) ON DELETE CASCADE ON UPDATE CASCADE;

