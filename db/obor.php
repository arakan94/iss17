<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Obor {
	private $zkratka_obor;
	public $nazev;

	private $existing = false;

	public function __construct($zkratka_obor)
	{
		$res = get_obor($zkratka_obor);
		$this->zkratka_obor = $zkratka_obor;

		if ($res->num_rows > 0)  // obor nalezen
		{
			$tmp = $res->fetch_assoc();
			$this->nazev = $tmp["nazev"];
			$this->existing = true;
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if (add_obor($this->zkratka_obor, $this->nazev))
		{
			$this->existing = true;
			return true;
		}
		else
			return false;
	}

	public function update()
	{
		if ($this->existing == false)
			return false;

		return update_obor($this->zkratka_obor, $this->nazev);
	}

	public function change_primary_key($new_zkratka_obor)
	{
		if (empty($new_zkratka_obor))
			return false;

		if ($this->existing == true)
			$res = update_primary_key_obor($this->zkratka_obor, $new_zkratka_obor);
		else
			$res = true;

		$this->zkratka_obor = $new_zkratka_obor;

		return $res;
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		if (delete_obor($this->zkratka_obor))
		{
			$this->existing = false;
			return true;
		}
		else
			return false;
	}
}

function get_obor($zkratka_obor)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM obor WHERE zkratka_obor = ? LIMIT 1");
	$q->bind_param("s", $zkratka_obor);
	$q->execute();
	return $q->get_result();
}

function get_obory()
{
	global $conn;
	return $conn->query("SELECT * FROM obor");
}

function add_obor($zkratka_obor, $nazev)
{
	if (empty($zkratka_obor) || empty($nazev))
		return false;

	global $conn;
	$q = $conn->prepare("INSERT INTO obor (zkratka_obor, nazev) VALUES (?, ?)");
	$q->bind_param("ss", $zkratka_obor, $nazev);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function update_obor($zkratka_obor, $nazev)
{
	if (empty($zkratka_obor) || empty($nazev))
		return false;

	global $conn;
	$q = $conn->prepare("UPDATE obor SET nazev = ? WHERE zkratka_obor = ?");
	$q->bind_param("ss", $nazev, $zkratka_obor);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function update_primary_key_obor($zkratka_obor, $new_zkratka_obor)
{
	if (empty($zkratka_obor) || empty($new_zkratka_obor))
		return false;

	global $conn;
	$q = $conn->prepare("UPDATE obor SET zkratka_obor = ? WHERE zkratka_obor = ?");
	$q->bind_param("ss", $new_zkratka_obor, $zkratka_obor);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function delete_obor($zkratka_obor)
{
	if (empty($zkratka_obor))
		return false;

	global $conn;
	$q = $conn->prepare("DELETE FROM obor WHERE zkratka_obor = ? LIMIT 1");
	$q->bind_param("s", $zkratka_obor);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}
?>
