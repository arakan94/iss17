<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Ucebna {
	private $ucebna_id;

	public $budova;
	public $patro;
	public $cislo_mistnosti;
	public $kapacita;
	public $popis;

	private $existing = false;

	public function __construct()
	{
		$cnt = func_num_args();
		$args = func_get_args();

		if ($cnt == 1)    // $ucebna_id
		{
			$res = get_ucebna($args[0]);
			if ($res->num_rows > 0)  // ucebna nalezena
			{
				$tmp = $res->fetch_assoc();
				$this->ucebna_id = $tmp["ucebna_id"];
				$this->budova = $tmp["budova"];
				$this->patro = $tmp["patro"];
				$this->cislo_mistnosti = $tmp["cislo_mistnosti"];
				$this->kapacita = $tmp["kapacita"];
				$this->popis = $tmp["popis"];
				$this->existing = true;
			}
		}
		else if ($cnt == 3) // $budova, $patro, $cislo_mistnosti
		{
			$res = get_ucebna_by_location($args[0], $args[1], $args[2]);
			if ($res->num_rows > 0)
			{
				$tmp = $res->fetch_assoc();
				$this->ucebna_id = $tmp["ucebna_id"];
				$this->budova = $tmp["budova"];
				$this->patro = $tmp["patro"];
				$this->cislo_mistnosti = $tmp["cislo_mistnosti"];
				$this->kapacita = $tmp["kapacita"];
				$this->popis = $tmp["popis"];
				$this->existing = true;
			}
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function get_id()
	{
		return $this->ucebna_id;
	}

	public function is_free($start, $konec)
	{
		if ($this->existing == false)
			return false;

		return is_ucebna_volna($this->ucebna_id, $start, $konec);
	}

	public function add()
	{
		if ($this->existing == true)
			return false;

		if (empty($this->budova) || empty($this->patro) || $this->cislo_mistnosti <= 0 || $this->kapacita < 0)
			return false;

		global $conn;
		$q = $conn->prepare("INSERT INTO ucebna (budova, patro, cislo_mistnosti, kapacita, popis) VALUES (?, ?, ?, ?, ?)");
		$q->bind_param("siiis", $this->budova, $this->patro, $this->cislo_mistnosti, $this->kapacita, $this->popis);

		if ($q->execute())
		{
			$this->ucebna_id = $conn->insert_id;
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if ($this->existing == false)
			return false;

		if (empty($this->budova) || empty($this->patro) || $this->cislo_mistnosti <= 0 || $this->kapacita < 0)
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE ucebna SET budova = ?, patro = ?, cislo_mistnosti = ?, kapacita = ?, popis = ? WHERE ucebna_id = ?");
		$q->bind_param("siiisi", $this->budova, $this->patro, $this->cislo_mistnosti, $this->kapacita, $this->popis, $this->ucebna_id);

		if ($q->execute())
		{
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		global $conn;
		$q = $conn->prepare("DELETE FROM ucebna WHERE ucebna_id = ?");
		$q->bind_param("i", $this->ucebna_id);

		if ($q->execute())
		{
			$this->existing = false;
			unset($this->ucebna_id);
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	// ocekava objekt tridy Vybaveni
	public function add_vybaveni($vybaveni)
	{
		if ($this->existing == false)
			return false;

		return $vybaveni->add($this-ucebna_id);
	}

	public function get_vybaveni()
	{
		if ($this->existing == false)
			return false;

		return get_vybaveni_ucebna($this->ucebna_id);
	}
}

function get_ucebna($ucebna_id)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM ucebna WHERE ucebna_id = ? LIMIT 1");
	$q->bind_param("i", $ucebna_id);
	$q->execute();
	return $q->get_result();
}

function get_ucebna_by_location($budova, $patro, $cislo_mistnosti)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM ucebna WHERE budova = ? AND patro = ? AND cislo_mistnosti = ? LIMIT 1");
	$q->bind_param("sii", $budova, $patro, $cislo_mistnosti);
	$q->execute();
	return $q->get_result();
}

function get_ucebny()
{
	global $conn;
	return $conn->query("SELECT * FROM ucebna ORDER BY ucebna.budova");
}

function get_ucebny_by_vybaveni($typ)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM ucebna
	INNER JOIN vybaveni ON vybaveni.ucebna_id=ucebna.ucebna_id
	WHERE vybaveni.typ=?
	ORDER BY ucebna.budova");
	$q->bind_param("s", $typ);
	$q->execute();
	return $q->get_result();
}

function get_ucebna_by_vybaveni($typ)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM ucebna
	INNER JOIN vybaveni ON vybaveni.ucebna_id=ucebna.ucebna_id
	WHERE vybaveni.typ=? LIMIT 1");
	$q->bind_param("s", $typ);
	$q->execute();
	return $q->get_result();
}

function get_ucebny_by_vybaveni_OR()
{
	$num = func_num_args();
	$args = func_get_args();

	if ($num < 1)
		return false;

	$query = "SELECT DISTINCT * FROM ucebna
	INNER JOIN vybaveni ON vybaveni.ucebna_id=ucebna.ucebna_id
	WHERE vybaveni.typ=?";

	$type = "s";

	for ($i = 1; $i < $num; $i++)
	{
		$query .= " OR vybaveni.typ=?";
		$type  .= "s";
	}

	array_unshift($args, $type);  // vlozit typovy string na zacatek pole

	global $conn;
	$q = $conn->prepare($query);
	call_user_func_array( array($q, 'bind_param'), $args );
	$q->execute();
	return $q->get_result();
}

# nefunguje
#function get_volne_ucebny($start, $konec)
#{
#	global $conn;
#	$q = $conn->prepare("SELECT DISTINCT ucebna.* FROM ucebna
#	LEFT JOIN rezervace ON ucebna.ucebna_id=rezervace.ucebna_id
#	WHERE ((rezervace.konec <= ?) OR (rezervace.zacatek >= ?))
#	ORDER BY ucebna.budova");
#	$q->bind_param("ss", $start, $konec);
#	$q->execute();
#	return $q->get_result();
#}

function is_ucebna_volna($ucebna_id, $start, $konec)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM ucebna
	INNER JOIN rezervace ON ucebna.ucebna_id=rezervace.ucebna_id
	WHERE ((rezervace.konec > ? AND rezervace.konec <= ?) OR (rezervace.zacatek >= ? AND rezervace.zacatek < ?)) AND ucebna.ucebna_id=?");
	$q->bind_param("ssssi", $start, $konec, $start, $konec, $ucebna_id);
	$q->execute();
    $q->store_result();
	if ($q->num_rows > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}


class Vybaveni {
	private $ucebna_id;
	private $vybav_id;

	public $nazev;
	public $typ;
	public $popis;

	private $existing = false;

	public function __construct($ucebna_id, $vybav_id)
	{
		if (!empty($ucebna_id) && !empty($vybav_id))
		{
			$res = get_vybaveni($ucebna_id, $vybav_id);
			if ($res->num_rows > 0)  // vybaveni nalezeno
			{
				$tmp = $res->fetch_assoc();
				$this->ucebna_id = $tmp["ucebna_id"];
				$this->vybav_id = $tmp["vybav_id"];
				$this->nazev = $tmp["nazev"];
				$this->typ = $tmp["typ"];
				$this->popis = $tmp["popis"];
				$this->existing = true;
			}
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function get_ucebna_id()
	{
		return $this->ucebna_id;
	}
	
	public function get_id()
	{
		return $this->vybav_id;
	}

	public function add($ucebna_id)
	{
		if ($this->existing == true)
			return false;

		if (empty($ucebna_id) || empty($this->nazev) || empty($this->typ))
			return false;

		// konverze typu na spolecny tvar: Typ
		$this->typ = strtolower($this->typ);
		$this->typ = ucfirst($this->typ);

		global $conn;
		$q = $conn->prepare("INSERT INTO vybaveni (ucebna_id, nazev, typ, popis) VALUES (?, ?, ?, ?)");
		$q->bind_param("isss", $ucebna_id, $this->nazev, $this->typ, $this->popis);

		if ($q->execute())
		{
			$this->ucebna_id = $ucebna_id;
			$this->vybav_id = $conn->insert_id;
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if ($this->existing == false || empty($this->vybav_id))
			return false;

		if (empty($this->nazev) || empty($this->typ))
			return false;

		$this->typ = strtolower($this->typ);
		$this->typ = ucfirst($this->typ);

		global $conn;
		$q = $conn->prepare("UPDATE vybaveni SET nazev = ?, typ = ?, popis = ? WHERE ucebna_id = ? AND vybav_id = ?");
		$q->bind_param("sssii", $this->nazev, $this->typ, $this->popis, $this->ucebna_id, $this->vybav_id);

		if ($q->execute())
		{
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false || empty($this->vybav_id))
			return false;

		global $conn;
		$q = $conn->prepare("DELETE FROM vybaveni WHERE ucebna_id = ? AND vybav_id = ?");
		$q->bind_param("ii", $this->ucebna_id, $this->vybav_id);

		if ($q->execute())
		{
			$this->existing = false;
			unset($this->ucebna_id);
			unset($this->vybav_id);
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}
}


function get_vybaveni($ucebna_id, $vybav_id)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM vybaveni WHERE ucebna_id = ? AND vybav_id = ? LIMIT 1");
	$q->bind_param("ii", $ucebna_id, $vybav_id);
	$q->execute();
	return $q->get_result();
}

function get_vybaveni_typ($ucebna_id, $typ)
{
	$typ = strtolower($typ);
	$typ = ucfirst($typ);

	global $conn;
	$q = $conn->prepare("SELECT * FROM vybaveni WHERE ucebna_id = ? AND typ = ? ORDER BY typ ASC, nazev ASC");
	$q->bind_param("is", $ucebna_id, $typ);
	$q->execute();
	return $q->get_result();
}

function get_all_vybaveni_typ($typ)
{
	$typ = strtolower($typ);
	$typ = ucfirst($typ);

	global $conn;
	$q = $conn->prepare("SELECT * FROM vybaveni WHERE typ = ? ORDER BY typ ASC, nazev ASC");
	$q->bind_param("s", $typ);
	$q->execute();
	return $q->get_result();
}

function get_vybaveni_ucebna($ucebna_id)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM vybaveni WHERE ucebna_id = ?");
	$q->bind_param("i", $ucebna_id);
	$q->execute();
	return $q->get_result();
}

function get_all_vybaveni()
{
	global $conn;
	return $conn->query("SELECT ucebna.budova, ucebna.patro, ucebna.cislo_mistnosti, vybaveni.* FROM vybaveni
	LEFT JOIN ucebna ON vybaveni.ucebna_id=ucebna.ucebna_id
	ORDER BY ucebna.budova, ucebna.patro, ucebna.cislo_mistnosti, vybaveni.nazev");
}

function get_typy_vybaveni()
{
	global $conn;
	return $conn->query("SELECT DISTINCT typ FROM vybaveni ORDER BY typ");
}

function get_typy_vybaveni_ucebna($ucebna_id)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT typ FROM vybaveni WHERE ucebna_id = ? ORDER BY typ");
	$q->bind_param("i", $ucebna_id);
	$q->execute();
	return $q->get_result();
}

?>
