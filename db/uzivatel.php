<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Uzivatel {
	public $jmeno;
	public $login;
	public $rocnik;
	public $zkratka_ustav;
	public $opravneni;

	private $uziv_cislo;
	private $heslo;
	private $existing = false;

	public function __construct($id)
	{
		$res = get_uzivatel($id);
		
		if ($res->num_rows > 0)  // uzivatel nalezen
		{
			$usr = $res->fetch_assoc();
			$this->jmeno = $usr["jmeno"];
			$this->login = $usr["login"];
			$this->heslo = $usr["heslo"];
			$this->rocnik = $usr["rocnik"];
			$this->zkratka_ustav = $usr["zkratka_ustav"];
			$this->opravneni = $usr["opravneni"];
			$this->uziv_cislo = $usr["uziv_cislo"];
			$this->existing = true;
		}
		else
		{
			if (!is_numeric($id))
				$this->login = $id;
		}
	}

	public function get_uziv_cislo()
	{
		return $this->uziv_cislo;
	}

	public function get_id()
	{
		return $this->uziv_cislo;
	}

	public function exists()
	{
		return $this->existing;
	}

	public function is_spravce()
	{
		if ($this->opravneni == 1)
			return true;
		else
			return false;
	}

	public function is_akademik()
	{
		if (empty($this->zkratka_ustav))
			return false;
		else
			return true;
	}

	public function is_student()
	{
		if (empty($this->rocnik) || $this->rocnik < 1)
			return false;	
		else
			return true;
	}

	public function check_heslo($heslo)
	{
		return password_verify($heslo, $this->heslo);
	}

	public function set_heslo($heslo)
	{
		$this->heslo = password_hash($heslo, PASSWORD_DEFAULT);

		if ($this->existing == true)
			$res = $this->update_heslo();
		else
			$res = true;

		return $res;
	}

	private function update_heslo()
	{
		global $conn;
		$q = $conn->prepare("UPDATE uzivatel SET heslo = ? WHERE uziv_cislo = ?");
		$q->bind_param("si", $this->heslo, $this->uziv_cislo);

		if ($q->execute())
			return true;
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function add()
	{
		if ($this->existing == true)
			return false;

		if (empty($this->jmeno) || empty($this->login) || empty($this->heslo) || ($this->opravneni != 0 && $this->opravneni != 1))
			return false;

		global $conn;
		$q = $conn->prepare("INSERT INTO uzivatel (jmeno, login, heslo, rocnik, zkratka_ustav, opravneni) VALUES (?, ?, ?, ?, ?, ?)");
		$q->bind_param("sssisi", $this->jmeno, $this->login, $this->heslo, $this->rocnik, $this->zkratka_ustav, $this->opravneni);

		if ($q->execute())
		{
			$this->uziv_cislo = $conn->insert_id;
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if ($this->existing == false)
			return false;

		if (empty($this->jmeno) || empty($this->login) || empty($this->heslo) || ($this->opravneni != 0 && $this->opravneni != 1))
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE uzivatel SET jmeno = ?, login = ?, heslo = ?, rocnik = ?, zkratka_ustav = ?, opravneni = ? WHERE uziv_cislo = ?");
		$q->bind_param("sssisii", $this->jmeno, $this->login, $this->heslo, $this->rocnik, $this->zkratka_ustav, $this->opravneni, $this->uziv_cislo);

		if ($q->execute())
			return true;
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		$res = delete_uzivatel($this->uziv_cislo);

		if ($res == true)
		{
			unset($this->uziv_cislo);
			$this->existing = false;
		}

		return $res;
	}

	public function zapsane_obory()
	{
		return get_zapsane_obory_uzivatel($this->uziv_cislo);
	}

	public function add_zapsany_obor($zkratka_obor)
	{
		return zapis_obor_uzivatel($this->uziv_cislo, $zkratka_obor);
	}

	public function delete_zapsany_obor($zkratka_obor)
	{
		return delete_zapis_obor_uzivatel($this->uziv_cislo, $zkratka_obor);
	}

	public function registrovane_predmety()
	{
		return get_registrovane_predmety($this->uziv_cislo);
	}

	public function add_registrovany_predmet($zkratka_predmet, $ak_rok)
	{
		return add_registrace_predmetu($this->uziv_cislo, $zkratka_predmet, $ak_rok);
	}

	public function delete_registrovany_predmet($zkratka_predmet, $ak_rok)
	{
		return delete_registrace_predmetu($this->uziv_cislo, $zkratka_predmet, $ak_rok);
	}
};

function get_uzivatel($id)
{
	global $conn;

	if (is_numeric($id))
	{
		$q = $conn->prepare("SELECT * FROM uzivatel WHERE uziv_cislo = ? LIMIT 1");
		$q->bind_param("i", $id);
	}
	else
	{
		$q = $conn->prepare("SELECT * FROM uzivatel WHERE login = ? LIMIT 1");
		$q->bind_param("s", $id);
	}

	$q->execute();
	return $q->get_result();
}

function get_uzivatele()
{
	global $conn;
	return $conn->query("SELECT * FROM uzivatel ORDER BY login, jmeno");
}

function get_spravce()
{
	global $conn;
	return $conn->query("SELECT * FROM uzivatel WHERE opravneni = 1 ORDER BY login, jmeno");
}

function get_akademiky()
{
	global $conn;
	return $conn->query("SELECT * FROM uzivatel WHERE zkratka_ustav <> '' AND zkratka_ustav IS NOT NULL ORDER BY login, jmeno");
}

function get_studenty()
{
	global $conn;
	return $conn->query("SELECT * FROM uzivatel	WHERE rocnik > 0 ORDER BY login, jmeno");
}

function get_studenty_obor($zkratka_obor)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM uzivatel
	LEFT JOIN zapsany_obor ON uzivatel.uziv_cislo=zapsany_obor.uziv_cislo
	WHERE zapsany_obor.zkratka_obor = ?
	ORDER BY uzivatel.login, uzivatel.jmeno");
	$q->bind_param("s", $zkratka_obor);
	$q->execute();
	return $q->get_result();

	global $conn;
}

function delete_uzivatel($id)
{
	global $conn;

	if (is_numeric($id))
	{
		$q = $conn->prepare("DELETE FROM uzivatel WHERE uziv_cislo = ? LIMIT 1");
		$q->bind_param("i", $id);
	}
	else
	{
		$q = $conn->prepare("DELETE FROM uzivatel WHERE login = ? LIMIT 1");
		$q->bind_param("s", $id);
	}

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

/* ZAPSANE OBORY */
function get_zapsane_obory_uzivatel($uziv_cislo)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM zapsany_obor
	JOIN obor ON zapsany_obor.zkratka_obor=obor.zkratka_obor
	WHERE zapsany_obor.uziv_cislo = ?
	ORDER BY zapsany_obor.zkratka_obor");
	$q->bind_param("i", $uziv_cislo);
	$q->execute();
	return $q->get_result();
}

function zapis_obor_uzivatel($uziv_cislo, $zkratka_obor)
{
	if (empty($zkratka_obor) || empty($uziv_cislo))
		return false;

	global $conn;
	$q = $conn->prepare("INSERT INTO zapsany_obor VALUES (?, ?)");
	$q->bind_param("is", $uziv_cislo, $zkratka_obor);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function delete_zapis_obor_uzivatel($uziv_cislo, $zkratka_obor)
{
	if (empty($zkratka_obor) || empty($uziv_cislo))
		return false;

	global $conn;
	$q = $conn->prepare("DELETE FROM zapsany_obor WHERE uziv_cislo = ? AND zkratka_obor = ?");
	$q->bind_param("is", $uziv_cislo, $zkratka_obor);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

/* REGISTROVANE PREDMETY */

function get_registrovane_predmety($uziv_cislo)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM registrovany_predmet 
	JOIN predmet ON registrovany_predmet.zkratka_predmet=predmet.zkratka_predmet AND registrovany_predmet.ak_rok=predmet.ak_rok 
	WHERE registrovany_predmet.uziv_cislo = ?
	ORDER BY registrovany_predmet.zkratka_predmet");
	$q->bind_param("i", $uziv_cislo);
	$q->execute();
	return $q->get_result();
}

function add_registrace_predmetu($uziv_cislo, $zkratka_predmet, $ak_rok)
{
	if (empty($zkratka_predmet) || empty($uziv_cislo) || empty($ak_rok))
		return false;

	global $conn;
	$q = $conn->prepare("INSERT INTO registrovany_predmet VALUES (?, ?, ?)");
	$q->bind_param("sii", $zkratka_predmet, $ak_rok, $uziv_cislo);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function delete_registrace_predmetu($uziv_cislo, $zkratka_predmet, $ak_rok)
{
	if (empty($zkratka_predmet) || empty($uziv_cislo) || empty($ak_rok))
		return false;

	global $conn;
	$q = $conn->prepare("DELETE FROM registrovany_predmet WHERE zkratka_predmet = ? AND ak_rok = ? AND uziv_cislo = ?");
	$q->bind_param("sii", $zkratka_predmet, $ak_rok, $uziv_cislo);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

/* GARANT */
function get_garantovane_predmety($uziv_cislo)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT zkratka_predmet, ak_rok, nazev FROM predmet
	WHERE predmet.uziv_cislo = ?
	ORDER BY zkratka_predmet, nazev");
	$q->bind_param("i", $uziv_cislo);
	$q->execute();
	return $q->get_result();
}

?>
