<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

	function db_connect()
	{
		$server = "localhost";
		$socket = "/var/run/mysql/mysql.sock";
		$db     = "xnovak1m";
		$user   = "xnovak1m";
		$pass   = "eji4oram";

		$conn = new mysqli($server, $user, $pass, $db, null, $socket);

		if ($conn->connect_error)
			die("Connection failed: " . $conn->connect_error);

		$conn->set_charset("utf8");

		return $conn;
	}

	session_start();

	if ($_SESSION['login_status'] == true)
	{
		if (time() - $_SESSION['last_activity'] > 1200)  // auto logout po 20 minutach
		{
			$_SESSION['login_status'] = false;
			header("Location: " . $_SERVER['ROOT_URL'] . "/pages/prihlaseni.php?action=auto_odhlaseni");
			exit();
		}
		else
			$_SESSION['last_activity'] = time();
	}
?>
