<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Predmet {
	private $zkratka_predmet;
	private $ak_rok;

	public $zkratka_obor = NULL;
	public $zkratka_ustav = NULL;
	public $uziv_cislo = NULL;    // garant predmetu

	public $rocnik = 0;
	public $nazev;
	public $popis = NULL;

	private $existing = false;

	public function __construct($zkratka_predmet, $ak_rok)
	{
		$res = get_predmet($zkratka_predmet, $ak_rok);
		
		if ($res->num_rows > 0)  // predmet nalezen
		{
			$tmp = $res->fetch_assoc();
			$this->zkratka_predmet = $tmp["zkratka_predmet"];
			$this->ak_rok = $tmp["ak_rok"];
			$this->zkratka_obor = $tmp["zkratka_obor"];
			$this->zkratka_ustav = $tmp["zkratka_ustav"];
			$this->uziv_cislo = $tmp["uziv_cislo"];
			$this->rocnik = $tmp["rocnik"];
			$this->nazev = $tmp["nazev"];
			$this->popis = $tmp["popis"];
			$this->existing = true;
		}
		else
		{
			$this->zkratka_predmet = $zkratka_predmet;
			$this->ak_rok = $ak_rok;
			$this->existing = false;
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if (empty($this->zkratka_predmet) || empty($this->ak_rok) || empty($this->nazev) || $this->rocnik < 0 || $this->ak_rok < 2002)
		{
			echo "Nejsou vyplněny povinné položky, Predmet->add()\n";
			return false;
		}

		if ($this->existing == true)
		{
			echo "Predmet " . $this->nazev . " je již v databázi zapsán - použijte Predmet->Update()\n";
			return false;
		}

		global $conn;
		$q = $conn->prepare("INSERT INTO predmet (zkratka_predmet, ak_rok, uziv_cislo, zkratka_obor, zkratka_ustav, rocnik, nazev, popis) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		$q->bind_param("siississ", $this->zkratka_predmet, $this->ak_rok, $this->uziv_cislo, $this->zkratka_obor, $this->zkratka_ustav, $this->rocnik, $this->nazev, $this->popis);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if (empty($this->zkratka_predmet) || empty($this->ak_rok) || empty($this->nazev) || $this->rocnik < 0 || $this->ak_rok < 2002 || $this->existing == false)
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE predmet SET uziv_cislo = ?, zkratka_obor = ?, zkratka_ustav = ?, rocnik = ?, nazev = ?, popis = ? WHERE zkratka_predmet = ? AND ak_rok = ?");
		$q->bind_param("ississsi", $this->uziv_cislo, $this->zkratka_obor, $this->zkratka_ustav, $this->rocnik, $this->nazev, $this->popis, $this->zkratka_predmet, $this->ak_rok);

		if ($q->execute())
			return true;
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function change_primary_key($new_zkratka_predmet, $new_ak_rok)
	{
		if (empty($new_zkratka_predmet) || $new_ak_rok < 2002)
			return false;

		if ($this->existing == true)
			$res = $this->update_primary_key($new_zkratka_predmet, $new_ak_rok);
		else
			$res = true;

		$this->zkratka_predmet = $new_zkratka_predmet;
		$this->ak_rok = $new_ak_rok;

		return $res;
	}

	private function update_primary_key($new_zkratka_predmet, $new_ak_rok)
	{
		global $conn;
		$q = $conn->prepare("UPDATE predmet SET zkratka_predmet = ?, ak_rok = ? WHERE zkratka_predmet = ? AND ak_rok = ?");
		$q->bind_param("sisi", $new_zkratka_predmet, $new_ak_rok, $this->zkratka_predmet, $this->ak_rok);

		if ($q->execute())
			return true;
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		$res = delete_predmet($this->zkratka_predmet, $this->ak_rok);		

		if ($res)
		{
			$this->existing = false;
		}

		return $res;
	}

	public function set_garant($uziv_cislo)
	{
		if (empty($uziv_cislo))
			return false;

		$this->uziv_cislo = $uziv_cislo;

		if ($this->existing)
			return predmet_set_garant($this->zkratka_predmet, $this->ak_rok, $this->uziv_cislo);
		else
			return true;
	}

	public function get_zkratka()
	{
		return $this->zkratka_predmet;
	}

	public function get_ak_rok()
	{
		return $this->ak_rok;
	}
};

function get_predmety_zkratka()
{
	global $conn;
	return $conn->query("SELECT DISTINCT zkratka_predmet, nazev, popis FROM predmet ORDER BY zkratka_predmet");
}

function get_predmety()
{
	global $conn;
	return $conn->query("SELECT * FROM predmet ORDER BY zkratka_predmet");
}

function get_predmet($zkratka_predmet, $ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT predmet.*, uzivatel.login, uzivatel.jmeno FROM predmet
	LEFT JOIN uzivatel ON predmet.uziv_cislo=uzivatel.uziv_cislo
	WHERE predmet.zkratka_predmet = ? AND predmet.ak_rok = ? LIMIT 1");
	$q->bind_param("si", $zkratka_predmet, $ak_rok);
	$q->execute();
	return $q->get_result();
}

function get_predmety_roky($zkratka_predmet)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT ak_rok FROM predmet WHERE zkratka_predmet = ? ORDER BY ak_rok DESC");
	$q->bind_param("s", $zkratka_predmet);
	$q->execute();
	return $q->get_result();
}

function get_predmety_by_rok($ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM predmet WHERE ak_rok = ? ORDER BY zkratka_predmet");
	$q->bind_param("i", $ak_rok);
	$q->execute();
	return $q->get_result();
}

function get_predmety_by_obor($zkratka_obor)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM predmet WHERE zkratka_obor = ? ORDER BY zkratka_predmet");
	$q->bind_param("s", $zkratka_obor);
	$q->execute();
	return $q->get_result();
}

function get_predmety_by_obor_rok($zkratka_obor, $ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT * FROM predmet WHERE zkratka_obor = ? AND ak_rok = ? ORDER BY zkratka_predmet");
	$q->bind_param("si", $zkratka_obor, $ak_rok);
	$q->execute();
	return $q->get_result();
}

function predmet_set_garant($zkratka_predmet, $ak_rok, $uziv_cislo)
{
	if (empty($zkratka_predmet) || $ak_rok < 2002 || empty($uziv_cislo))
		return false;

	global $conn;
	$q = $conn->prepare("UPDATE predmet SET uziv_cislo=? WHERE zkratka_predmet = ? AND ak_rok = ?");
	$q->bind_param("isi", $uziv_cislo, $zkratka_predmet, $ak_rok);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function delete_predmet($zkratka_predmet, $ak_rok)
{
	if (empty($zkratka_predmet) || $ak_rok < 2002)
		return false;

	global $conn;
	$q = $conn->prepare("DELETE FROM predmet WHERE zkratka_predmet = ? AND ak_rok = ? LIMIT 1");
	$q->bind_param("si", $zkratka_predmet, $ak_rok);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}
?>
