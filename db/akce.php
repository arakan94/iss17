<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Akce {
	public $zkratka_predmet;
	public $ak_rok;
	public $typ_id;

	public $uziv_cislo;  // garant
	public $nazev;
	public $popis = NULL;

	private $existing = false;
    
	public function __construct($zkratka_predmet, $ak_rok, $typ_id)
	{
		$res = get_akce($zkratka_predmet, $ak_rok, $typ_id);

		if ($res->num_rows > 0) 
		{
			$akce = $res->fetch_assoc();
			$this->uziv_cislo = $akce["uziv_cislo"];
			$this->nazev = $akce["nazev"];
			$this->popis = $akce["popis"];
			$this->existing = true;
		}

		$this->zkratka_predmet = $zkratka_predmet;
		$this->ak_rok = $ak_rok;
		$this->typ_id = $typ_id;  
	}
    
	public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if (empty($this->zkratka_predmet) || empty($this->ak_rok) || empty($this->typ_id) || empty($this->uziv_cislo) || empty($this->nazev) ||
		$this->ak_rok < 2002 || $this->existing)
			return false;

		global $conn;
		$q = $conn->prepare("INSERT INTO akce (zkratka_predmet, ak_rok, typ_id, uziv_cislo, nazev, popis) VALUES (?, ?, ?, ?, ?, ?)");
		$q->bind_param("siiiss", $this->zkratka_predmet, $this->ak_rok, $this->typ_id, $this->uziv_cislo, $this->nazev, $this->popis);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if (empty($this->zkratka_predmet) || empty($this->ak_rok) || empty($this->typ_id) || empty($this->uziv_cislo) || empty($this->nazev) ||
		$this->ak_rok < 2002 || $this->existing == false)
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE akce SET uziv_cislo = ?, nazev = ?, popis = ? WHERE zkratka_predmet = ? AND ak_rok = ? AND typ_id = ?");
		$q->bind_param("isssii", $this->uziv_cislo, $this->nazev, $this->popis, $this->zkratka_predmet, $this->ak_rok, $this->typ_id);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function change_typ_id($new_typ_id)
	{
		if (empty($new_typ_id))
			return false;

		if ($this->existing == true)
			$res = $this->update_typ_id($new_typ_id);
		else
			$res = true;

		$this->typ_id = $new_typ_id;

		return $res;
	}

	public function update_typ_id()
	{
		global $conn;
		$q = $conn->prepare("UPDATE akce SET typ_id = ? WHERE zkratka_predmet = ? AND ak_rok = ? AND typ_id = ?");
		$q->bind_param("isii", $new_typ_id, $this->zkratka_predmet, $this->ak_rok, $this->typ_id);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function change_primary_key($new_zkratka_predmet, $new_ak_rok, $new_typ_id)
	{
		if (empty($new_zkratka_predmet) || empty($new_typ_id) || $new_ak_rok < 2002)
			return false;

		if ($this->existing == true)
			$res = $this->update_primary_key($new_zkratka_predmet, $new_ak_rok, $new_typ_id);
		else
			$res = true;

		$this->zkratka_predmet = $new_zkratka_predmet;
		$this->ak_rok = $new_ak_rok;
		$this->typ_id = $new_typ_id;

		return $res;
	}

	private function update_primary_key($new_zkratka_predmet, $new_ak_rok, $new_typ_id)
	{
		global $conn;
		$q = $conn->prepare("UPDATE akce SET zkratka_predmet = ?, ak_rok = ?, typ_id = ? WHERE zkratka_predmet = ? AND ak_rok = ? AND typ_id = ?");
		$q->bind_param("siisii", $new_zkratka_predmet, $new_ak_rok, $new_typ_id, $this->zkratka_predmet, $this->ak_rok, $this->typ_id);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if (empty($this->zkratka_predmet) || empty($this->ak_rok) || empty($this->typ_id) || $this->existing == false)
			return false;

		global $conn;
		$q = $conn->prepare("DELETE FROM akce WHERE zkratka_predmet = ? AND ak_rok = ? AND typ_id = ? LIMIT 1");
		$q->bind_param("sii", $this->zkratka_predmet, $this->ak_rok, $this->typ_id);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}
};

class TypAkce {
	public $nazev;

	private $typ_id;
	private $existing = false;

	public function __construct($nazev)
	{
		$res = get_typ_akce($nazev);
		
		if ($res->num_rows > 0) 
		{
			$typ = $res->fetch_assoc();
			$this->typ_id = $typ["typ_id"];
			$this->existing = true;
		}
            $this->nazev = $nazev;
	}
    
    public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if (empty($this->nazev || $this->existing)){
			return false;
        }
		global $conn;
		$q = $conn->prepare("INSERT INTO typ_akce (nazev) VALUES (?)");
		$q->bind_param("s", $this->nazev);

		if ($q->execute())
		{
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if (empty($this->nazev || $this->existing == false))
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE typ_akce SET nazev = ? WHERE typ_id = ?");
		$q->bind_param("si", $this->nazev, $this->typ_id);

		if ($q->execute())
		{
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;
        
        global $conn;
		$q = $conn->prepare("DELETE FROM typ_akce WHERE typ_id = ?");
		$q->bind_param("i", $this->typ_id);

		if ($q->execute())
		{
			$this->existing = false;
			unset($this->typ_id);
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}			
	}
};

/* TYPY AKCI */

function get_all_typ_akce()
{
	global $conn;
	return $conn->query("SELECT * FROM typ_akce ORDER BY nazev");
}

function get_typ_akce($nazev)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM typ_akce WHERE nazev = ? LIMIT 1");
	$q->bind_param("s", $nazev);
	$q->execute();
	return $q->get_result();
}

function get_typ_akce_id($typ_id)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM typ_akce WHERE typ_id = ? LIMIT 1");
	$q->bind_param("i", $typ_id);
	$q->execute();
	return $q->get_result();
}

/* AKCE */
function get_all_akce()
{
	global $conn;
	return $conn->query("SELECT * FROM akce ORDER BY zkratka_predmet, nazev");
}

function get_akce_rok($ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM akce WHERE ak_rok = ? ORDER BY zkratka_predmet, nazev");
	$q->bind_param("i", $ak_rok);
	$q->execute();
	return $q->get_result();
}

function get_akce_predmet_rok($zkratka_predmet, $ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM akce WHERE zkratka_predmet = ? AND ak_rok = ? ORDER BY nazev");
	$q->bind_param("si", $zkratka_predmet, $ak_rok);
	$q->execute();
	return $q->get_result();
}

function get_akce_predmet($zkratka_predmet)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM akce WHERE zkratka_predmet = ? ORDER BY nazev");
	$q->bind_param("s", $zkratka_predmet);
	$q->execute();
	return $q->get_result();
}

function get_akce($zkratka_predmet, $ak_rok, $typ_id)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM akce WHERE zkratka_predmet = ? AND ak_rok = ? AND typ_id = ? LIMIT 1");
	$q->bind_param("sii", $zkratka_predmet, $ak_rok, $typ_id);
	$q->execute();
	return $q->get_result();
}

function get_akce_join($zkratka_predmet, $ak_rok, $typ_id)
{
	global $conn;
	$q = $conn->prepare("SELECT akce.*, uzivatel.*, typ_akce.nazev AS typ_akce_nazev FROM akce
	LEFT JOIN uzivatel ON akce.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN typ_akce ON akce.typ_id=typ_akce.typ_id
	WHERE akce.zkratka_predmet = ? AND akce.ak_rok = ? AND akce.typ_id = ? LIMIT 1");
	$q->bind_param("sii", $zkratka_predmet, $ak_rok, $typ_id);
	$q->execute();
	return $q->get_result();
}
?>
