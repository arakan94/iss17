<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Ustav {
	private $zkratka_ustav;
	public $nazev;
	
	private $existing = false;

	public function __construct($zkratka_ustav)
	{
		$res = get_ustav($zkratka_ustav);
		$this->zkratka_ustav = $zkratka_ustav;

		if ($res->num_rows > 0)  // ustav nalezen
		{
			$tmp = $res->fetch_assoc();
			$this->nazev = $tmp["nazev"];
			$this->existing = true;
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if (add_ustav($this->zkratka_ustav, $this->nazev))
		{
			$this->existing = true;
			return true;
		}
		else
			return false;
	}

	public function update()
	{
		if ($this->existing == false)
			return false;

		return update_ustav($this->zkratka_ustav, $this->nazev);
	}

	public function change_primary_key($new_zkratka_ustav)
	{
		if (empty($new_zkratka_ustav))
			return false;

		if ($this->existing == true)
			$res = update_primary_key_ustav($this->zkratka_ustav, $new_zkratka_ustav);
		else
			$res = true;

		$this->zkratka_ustav = $new_zkratka_ustav;
		return $res;
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		if (delete_ustav($this->zkratka_ustav))
		{
			$this->existing = false;
			return true;
		}
		else
			return false;
	}
}


function get_ustav($zkratka_ustav)
{
	global $conn;
	$q = $conn->prepare("SELECT * FROM ustav WHERE zkratka_ustav = ? LIMIT 1");
	$q->bind_param("s", $zkratka_ustav);
	$q->execute();
	return $q->get_result();
}

function get_ustavy()
{
	global $conn;
	return $conn->query("SELECT * FROM ustav");
}

function add_ustav($zkratka_ustav, $nazev)
{
	if (empty($zkratka_ustav) || empty($nazev))
		return false;

	global $conn;
	$q = $conn->prepare("INSERT INTO ustav (zkratka_ustav, nazev) VALUES (?, ?)");
	$q->bind_param("ss", $zkratka_ustav, $nazev);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function update_ustav($zkratka_ustav, $nazev)
{
	if (empty($zkratka_ustav) || empty($nazev))
		return false;

	global $conn;
	$q = $conn->prepare("UPDATE ustav SET nazev = ? WHERE zkratka_ustav = ?");
	$q->bind_param("ss", $nazev, $zkratka_ustav);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function update_primary_key_ustav($zkratka_ustav, $new_zkratka_ustav)
{
	if (empty($zkratka_ustav) || empty($new_zkratka_ustav))
		return false;

	global $conn;
	$q = $conn->prepare("UPDATE ustav SET zkratka_ustav = ? WHERE zkratka_ustav = ?");
	$q->bind_param("ss", $new_zkratka_ustav, $zkratka_ustav);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}

function delete_ustav($zkratka_ustav)
{
	if (empty($zkratka_ustav))
		return false;

	global $conn;
	$q = $conn->prepare("DELETE FROM ustav WHERE zkratka_ustav = ? LIMIT 1");
	$q->bind_param("s", $zkratka_ustav);

	if ($q->execute())
		return true;
	else
	{
		echo $q->error . "\n";
		return false;
	}
}
?>
