<?php

// Author: David Novák <david.novak.work@gmail.com>, <xnovak1m@stud.fit.vutbr.cz>

class Rezervace {
	private $rezerv_id;

	public $uziv_cislo; // spravce rezervace

	public $ucebna_id;

	public $zkratka_predmet;
	public $ak_rok;
	public $typ_id;

	public $zacatek;   // timestamp
	public $konec;     // timestamp
	public $poznamka;

	private $existing = false;

	public function __construct()
	{
		$cnt = func_num_args();
		$args = func_get_args();

		if ($cnt == 1)
		{
			$res = get_rezervace($args[0]);
			if ($res->num_rows > 0)
			{
				$tmp = $res->fetch_assoc();
				$this->rezerv_id = $tmp["rezerv_id"];

				$this->uziv_cislo = $tmp["uziv_cislo"];

				$this->ucebna_id = $tmp["ucebna_id"];

				$this->zkratka_predmet = $tmp["zkratka_predmet"];
				$this->ak_rok = $tmp["ak_rok"];
				$this->typ_id = $tmp["typ_id"];

				$this->zacatek = $tmp["zacatek"];
				$this->konec = $tmp["konec"];
				$this->poznamka = $tmp["poznamka"];

				$this->existing = true;
			}
		}
	}

	public function exists()
	{
		return $this->existing;
	}

	public function add()
	{
		if ($this->existing == true)
			return false;

		if (empty($this->uziv_cislo) || empty($this->ucebna_id) || empty($this->typ_id) || empty($this->ak_rok) || empty($this->zkratka_predmet))
			return false;

		if ($this->ak_rok < 2002 || $this->konec <= $this->zacatek)
			return false;

		if ($this->existing == true)
			return false;

		global $conn;
		$q = $conn->prepare("INSERT INTO rezervace (zkratka_predmet, ak_rok, typ_id, uziv_cislo, ucebna_id, zacatek, konec, poznamka) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		$q->bind_param("siiiisss", $this->zkratka_predmet, $this->ak_rok, $this->typ_id, $this->uziv_cislo, $this->ucebna_id, $this->zacatek, $this->konec, $this->poznamka);

		if ($q->execute())
		{
			$this->rezerv_id = $conn->insert_id;
			$this->existing = true;
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function update()
	{
		if ($this->existing == false)
			return false;

		if (empty($this->uziv_cislo) || empty($this->ucebna_id) || empty($this->typ_id) || empty($this->ak_rok) || empty($this->zkratka_predmet))
			return false;

		if ($this->ak_rok < 2002 || $this->konec <= $this->zacatek)
			return false;

		global $conn;
		$q = $conn->prepare("UPDATE rezervace SET zkratka_predmet = ?, ak_rok = ?, typ_id = ?, uziv_cislo = ?, ucebna_id = ?, zacatek = ?, konec = ?, poznamka = ? WHERE rezerv_id = ?");
		$q->bind_param("siiiisssi", $this->zkratka_predmet, $this->ak_rok, $this->typ_id, $this->uziv_cislo, $this->ucebna_id, $this->zacatek, $this->konec, $this->poznamka, $this->rezerv_id);

		if ($q->execute())
		{
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function delete()
	{
		if ($this->existing == false)
			return false;

		global $conn;
		$q = $conn->prepare("DELETE FROM rezervace WHERE rezerv_id = ?");
		$q->bind_param("i", $this->rezerv_id);

		if ($q->execute())
		{
			$this->existing = false;
			unset($this->rezerv_id);
			return true;
		}
		else
		{
			echo $q->error . "\n";
			return false;
		}
	}

	public function get_id()
	{
		return $this->rezerv_id;
	}
};

function get_all_rezervace()
{
	global $conn;
	return $conn->query("SELECT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id");
}

function get_rezervace($rezerv_id)
{
	global $conn;
	$q = $conn->prepare("SELECT rezervace.*, akce.*, ucebna.*, uzivatel.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.rezerv_id = ? LIMIT 1");
	$q->bind_param("i", $rezerv_id);
	$q->execute();
	return $q->get_result();
}

function get_rezervace_garant($uziv_cislo)
{
	global $conn;
	$q = $conn->prepare("SELECT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.uziv_cislo = ?");
	$q->bind_param("i", $uziv_cislo);
	$q->execute();
	return $q->get_result();
}

function get_rezervace_predmet($zkratka_predmet, $ak_rok)
{
	global $conn;
	$q = $conn->prepare("SELECT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.zkratka_predmet = ? AND rezervace.ak_rok = ?");
	$q->bind_param("si", $zkratka_predmet, $ak_rok);
	$q->execute();
	return $q->get_result();
}

function get_rezervace_akce($zkratka_predmet, $ak_rok, $typ_id)
{
	global $conn;
	$q = $conn->prepare("SELECT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.zkratka_predmet = ? AND rezervace.ak_rok = ? AND rezervace.typ_id = ?");
	$q->bind_param("sii", $zkratka_predmet, $ak_rok, $typ_id);
	$q->execute();
	return $q->get_result();
}

function get_rezervace_ucebna($ucebna_id)
{
	global $conn;
	$q = $conn->prepare("SELECT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.ucebna_id = ?");
	$q->bind_param("i", $ucebna_id);
	$q->execute();
	return $q->get_result();
}

// vrati rezervace v danem rozpeti start-konec
function get_rezervace_rozpeti($start, $konec)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE rezervace.start >= ? AND rezervace.konec <= ?");
	$q->bind_param("ss", $start, $konec);
	$q->execute();
	return $q->get_result();
}

function get_rezervace_rozpeti_castecne($start, $konec)
{
	global $conn;
	$q = $conn->prepare("SELECT DISTINCT rezervace.*, akce.*, ucebna.*, typ_akce.nazev AS typ_akce_nazev FROM rezervace
	LEFT JOIN uzivatel ON rezervace.uziv_cislo=uzivatel.uziv_cislo
	LEFT JOIN akce ON rezervace.zkratka_predmet=akce.zkratka_predmet AND rezervace.ak_rok=akce.ak_rok AND rezervace.typ_id=akce.typ_id
	LEFT JOIN typ_akce ON rezervace.typ_id=typ_akce.typ_id
	LEFT JOIN ucebna ON rezervace.ucebna_id=ucebna.ucebna_id
	WHERE (rezervace.start BETWEEN ? AND ?) OR (rezervace.konec BETWEEN ? AND ?)");
	$q->bind_param("ssss", $start, $konec, $start, $konec);
	$q->execute();
	return $q->get_result();
}

?>
