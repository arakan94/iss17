# README #

Školní projekt, IIS 2017, VUT FIT

[Testovací server na eva](http://www.stud.fit.vutbr.cz/~xnovak1m/)

## Členové ##

David Novák, xnovak1m

Přemysl Mlýnek, xmlyne04

## Konfigurace ##

Pro korektní běh na serveru eva je třeba v .htaccess nastavit toto:

- DefaultCharset utf-8
- LanguageCharset cs utf-8
- AddHandler application/x-httpd-php70 .php *(použití PHP 7)*
- SetEnv ROOT_URL /~xnovak1m *(url, ke kterému se mají generovat odkazy v HTML)*
- SetEnv ROOT_DIR /home/users/xn/xnovak1m/WWW *(kde je web skutečně uložen - pro php include)*

## Zadání ##

Navrhněte modul fakultního informačního systému, který bude umožňovat správu učeben a
laboratoří na FITu. Systém musí uchovávat základní informace o učebnách, jejich umístění, kapacitu a
vybavení (projektor, klimatizace, kamera, počet a typ tabulí,...). Dále musí systém umožňovat
rezervaci učeben pro různé akce (zkoušky, semináře, cvičení,...). Rezervace učeben a laboratoří
mohou provádět jen akademičtí pracovníci, změny informací o učebnách a laboratořích provádí
pouze správce systému. Systém musí umožňovat vypisovat rozvrh učeben po oborech, ročnících,
předmětech nebo po učebnách dle potřeby, musí umožnit zjistit volné učebny v daném termínu,
případně s konkrétním vybavením. Systém také musí uchovávat informace o tom, kdo a kdy danou
učebnu rezervoval.